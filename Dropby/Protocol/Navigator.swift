//
//  Navigator.swift
//  Dropby
//
//  Created by Junaid on 10/11/2020.
//

import Foundation
import UIKit
import MBProgressHUD

protocol Navigator {
    
}

extension Navigator{
        
    func logOut(){
        
        guard let vc = UIStoryboard(name: "SignIn", bundle: nil).instantiateViewController(withIdentifier: "startNav") as? UINavigationController else {print("no home tab");return}
        UIView.animate(withDuration: 0.3) {
            UIView.animate(withDuration: 0.3) {
                
                UIApplication.shared.windows.first?.rootViewController = vc
                UIApplication.shared.windows.first?.makeKeyAndVisible()
                
            }
        }
    }
    
    func goToHome(){
        
        //user is registered and logged in so activate the realtimeupdates
        ProfileHandler.shared.setupLocationListener()                
        ProfileHandler.shared.authInteractor.realtimeUpdates()
        ProfileHandler.shared.subscribeToTopic()
        
        guard let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "MainTab") as? UITabBarController else {print("no home tab");return}
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
        
    
    func checkUserExistsAndDecideNavigation(userID: String, viewController: UIViewController, completion: @escaping (()->Void)) {
                
        ProfileHandler.shared.fetchProfileModelFromServer { (success) in
            
            if success {
                UserDefaults.standard.set(true, forKey: AppState.tags_added.rawValue)
                self.goToHome()
            }
            else {
                // move to email
                let vc = EmailLinkingViewController.instantiate(fromAppStoryboard: .UserAuthentication)
                viewController.show(vc, sender: true)
            }
            completion()
        }
    }
}

//MARK:- Naviagtor for AppDeleget
extension Navigator where Self:SceneDelegate{

    func appScreenOpenLogic(){

        if let _ = UserDefaults.standard.string(forKey: "UserId") {
            //fetch object and load profile
//            addLoader()
            ProfileHandler.shared.fetchProfileModelFromServer { (success) in
                if success {
                    self.removeLoader()
                    self.goToHome()
                }
                else {
                    //case in which profile was deleted from backend.
                    self.removeLoader()
                    UserDefaults.standard.removeObject(forKey: "UserId")
                    UserDefaults.standard.synchronize()
                    self.appScreenOpenLogic()
                }
                
                if UserDefaults.standard.bool(forKey: AppState.tags_added.rawValue) {
                    // show home screen
                    self.goToHome()
                }
                else {
                    self.removeLoader()
                    guard let nav = UIStoryboard(name: "UserAuthentication", bundle: nil).instantiateViewController(withIdentifier: "SignInNav") as? UINavigationController else {print("no home tab");return}
                    //check profile
                    if UserDefaults.standard.bool(forKey: AppState.profile_completed.rawValue) {
                        //show tags screen
                        let vc = MyTagsViewController.instantiate(fromAppStoryboard: .UserAuthentication)
                        nav.pushViewController(vc, animated: true)
                        UIApplication.shared.windows.first?.rootViewController = nav
                        UIApplication.shared.windows.first?.makeKeyAndVisible()
                    }
                    else {
                        //check email connectivity
                        if UserDefaults.standard.bool(forKey: AppState.email_connected.rawValue) {
                            //show profile screen
                            let vc = EditProfileViewController.instantiate(fromAppStoryboard: .UserAuthentication)
                            nav.pushViewController(vc, animated: true)
                            UIApplication.shared.windows.first?.rootViewController = nav
                            UIApplication.shared.windows.first?.makeKeyAndVisible()
                        }
                        else {
                            // start from beginning
                            let vc = LoginViewController.instantiate(fromAppStoryboard: .UserAuthentication)
                            nav.pushViewController(vc, animated: true)
                            UIApplication.shared.windows.first?.rootViewController = nav
                            UIApplication.shared.windows.first?.makeKeyAndVisible()
                        }
                    }
                }
            }
        }
        else {
            // start from beginning
            guard let nav = UIStoryboard(name: "UserAuthentication", bundle: nil).instantiateViewController(withIdentifier: "SignInNav") as? UINavigationController else {print("no home tab");return}
            
            let vc = LoginViewController.instantiate(fromAppStoryboard: .UserAuthentication)
            nav.pushViewController(vc, animated: true)
            UIApplication.shared.windows.first?.rootViewController = nav
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }
    
    
    func addLoader() {
        if let viewToAdd = self.window?.rootViewController?.view {
            MBProgressHUD.showAdded(to: viewToAdd, animated: true)
        }
    }
    
    func removeLoader() {
        if let viewToAdd = self.window?.rootViewController?.view {
            MBProgressHUD.hide(for: viewToAdd, animated: true)
        }
    }
    
    func goToSettingScreen(){
        
        guard let vc = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "SettingVC") as? SettingVC else {print("no Setting found");return}
        UIView.animate(withDuration: 0.3) {
            UIView.animate(withDuration: 0.3) {
                UIApplication.shared.windows.first?.rootViewController = vc
                UIApplication.shared.windows.first?.makeKeyAndVisible()
            }
        }
    }
}
