//
//  MoreActionSheet.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 24/11/2020.
//

import UIKit
import MBProgressHUD

class MoreActionSheet: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    
    var callBack: ((ActionSheetType)-> Void)? = nil
    var userModel : ProfileModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func optionTapped(_ sender: Any) {
        
        var popupTitle = ""
        var popupDescription = ""
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ProfileHandler.shared.checkUserFriendStatus(userId: userModel.userInfoModel.userId) { (status) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if let btn = sender as? UIButton {
                
                if btn.tag == 1{
                    if status == .friends {
                        
                        popupTitle = "Remove Connection"
                        popupDescription = "Do you want to remove connection with this contact?"
                    }
                    else if status == .friendRequested {
                        
                        popupTitle = "Already Requested"
                        popupDescription = "Do you want to cancel connection request with this contact?"
                    }
                    else {
                        popupTitle = "Connect"
                        popupDescription = "Do you want to connect with this contact?"
                    }
                }
                else if btn.tag == 2 {
                    popupTitle = "Mark Invisible"
                    popupDescription = "Do you want to mark this contact invisible?"
                }
                else if btn.tag == 3 {
                    popupTitle = "Report Contact"
                    popupDescription = "Do you want to report this contact?"
                    
                    self.closeMenu()
                    self.callBack!(.report)
                }
                else if btn.tag == 4 {
                    popupTitle = "Block Contact"
                    popupDescription = "Do you want to block this contact?"
                }
                else {
                 
                    self.closeMenu()
                    return
                }
                
                let vc = GenericPopup.instantiate(fromAppStoryboard: .PopUp)
                vc.modalPresentationStyle = .overFullScreen
                vc.titleText = popupTitle
                vc.descriptionText = popupDescription
                vc.okButtonText = "YES"
                vc.cancelButtonText = "CLOSE"
                
                self.present(vc, animated: true) {
                    
                    UIView.animate(withDuration: 0.1) {
                        vc.backgroundView.alpha = 0.5
                    }
                    vc.btnActionCallback = {action in
                        
                        if action == .ok
                        {
                            if btn.tag == 1  {
                                if status == .friends {
                                    //Cancel request - remove friends
                                    MBProgressHUD.showAdded(to: self.view, animated: true)
                                    ProfileHandler.shared.authInteractor.removeConnection(user: self.userModel) { (success) in
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                    }
                                }
                                else if status == .friendRequested {
                                    MBProgressHUD.showAdded(to: self.view, animated: true)
                                    ProfileHandler.shared.authInteractor.cancelConnectionRequest(user: self.userModel) { (success) in
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                    }
                                }
                                else {
                                    
                                    //send friend request
                                    //TODO:- friend request
                                    MBProgressHUD.showAdded(to: self.view, animated: true)
                                    ProfileHandler.shared.sendFriendRequest(user: self.userModel) { (success) in
                                        
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                    }
                                }
                                self.closeMenu()
                                self.callBack!(.friendUnFriend)
                            }
                            if btn.tag == 4  {
                                self.callBack!(.block)
                            }
                            if btn.tag == 2 {
                                self.callBack!(.invisible)
                            }
                        }
                    }
                }
            }
        }
//        fetchFriendsSatus(forUser: self.userModel.userInfoModel.userId)
        
        
    }
    @IBAction func cancelTapped(_ sender: Any) {
        closeMenu()
    }
    
    @IBAction func gestureTapped(_ sender: Any) {
        closeMenu()
    }
    
    func closeMenu() {
        self.backgroundView.alpha = 0
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
