//
//  MyVisibilityPopup.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 24/11/2020.
//

import UIKit
import MBProgressHUD

class MyVisibilityPopup: UIViewController {
    var selectedTag = 4
    @IBOutlet weak var goOfflineBtn: UIButton!
    @IBOutlet weak var friendsBtn: UIButton!
    @IBOutlet weak var fofBtn: UIButton!
    @IBOutlet weak var allBtn: UIButton!
    
    @IBOutlet weak var backGroundView: UIView!
    var successCallback: ((Bool)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        UIView.animate(withDuration: 0.5) {
//            self.backGroundView.alpha = 0.5
//        }
        let visibility = ProfileHandler.shared.profileModel.visibilityModel.status
        
        if visibility == VisibilityStatus.GoOffline.rawValue {
            selectedTag = 1
            goOfflineBtn.isSelected = true
        }
        else if visibility == VisibilityStatus.Friends.rawValue {
            friendsBtn.isSelected = true
            selectedTag = 2
        }
        else if visibility == VisibilityStatus.FriendsOfFriends.rawValue {
            fofBtn.isSelected = true
            selectedTag = 3
        }
        else if visibility == VisibilityStatus.Everyone.rawValue {
            allBtn.isSelected = true
            selectedTag = 4
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func gestureTapped(_ sender: Any) {
        closeMenu()
    }
    
    @IBAction func visibilitySelectionTapped(_ sender: Any) {
                
        var selectedVisibility  = VisibilityStatus.Everyone
        
        if let btn = sender as? UIButton {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            ProfileHandler.shared.checkEmailVerified { (status) in
                
                if !status && self.allBtn.tag == btn.tag {
                    let vc = AppHandler.shared.showEmailNotVerifiedPopup()
                    self.present(vc, animated: true, completion: {
                        UIView.animate(withDuration: 0.1) {
                                                                    
                            vc.backgroundView.alpha = 0.5
                        }
                    })
                    vc.callback = { action in
                        
                        if action == .ok {
                        }
                        else {
                            MBProgressHUD.showAdded(to: self.view, animated: true)
                            APIManager.shared.sendVerificationEmail { (json) in
                                DispatchQueue.main.async {
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    self.view.makeToast("Verification email has been sent", duration: 3.0, position: .bottom)
                                }
                            }
                        }
                    }

                    vc.detailText.text = "To enable your visibility to public, you must verify your email through the link sent on your registered email"
                    MBProgressHUD.hide(for: self.view, animated: true)
                    return
                }
                
                self.goOfflineBtn.isSelected = false
                self.friendsBtn.isSelected = false
                self.fofBtn.isSelected = false
                self.allBtn.isSelected = false
                btn.isSelected = true
                
                self.selectedTag = btn.tag
                
                if btn.tag == 1 {
                    selectedVisibility = VisibilityStatus.GoOffline
                }
                else if btn.tag == 2 {
                    selectedVisibility = VisibilityStatus.Friends
                }
                else if btn.tag == 3 {
                    selectedVisibility = VisibilityStatus.FriendsOfFriends
                }
                else if btn.tag == 4 {
                    selectedVisibility = VisibilityStatus.Everyone
                }
                                
                
                ProfileHandler.shared.setVisibility(visibility: selectedVisibility.rawValue) { (success) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.closeMenu()
                    if let callback = self.successCallback {
                        callback(success)
                    }
                }
            }
        }
    }
    
    func closeMenu() {
        self.backGroundView.alpha = 0
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
