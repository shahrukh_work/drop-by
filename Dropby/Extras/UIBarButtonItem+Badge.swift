//
//  UIBarButtonItem+Badge.swift
//  Dropby
//
//  Created by Asad Mehmood on 26/08/2021.
//

import UIKit

private var handle: UInt8 = 0
extension UIBarButtonItem {
    
    private var badgeLayer: CAShapeLayer? {
        if let b: AnyObject = objc_getAssociatedObject(self, &handle) as AnyObject? {
            return b as? CAShapeLayer
        } else {
            return nil
        }
    }
    
    func setBadge(offset: CGPoint = .zero, color: UIColor = .orange, filled: Bool = true, fontSize: CGFloat = 11) {
        badgeLayer?.removeFromSuperlayer()
        guard let view = self.value(forKey: "view") as? UIView else {
            return
        }
        
        var font = UIFont.systemFont(ofSize: fontSize)
        
        if #available(iOS 9.0, *) {
            font = UIFont.monospacedDigitSystemFont(ofSize: fontSize, weight: .regular)
        }
        
        //Size of the dot
        let badgeSize = UILabel(frame: CGRect(x: 22, y: -05, width: 10, height: 10))
        
        // initialize Badge
        let badge = CAShapeLayer()
        
        let height = badgeSize.frame.height
        let width = badgeSize.frame.width
        
        // x position is offset from right-hand side
        let x = view.frame.width - width + offset.x - 10
        
        // I suggest you try the x and y sets, for my case, i will use this coordinates for better result,
        // but depends on the syze of your image
        // let x = view.frame.width + offset.x - 17
        // let y = view.frame.height + offset.y - 34
        
        let badgeFrame = CGRect(origin: CGPoint(x: x, y: offset.y + 7), size: CGSize(width: width, height: height))
        
        badge.drawRoundedRect(rect: badgeFrame, andColor: color, filled: filled)
        view.layer.addSublayer(badge)
        
        // initialiaze Badge's label
        let label = CATextLayer()
        label.alignmentMode = .center
        label.font = font
        label.fontSize = font.pointSize
        
        label.frame = badgeFrame
        label.foregroundColor = filled ? UIColor.white.cgColor : color.cgColor
        label.backgroundColor = UIColor.clear.cgColor
        label.contentsScale = UIScreen.main.scale
        badge.addSublayer(label)
        
        // save Badge as UIBarButtonItem property
        objc_setAssociatedObject(self, &handle, badge, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        
        // bring layer to front
        badge.zPosition = 1_000
    }
    
    func removeBadge() {
        badgeLayer?.removeFromSuperlayer()
    }
}

// MARK: - Utilities
extension CAShapeLayer {
    func drawRoundedRect(rect: CGRect, andColor color: UIColor, filled: Bool) {
        fillColor = filled ? color.cgColor : UIColor.white.cgColor
        strokeColor = color.cgColor
        path = UIBezierPath(roundedRect: rect, cornerRadius: 7).cgPath
    }
}

