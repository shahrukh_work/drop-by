//
//  VerifyAccountPopup.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 08/03/2021.
//

import UIKit

class VerifyAccountPopup: UIViewController {
    var callback: ((ButtonAction)->Void)?
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var titleImageView: UIImageView!
    
    @IBOutlet weak var detailText: UILabel!
    @IBOutlet weak var cancelBtn: DesignableButton!
    @IBOutlet weak var okBtn: DesignableButton!
    
    var descriptionTxt  = ""
    var cancelBtnHidden = false
    var imageViewHidden = false
    var okBtnTitle      = "OK"
    var cancelBtnTitle  = "CANCEL"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.detailText.text            = descriptionTxt
        self.cancelBtn.isHidden         = cancelBtnHidden
        self.titleImageView.isHidden    = imageViewHidden
        self.okBtn.setTitle(okBtnTitle, for: UIControl.State())
        self.cancelBtn.setTitle(cancelBtnTitle, for: UIControl.State())
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func didTapOk(_ sender: Any) {
        
        if callback != nil {
            callback!(ButtonAction.ok)
        }
        closeMenu()
    }
    @IBAction func didTapCancel(_ sender: Any) {
        if callback != nil {
            callback!(ButtonAction.cancel)
            closeMenu()
        }
    }
    
    func closeMenu() {
        self.backgroundView.alpha = 0
        self.dismiss(animated: true, completion: nil)
    }
}
