//
//  GenericPopup.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 24/11/2020.
//

import UIKit

class GenericPopup: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var acceptBtn: UIButton!
    
    var btnActionCallback: ((ButtonAction) -> Void)?
    var titleText = ""
    var descriptionText = ""
    var shouldHideCancel = false
    var shouldHideOk = false
    var cancelButtonText = ""
    var okButtonText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.titleLabel.text = titleText
        self.descriptionLabel.text = descriptionText
        self.cancelBtn.isHidden = shouldHideCancel
        self.acceptBtn.isHidden = shouldHideOk
        self.cancelBtn.setTitle(cancelButtonText, for: UIControl.State())
        self.acceptBtn.setTitle(okButtonText, for: UIControl.State())
    }
    
    @IBAction func closeMenuTapped(_ sender: Any) {
        closeMenu(action: .cancel)
    }
    
    @IBAction func acceptTapped(_ sender: Any) {
        closeMenu(action: .ok)
    }
    @IBAction func cancelTapped(_ sender: Any) {
        closeMenu(action: .cancel)
    }
    
    func closeMenu(action: ButtonAction) {
        
        if btnActionCallback != nil {
            btnActionCallback!(action)
        }
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
