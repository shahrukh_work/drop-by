//
//  DisclaimerPopup.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 08/03/2021.
//

import UIKit

class DisclaimerPopup: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var bodyText: UITextView!
    let disclaimerText = """
        <html>
                <u>Disclaimer</u> \n\n
                Hi there!\n
                Dropby is currently in early beta, and our app usage is limited to users with a verified
                institutional email or a LinkedIn account. If you do not have an organizational email, please
                reach out to us and we can verity you directly. Unverified accounts can still interact with dropby
                users who they share phone contacts with.\n\n
                The Dropby Community is designed to connect with users around you. We value privacy and
                safe of all users, and please check our FAQs how we protect your privacy form others users.\n\n\n

                Rules:\n\n
                YOU MUST - &gt;\n
                -Use a real name.\n
                -Be at least 18 years of age.\n
                -Not have a fake activity on the map.\n
                -Not engage in hateful conduct directed at, or threaten violence or harm against any person or
                groups of people.\n
                -Not share others location information granted to you with others other users without their
                permission.\n
                -Not use the platform for false information or spam.\n
                -Not share or promote information (or synthetic or manipulated media) that is intended or
                likely to cause harm to any person or groups of people, including minors.\n
                -Not use the service for the purpose of conducting any unauthorized or illegal activities.\n\n
                If you witness a Rules violation, please reach us via our app, email or the website\n
                (www.dropby.org)\n\n
                If you have questions, feedback, or suggestions, we’d love to hear from you.\n
                [let us know] (mailto:info@dropby.org)!
        </html>
        """
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        
    }
    func setup(){
        self.bodyText.attributedText = self.disclaimerText.htmlToAttributedString
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func didTapCloseBtn(_ sender: Any) {
        closeMenu()
    }
    
    func closeMenu() {
        self.backgroundView.alpha = 0
        self.dismiss(animated: true, completion: nil)
    }
}
