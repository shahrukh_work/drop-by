//
//  InfoWindow.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 06/05/2021.
//

import UIKit
import Firebase

class InfoWindow: UIView {

    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var imageV: UIImageView!
    
    var timer:Timer?
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func willRemoveSubview(_ subview: UIView) {
        super.willRemoveSubview(subview)
        
    }
    class func instanceFromNib() -> UIView {
            return UINib(nibName: "InfoWindow", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
        }
}
