//
//  Date.swift
//  Dropby
//
//  Created by Asad Mehmood on 10/08/2021.
//

import Foundation

extension Date {

    static func - (lhs: Date, rhs: Date) -> TimeInterval {
        return lhs.timeIntervalSince(rhs)
    }

}
