//
//  DeleteAccountVC.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 15/02/2021.
//

import UIKit
import MBProgressHUD

class DeleteAccountVC: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    var btnActionCallback: (()->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapButton(_ sender: Any) {
        
        if let button = sender as? UIButton{
            let tag = button.tag
            
            if tag == 1 {
                //delete
                MBProgressHUD.showAdded(to: self.view, animated: true)
                APIManager.shared.softDeleteAccount { (success) in
                    
                    DispatchQueue.main.async {
                        ProfileHandler.shared.logout(view: self.view) {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if let callback = self.btnActionCallback {
                                callback()
                            }
                            self.closeMenu()
                        }
                        
                    }
                }
            }
            else {
                closeMenu()
            }
            
        }
    }
    
    @IBAction func closeMenuTapped(_ sender: Any) {
        closeMenu()
    }
    
    func closeMenu() {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
