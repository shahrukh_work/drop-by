//
//  Constant.swift
//  Dropby
//
//  Created by Junaid on 12/11/2020.
//

import Foundation

//MARK:- Type of typealias using all over in app

typealias ErrorClosure      =  (Error) -> Void
let versionNotName = Notification.Name.init("VERSION")
let startActivity = Notification.Name.init("startActivity")
let kContacts = "LOCAL_CONTACTS"

//MARK:- LinkedInConstants
struct LinkedInConstants {
    
    static let CLIENT_ID = "778hny0bex6wwb"
    static let CLIENT_SECRET = "QmoeXnVfi1Tzf8GM"
    static let REDIRECT_URI = "https://www.google.com/"
    static let SCOPE = "r_liteprofile%20r_emailaddress" //Get lite profile info and e-mail address
    
    static let AUTHURL = "https://www.linkedin.com/oauth/v2/authorization"
    static let TOKENURL = "https://www.linkedin.com/oauth/v2/accessToken"
}

enum AppState : String {
    case phone_verified
    case email_connected
    case gmail_connected
    case linkedin_connected
    case profile_completed
    case tags_added
}

enum WebViewType {
    case privacy
    case terms
    case about_us
}

enum FriendStatus {
    case notFriends
    case friendRequested
    case friends
}

enum ButtonAction {
    case ok
    case cancel
}

enum ActionSheetType {
    case report
    case block
    case invisible
    case friendUnFriend
}

enum DropByActivity: String {
    case coffee
    case cycling
    case running
    case study
    case food
    case drinks
    case exercise
    case movie
    case chill
    case swimming
}

enum VisibilityStatus: String {
    case GoOffline
    case Friends
    case FriendsOfFriends
    case Everyone
}

enum VisibilityCheck: String {
    case amiVisible
    case amiVisibleDropBy
}
