//
//  EmailVerificationVC.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 11/03/2021.
//

import UIKit
import MBProgressHUD

class EmailVerificationVC: UIViewController {

    @IBOutlet weak var emailTF: DesignableTextFeild!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextAction(_ sender: Any) {
        
        guard let email = emailTF.text, !email.isEmpty else {
            self.view.makeToast("Please Add email address", duration: 3.0, position: .top)
            return
        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        APIManager.shared.checkEmailAllowed(email: email) { (object) in
            
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                        
                var status = false
                if object != nil {
                    status = object!["status"] as? Bool ?? false
                }
                                
                let vc = VerifyAccountPopup.instantiate(fromAppStoryboard: .PopUp)
                vc.modalPresentationStyle = .overFullScreen
                vc.loadViewIfNeeded()
                if status == true {
                    //allowed - show verification popup
                    vc.detailText.text = "You need to verify your email. A verification link has been sent to email you shared."
                    vc.titleImageView.isHidden = true
                    vc.cancelBtn.setTitle("MODIFY", for: UIControl.State())
                    vc.okBtn.setTitle("OK", for: UIControl.State())
                }
                else {
                    vc.titleImageView.isHidden = false
                    vc.detailText.text = "The entered email is not allowed to register on Dropby."
                    vc.cancelBtn.isHidden = true
                    vc.okBtn.setTitle("OK", for: UIControl.State())
                }
                self.present(vc, animated: true) {
                    vc.callback = { action in
                        
                        if action == .ok {
                            if status == true {
                                //move to next screen i.e. profile
                                MBProgressHUD.showAdded(to: self.view, animated: true)
                                ProfileHandler.shared.updateUserModelWithEmail(email: email)
                                ProfileHandler.shared.synchroniseProfileData() { (status) in
                                    
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    
                                    if status {
                                        
                                        //MARK:- CALL API
                                        APIManager.shared.sendVerificationEmail { (json) in
                                            print("something")
                                        }
                                        UserDefaults.standard.set(true, forKey: AppState.email_connected.rawValue)
                                        UserDefaults.standard.set(true, forKey: AppState.gmail_connected.rawValue)
                                        UserDefaults.standard.synchronize()
                                        let vc = EditProfileViewController.instantiate(fromAppStoryboard: .UserAuthentication)
                                        self.show(vc, sender: true)
                                    }
                                    else {
                                        self.view.makeToast("Something went wrong. Please try again", duration: 3.0, position: .top)
                                        print("something went wrong")
                                    }
                                }
                            }
                            else {
                                //dismiss only
                                //no action
                            }
                        }
                        else {
                            if status == true {
                                //dismiss only
                                //no action
                                //this is the case modify is pressed
                            }
                            else {
                                //not possible
                            }
                        }
                    }
                    
                    UIView.animate(withDuration: 0.1) {
                                                                
                        vc.backgroundView.alpha = 0.5
                    }
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
