//
//  OTPViewController.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 12/11/2020.
//

import UIKit
import FirebaseAuth
import Toast_Swift
import MBProgressHUD

class OTPViewController: UIViewController, Navigator {
    
    @IBOutlet weak fileprivate var firstNumberTF: UITextField!
    @IBOutlet weak fileprivate var secondNumberTF: UITextField!
    @IBOutlet weak fileprivate var thirdNumberTF: UITextField!
    @IBOutlet weak fileprivate var fourthNumberTF: UITextField!
    @IBOutlet weak fileprivate var fiveNumberTF: UITextField!
    @IBOutlet weak fileprivate var sixNumberTF: UITextField!

    /// public variable
    var phn = ""
    var isUpdate = false
    var callBack: (()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialization()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func urlTapped(_ sender: Any) {
        if let btn = sender as? UIButton {
            var type:WebViewType = .terms
            if btn.tag == 1 {
                //terms tapped
                type = .terms
            }
            else {
                type = .privacy
            }
            
            let vc = CustomWebViewController.instantiate(fromAppStoryboard: .UserAuthentication)
            vc.type = type
            self.show(vc, sender: true)
        }
    }
    
    @IBAction func resendAction(_ sender: Any) {
        resendPin()
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func verfiyAction(_ sender: Any) {
        
        guard let first = firstNumberTF.text, !first.isEmpty else {
            self.view.makeToast("Add OTP", duration: 3.0, position: .top)
            return
        }
        guard let second = secondNumberTF.text, !second.isEmpty else {
            self.view.makeToast("Please Add complete OTP", duration: 3.0, position: .top)
            return
        }
        guard let three = thirdNumberTF.text, !three.isEmpty else {
            self.view.makeToast("Please Add complete OTP", duration: 3.0, position: .top)
            return
        }
        guard let four = fourthNumberTF.text, !four.isEmpty else {
            self.view.makeToast("Please Add complete OTP", duration: 3.0, position: .top)
            return
        }
        
        
        guard let five = fiveNumberTF.text, !five.isEmpty else {
            self.view.makeToast("Please Add complete OTP", duration: 3.0, position: .top)
            return
        }
        guard let six = sixNumberTF.text, !six.isEmpty else {
            self.view.makeToast("Please Add complete OTP", duration: 3.0, position: .top)
            return
        }
        
        
        let otpNumber = "\(first)\(second)\(three)\(four)\(five)\(six.trimmingCharacters(in: .whitespaces))"
        verifyOTP(otp: otpNumber)
    }
    
    func resendPin () {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        PhoneAuthProvider.provider().verifyPhoneNumber(phn, uiDelegate: nil) {[weak self] (verificationID, error) in
            guard let self = self else {print("no self");return}
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if let error = error {
                self.view.makeToast(error.localizedDescription, duration: 3.0, position: .top)
                
                return
            }
            ///save id in user defualt
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
        }
    }
}
//MARK:- initialization
fileprivate extension OTPViewController  {
    
    func initialization (){
        setDelegate()

    }
    
    private func setDelegate(){
        firstNumberTF.delegate = self
        secondNumberTF.delegate = self
        thirdNumberTF.delegate = self
        fourthNumberTF.delegate = self
        fiveNumberTF.delegate = self
        sixNumberTF.delegate = self
        
        firstNumberTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        secondNumberTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        thirdNumberTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        fourthNumberTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        fiveNumberTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        sixNumberTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        (self.view.viewWithTag(1001) as! UITextField).becomeFirstResponder()
    }
}
//MARK:- TextField Delegates
fileprivate extension OTPViewController{
    func verifyOTP(otp : String){
        
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID ?? "",verificationCode: otp)
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        if isUpdate {
            
            Auth.auth().currentUser?.updatePhoneNumber(credential, completion: { (error) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    // Handles error
                    self.view.makeToast(error.localizedDescription, duration: 3.0, position: .top)
                    return
                }
                if let call = self.callBack {
                    call()
                }
                self.navigationController?.popViewController(animated: true)
            })
        }
        else {
            Auth.auth().signIn(with: credential) {[weak self] authData, error in
                guard let self = self else {print("no self");return}
                
                
                if let error = error {
                    // Handles error
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.view.makeToast(error.localizedDescription, duration: 3.0, position: .top)
                    return
                }
                print("all good. move to next screen")
                let userID = authData?.user.uid ?? ""
                UserDefaults.standard.set(userID, forKey: "UserId")
                
                ///go to vafiftion code screen
                self.checkUserExistsAndDecideNavigation(userID: userID, viewController: self) {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }
        }
        
    }
}
//MARK:- TextField Delegates
extension OTPViewController: UITextFieldDelegate {
    
    //MARK: - UITextFieldDelegate
    @objc func textFieldDidChange(_ textField: UITextField) {
        let tag = textField.tag
       // textField.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        if tag == 1006 {
            
            var verificationCode = firstNumberTF.text! + secondNumberTF.text! + thirdNumberTF.text!
            verificationCode = verificationCode + fourthNumberTF.text! + fiveNumberTF.text! + sixNumberTF.text!.trimmingCharacters(in: .whitespaces)
            
            if verificationCode.count == 6 {
              //  verifyButton.isUserInteractionEnabled = true
              //  verifyButton.alpha = 1
                
            } else {
              //  verifyButton.isUserInteractionEnabled = false
             //   verifyButton.alpha = 0.5
            }
            
        } else {
            let text = textField.text!
            
            if text != "" {
                let lastChar = String(describing: Array(text)[text.count - 1])
                textField.text = lastChar + ""
                (self.view.viewWithTag(tag+1) as! UITextField).isUserInteractionEnabled = true
                (self.view.viewWithTag(tag+1) as! UITextField).becomeFirstResponder()
                (self.view.viewWithTag(tag+1) as! UITextField).text = " "
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            //textField.backgroundColor = #colorLiteral(red: 0.9098039216, green: 0.9098039216, blue: 0.9098039216, alpha: 1)
           // verifyButton.isUserInteractionEnabled = false
         //   verifyButton.alpha = 0.5
            textField.isUserInteractionEnabled =  true
            textField.text = ""
           // invalidCodeLabel.isHidden = true
            
            if textField.tag != 1001
            {
                (self.view.viewWithTag(textField.tag-1) as! UITextField).becomeFirstResponder()
                return false
            }
        }
       // let bool = maxLengthForTextfield(textfield: textField, range: range, string: string, length: 1)
    
        return true && (sixNumberTF.text?.count ?? 0) <= 1
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func maxLengthForTextfield(textfield: UITextField, range: NSRange, string: String, length: Int) -> Bool {
        let currentText = textfield.text ?? ""
        guard let strRange = Range(range, in: currentText) else { return false }
        let newString = currentText.replacingCharacters(in: strRange, with: string)
        
        return newString.count <= length
    }
}
