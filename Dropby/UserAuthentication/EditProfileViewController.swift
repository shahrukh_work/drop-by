//
//  EditProfileViewController.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 17/11/2020.
//

import UIKit
import MBProgressHUD
import SDWebImage

class EditProfileViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    @IBOutlet weak var nextBtn: DesignableButton!
    @IBOutlet weak var avatarCollectionView: UICollectionView!
    @IBOutlet weak var avatarView: DesignableView!
    @IBOutlet weak var firstName: DesignableTextFeild!
    @IBOutlet weak var lastName: DesignableTextFeild!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var avatarSelection: UIImageView!
    var selectedAvatarName: String = ""
    var editMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        avatarCollectionView.delegate = self
        avatarCollectionView.dataSource = self
        //        avatarCollectionView.register(AvatarCell.self, forCellWithReuseIdentifier: "avatarCell")
        
        firstName.text = ProfileHandler.shared.profileModel.userInfoModel.firstName
        lastName.text = ProfileHandler.shared.profileModel.userInfoModel.lastName
        
        updateAvatarSelection()
        // Do any additional setup after loading the view.
        avatarView.isHidden = true
        if editMode {
            self.prefilProfileForEdit()
        }
        else {
            let profileLink = ProfileHandler.shared.profileModel.linkModel.photoUrl
            profilePicture.sd_setImage(with: URL(string: profileLink), placeholderImage: nil, options: .refreshCached, context: nil)
            
//            profilePicture.sd_setImage(with: URL(string: profileLink)) { (image, error, SDImageCacheTypeAll, url) in
//                print("image set")
//            }
        }
    }
    
    func prefilProfileForEdit() {
        
        self.nextBtn.setTitle("Update", for: UIControl.State())
        self.firstName.isEnabled = false
        self.lastName.isEnabled = false
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let destinationPath = URL(fileURLWithPath: documentsPath).appendingPathComponent("filename.png")
        
        do {
            let imageData = try Data(contentsOf: destinationPath)
            self.profilePicture.image = UIImage(data: imageData)
        } catch {
            print("Error loading image : \(error)")
        }
        
        profilePicture.sd_setImage(with: URL(string: ProfileHandler.shared.profileModel.userInfoModel.profilePicURL), placeholderImage: nil, options: .refreshCached, context: nil)

        selectedAvatarName = ProfileHandler.shared.profileModel.userInfoModel.avatarId
    }
    
    func updateAvatarSelection() {
        selectedAvatarName = ProfileHandler.shared.profileModel.userInfoModel.avatarId
        if !selectedAvatarName.isEmpty {
            avatarSelection.image = UIImage.init(named: "avatar_"+selectedAvatarName)
        }
        
        //profile image
        
    }
    
    @IBAction func chooseAvatarAction(_ sender: Any) {
        UIView.animate(withDuration: 0.5) {
            self.avatarView.alpha = 1
        }
        avatarCollectionView.reloadData()
    }
    
    @IBAction func uploadPictureAction(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func isValidProfileData() -> Bool {
        guard let _ = profilePicture.image else {
            self.view.makeToast("Please add image.", duration: 3.0, position: .top)
            return false
        }
        if ((firstName.text ?? "").isReallyEmpty || (firstName.text ?? "").isEmpty) {
            self.view.makeToast("First name cannot be empty.", duration: 3.0, position: .top)
            return false
        }
        if ((lastName.text ?? "").isReallyEmpty || (lastName.text ?? "").isEmpty) {
            self.view.makeToast("Last name cannot be empty.", duration: 3.0, position: .top)
            return false
        }
        return true
    }
    @IBAction func nextBtnAction(_ sender: Any) {
        
        guard isValidProfileData() == true else {
            return
        }
         
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ProfileHandler.shared.profileModel.userInfoModel.firstName = firstName.text ?? ""
        ProfileHandler.shared.profileModel.userInfoModel.lastName = lastName.text ?? ""
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let destinationPath = URL(fileURLWithPath: documentsPath).appendingPathComponent("filename.png")

        debugPrint("destination path is",destinationPath)

        do {
            try profilePicture.image?.pngData()?.write(to: destinationPath)
        } catch {
            debugPrint("writing file error", error)
        }
        
        //upload to server
        ProfileHandler.shared.authInteractor.uploadPictureToStorage(url: destinationPath) { (urlString, status) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if status == true && urlString != nil {
                //all good
                ProfileHandler.shared.profileModel.userInfoModel.profilePicURL = urlString!
                self.saveInTOFireBase()
                
//                if self.editMode == true {
//                    //do nothing
//                }
//                else {
//                    ProfileHandler.shared.profileModel.userInfoModel.profilePicURL = urlString!
//                    self.saveInTOFireBase()
//                }
            }
            else {
                
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            
            profilePicture.image = pickedImage.resized(toWidth: 200)
            profilePicture.contentMode = .scaleAspectFit
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    private func saveInTOFireBase(){
        ///Adding into Profile model
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ProfileHandler.shared.synchroniseProfileData { (success) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            ///TODO:- show success popup
            if self.editMode == false {
                if success == true {
                    UserDefaults.standard.set(true, forKey: AppState.profile_completed.rawValue)
                    UserDefaults.standard.synchronize()
                    let vc = MyTagsViewController.instantiate(fromAppStoryboard: .UserAuthentication)
                    self.show(vc, sender: true)
                }
                else {
    //                guard let self = self else {print("no self ");return}
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.view.makeToast("Error in connection", duration: 3.0, position: .top)
                }
            }
        }
    }
}

extension EditProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "avatarCell", for: indexPath) as? AvatarCell {
            
            let imageNumber = String(indexPath.item + 1)
            var imageName = "avatar_"+String(imageNumber)
            
            if imageNumber == selectedAvatarName {
                imageName = imageName+"_selected"
            }
            cell.avatarImage.image = UIImage.init(named:imageName)
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedAvatarName = String(indexPath.item+1)
        ProfileHandler.shared.profileModel.userInfoModel.avatarId = selectedAvatarName
        updateAvatarSelection()
        UIView.animate(withDuration: 0.2) {
            self.avatarView.alpha = 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = UIScreen.main.bounds.width
        let sizeForItem = ((width*0.7)/3) - 25
        return CGSize(width: sizeForItem, height: sizeForItem)
    }
    
}

class AvatarCell: UICollectionViewCell {
    
    @IBOutlet weak var avatarImage: UIImageView!
}

extension UIImage {
    func resized(withPercentage percentage: CGFloat, isOpaque: Bool = true) -> UIImage? {
        let canvas = CGSize(width: size.width * percentage, height: size.height * percentage)
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: canvas, format: format).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
    func resized(toWidth width: CGFloat, isOpaque: Bool = true) -> UIImage? {
        let canvas = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: canvas, format: format).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
}
