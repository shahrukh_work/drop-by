//
//  CustomWebViewController.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 12/11/2020.
//

import UIKit
import WebKit
import MBProgressHUD

class CustomWebViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {

    @IBOutlet weak var webView: WKWebView!
    var type: WebViewType?
    @IBOutlet weak var backBtn: UIButton!
    var shouldHideBackBtn = false
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.uiDelegate = self
        webView.navigationDelegate = self
        loadWebviewWithType(receivedType: type ?? .terms)
        backBtn.isHidden = shouldHideBackBtn
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.tabBarController?.tabBar.isHidden = true
        //self.navigationItem.hidesBackButton = true

    }
    
    private func loadWebviewWithType(receivedType: WebViewType) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        APIManager.shared.getWebContent(type: receivedType) { (htmlString) in
            
            if htmlString != nil {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.webView.loadHTMLString(htmlString!, baseURL: nil)
                    self.webView.allowsBackForwardNavigationGestures = true
                }
            }
        }
    }
        
    @IBAction func backBtnAction(_ sender: Any) {

        self.navigationController?.popToRootViewController(animated: true)
//        self.parent?.dismiss(animated: true, completion: nil)
    }
    
    
}
