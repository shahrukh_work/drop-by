//
//  DetailEditProfile.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 16/04/2021.
//

import UIKit
import TagListView
import MBProgressHUD

class DetailEditProfile: UIViewController {

    @IBOutlet weak var avatarCollectionView: UICollectionView!
    @IBOutlet weak var avatarView: DesignableView!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var instituteNameTextField: UITextField!
    @IBOutlet weak var addTagTextFields: UITextField!
    @IBOutlet weak var tagsList: TagListView!
    @IBOutlet weak var avatarStackView: UIStackView!
    var selectedAvatarName: String = ""
    var allTags = [String]()
    
    @IBOutlet weak var bioTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        avatarStackView.isHidden = true
        // Do any additional setup after loading the view.
        avatarCollectionView.delegate = self
        avatarCollectionView.dataSource = self
        tagsList.delegate = self
        updateAvatarSelection()
        prefilProfileForEdit()
    }
    
    @IBAction func addTag(_ sender: Any) {
        if allTags.count == 5 {
            self.view.makeToast("Maximum 5 tags are allowed", duration: 3.0, position: .top)
            return
        }
        guard let tag = addTagTextFields.text, !tag.isEmpty else {
            self.view.makeToast("Empty tag cannot be added", duration: 3.0, position: .top)
            return
        }
        
        allTags.append(addTagTextFields.text!)
        tagsList.addTag(tag)
        addTagTextFields.text = ""
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    @IBAction func uploadPicture(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func chooseAvatar(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5) {
            self.avatarView.alpha = 1
        }
        avatarCollectionView.reloadData()
    }
    
    @IBAction func saveProfile(_ sender: Any) {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ProfileHandler.shared.profileModel.userInfoModel.firstName = firstNameTextField.text ?? ""
        ProfileHandler.shared.profileModel.userInfoModel.lastName = lastNameTextField.text ?? ""
        ProfileHandler.shared.profileModel.userInfoModel.instituteName = instituteNameTextField.text ?? ""
        ProfileHandler.shared.profileModel.userInfoModel.basicInfo = bioTextView.text ?? ""
        ProfileHandler.shared.profileModel.tagsModel = TagsModel(tags: allTags)
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let destinationPath = URL(fileURLWithPath: documentsPath).appendingPathComponent("filename.png")

        debugPrint("destination path is",destinationPath)

        do {
            try profilePictureImageView.image?.pngData()?.write(to: destinationPath)
        } catch {
            debugPrint("writing file error", error)
        }
        
        //upload to server
        ProfileHandler.shared.authInteractor.uploadPictureToStorage(url: destinationPath) { (urlString, status) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if status == true && urlString != nil {
                //all good
                ProfileHandler.shared.profileModel.userInfoModel.profilePicURL = urlString!
                self.saveInTOFireBase()
            }
            else {
                
            }
        }
    }
    
    private func saveInTOFireBase(){
        ///Adding into Profile model
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ProfileHandler.shared.synchroniseProfileData { (success) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            ///TODO:- show success popup
            if success == true {
                self.view.makeToast("Profile updated successfully!", duration: 3.0, position: .top)
            }
            else {
//                guard let self = self else {print("no self ");return}
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.makeToast("Error in connection", duration: 3.0, position: .top)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func updateAvatarSelection() {
        selectedAvatarName = ProfileHandler.shared.profileModel.userInfoModel.avatarId
        if !selectedAvatarName.isEmpty {
            avatarImageView.image = UIImage.init(named: "avatar_"+selectedAvatarName)
        }
    }
    
    func prefilProfileForEdit() {
             
        firstNameTextField.text = ProfileHandler.shared.profileModel.userInfoModel.firstName
        lastNameTextField.text = ProfileHandler.shared.profileModel.userInfoModel.lastName
        instituteNameTextField.text = ProfileHandler.shared.profileModel.userInfoModel.instituteName
        bioTextView.text = ProfileHandler.shared.profileModel.userInfoModel.basicInfo
        let tags = ProfileHandler.shared.profileModel.tagsModel.tagsList
        allTags = tags
        tagsList.addTags(allTags)
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let destinationPath = URL(fileURLWithPath: documentsPath).appendingPathComponent("filename.png")
        
        do {
            let imageData = try Data(contentsOf: destinationPath)
            self.profilePictureImageView.image = UIImage(data: imageData)
        } catch {
            print("Error loading image : \(error)")
        }
        
        profilePictureImageView.sd_setImage(with: URL(string: ProfileHandler.shared.profileModel.userInfoModel.profilePicURL), placeholderImage: nil, options: .refreshCached, context: nil)

        selectedAvatarName = ProfileHandler.shared.profileModel.userInfoModel.avatarId
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    
}

extension DetailEditProfile: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            
            profilePictureImageView.image = pickedImage.resized(toWidth: 200)
            profilePictureImageView.contentMode = .scaleAspectFit
        }
        picker.dismiss(animated: true, completion: nil)
    }    
}

extension DetailEditProfile: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "avatarCell", for: indexPath) as? AvatarCell {
            
            let imageNumber = String(indexPath.item + 1)
            var imageName = "avatar_"+String(imageNumber)
            
            if imageNumber == selectedAvatarName {
                imageName = imageName+"_selected"
            }
            cell.avatarImage.image = UIImage.init(named:imageName)
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedAvatarName = String(indexPath.item+1)
        ProfileHandler.shared.profileModel.userInfoModel.avatarId = selectedAvatarName
        updateAvatarSelection()
        UIView.animate(withDuration: 0.2) {
            self.avatarView.alpha = 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = UIScreen.main.bounds.width
        let sizeForItem = ((width*0.7)/3) - 25
        return CGSize(width: sizeForItem, height: sizeForItem)
    }
    
}

extension DetailEditProfile: TagListViewDelegate {
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        tagsList.removeTag(title)
        if let index = allTags.firstIndex(of: title) {
            allTags.remove(at: index)
        } else {
            // not found
        }
    }
}
