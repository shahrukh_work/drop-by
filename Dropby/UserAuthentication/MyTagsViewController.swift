//
//  MyTagsViewController.swift
//  
//
//  Created by Junaid Mukhtar on 17/11/2020.
//

import UIKit
import TagListView
import MBProgressHUD

class MyTagsViewController: UIViewController, Navigator {

    @IBOutlet weak private var tagListView: TagListView!
    @IBOutlet private weak var tagText: UITextField!
    var allTags = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inilization()
    }
    
    @IBAction func doneTapped(_ sender: Any) {
                
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ProfileHandler.shared.profileModel.tagsModel = TagsModel(tags: allTags)
        ProfileHandler.shared.checkEmailVerified { (status) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if status {
                ProfileHandler.shared.profileModel.visibilityModel.status = VisibilityStatus.Everyone.rawValue
            }
            else {
                ProfileHandler.shared.profileModel.visibilityModel.status = VisibilityStatus.Friends.rawValue
            }
            self.saveInTOFireBase()
        }
    }
    
    
    @IBAction private func addTag(_ sender: Any){
        
        guard let tag = tagText.text, !tag.isEmpty else {
            self.view.makeToast("Empty tag cannot be added", duration: 3.0, position: .top)
            return
        }
        if allTags.count > 4
        {
            self.view.makeToast("Maximum Five Tags can be added", duration: 3.0, position: .top)
        }
        else
        {
            allTags.append(tagText.text!)
            tagListView.addTag(tag)
            tagText.text = ""
        }
    }

   
}

//MARK:- tagCheck method




//MARK:- Basic Method
private extension MyTagsViewController {
    func inilization(){
        setTagSeting()
    }
    private func setTagSeting(){
//        if !ProfileHandler.shared.profileModel.tagsModel.tagsList.isEmpty {
//            tagListView.addTags(ProfileHandler.shared.profileModel.tagsModel.tagsList)
//        }
        
        tagListView.alignment = .center
        tagListView.delegate = self
    }
}
//MARK:- saveInTOFireBase
private extension MyTagsViewController {
    private func saveInTOFireBase(){
        ///Adding into Profile model
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ProfileHandler.shared.synchroniseProfileData { (success) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if success == true {
                UserDefaults.standard.set(true, forKey: AppState.tags_added.rawValue)
                UserDefaults.standard.synchronize()
                self.goToHome()
            }
            else {
//                guard let self = self else {print("no self ");return}
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.makeToast("Error in connection", duration: 3.0, position: .bottom)
            }
        }
        ProfileHandler.shared.authInteractor.loadAllTables()
    }
}
//MARK:- TagListViewDelegate
extension MyTagsViewController : TagListViewDelegate{
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        tagListView.removeTag(title)
        if let index = allTags.firstIndex(of: title) {
            allTags.remove(at: index)
        } else {
            // not found
        }
    }
}
//MARK:- TOAST Message
extension UIViewController {

func showToast(message : String, font: UIFont) {

    let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 90, y: self.view.frame.size.height-100, width: 200, height: 35))
    toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    toastLabel.textColor = UIColor.white
    toastLabel.font = font
    toastLabel.textAlignment = .center;
    toastLabel.text = message
    toastLabel.alpha = 1.0
    toastLabel.layer.cornerRadius = 10;
    toastLabel.clipsToBounds  =  true
    self.view.addSubview(toastLabel)
    UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
         toastLabel.alpha = 0.0
    }, completion: {(isCompleted) in
        toastLabel.removeFromSuperview()
    })
} }
