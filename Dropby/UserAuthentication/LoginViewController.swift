//
//  LoginViewController.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 12/11/2020.
//

import UIKit
import FirebaseAuth
import Toast_Swift
import MBProgressHUD
import ADCountryPicker

class LoginViewController: UIViewController {

    @IBOutlet weak var showCountryCodesBtn: DesignableButton!
    @IBOutlet private weak var phoneTF: UITextField!
    let countryCodes = ["+1","+92"]
    var selectedCountryCode = "+1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func loginAction(_ sender: Any) {
        
        guard let phone = phoneTF.text, !phone.isEmpty else {
            self.view.makeToast("Please Add phone number", duration: 3.0, position: .top)
            return
        }
        
        
        if phone.isPhoneNumber == false{
            self.view.makeToast("Phone number is not correct", duration: 3.0, position: .top)
            return
        }
        var finalNumber = showCountryCodesBtn.title(for: UIControl.State()) ?? ""
        finalNumber = finalNumber + phone
        sendCodeToNumber(phone: finalNumber)
    }
    
    @IBAction func urlTapped(_ sender: Any) {
        if let btn = sender as? UIButton {
            var type:WebViewType = .terms
            if btn.tag == 1 {
                //terms tapped
                type = .terms
            }
            else {
                type = .privacy
            }
            
            let vc = CustomWebViewController.instantiate(fromAppStoryboard: .UserAuthentication)
            vc.type = type
            self.show(vc, sender: true)
        }
    }
    
    @IBAction func showCountryCodesAction(_ sender: Any) {
        
        let picker = ADCountryPicker()
        picker.showCallingCodes = true
        picker.defaultCountryCode = "US"
        picker.font = UIFont(name: "Roboto-Regular", size: 15)

        // or closure

        picker.didSelectCountryWithCallingCodeClosure = { name, code, dialCode in
                print(dialCode)
            self.selectedCountryCode = dialCode // selected item
            self.showCountryCodesBtn.setTitle(self.selectedCountryCode, for: UIControl.State())
            self.showCountryCodesBtn.titleLabel?.text = self.selectedCountryCode
            picker.dismiss(animated: true, completion: nil)
        }
        
        let pickerNavigationController = UINavigationController(rootViewController: picker)
        self.present(pickerNavigationController, animated: true, completion: nil)
        
//        let vc = CountryCodeViewController.instantiate(fromAppStoryboard: .UserAuthentication)
//        self.navigationController?.show(vc, sender: nil)
        
//        pickerContainer.isHidden = false
    }
}
//MARK:- sendCodeToNumber
fileprivate extension LoginViewController {
    func sendCodeToNumber(phone : String){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) {[weak self] (verificationID, error) in
            guard let self = self else {print("no self");return}
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if let error = error {
                self.view.makeToast(error.localizedDescription, duration: 3.0, position: .top)
                
                return
            }
            ///save id in user defualt
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            ///go to vafiftion code screen
            let vc = OTPViewController.instantiate(fromAppStoryboard: .UserAuthentication)
            vc.phn = phone
            self.show(vc, sender: true)
            
        }
    }
}

extension LoginViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryCodes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countryCodes[row] // dropdown item
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedCountryCode = countryCodes[row] // selected item
        showCountryCodesBtn.setTitle(selectedCountryCode, for: UIControl.State())
        showCountryCodesBtn.titleLabel?.text = selectedCountryCode
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view as? UILabel { label = v }
        label.font = UIFont (name: "Roboto-Bold", size: 13)
        label.text =  countryCodes[row]
        label.textAlignment = .center
        return label
    }
}
