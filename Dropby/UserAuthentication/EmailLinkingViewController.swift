//
//  EmailLinkingViewController.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 16/11/2020.
//

import UIKit
import GoogleSignIn
import WebKit
import MBProgressHUD

class EmailLinkingViewController: UIViewController {
    
    @IBOutlet weak var googleSignInBtn: DesignableButton!
    var webView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inilization()
        showDisclaimer()
    }
    
    func showDisclaimer() {
        let vc = DisclaimerPopup.instantiate(fromAppStoryboard: .PopUp)
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true) {
         
            UIView.animate(withDuration: 0.1) {
                                                        
                vc.backgroundView.alpha = 0.5
            }
        }
    }
    
    @IBAction func googleSigninAction(_ sender: Any) {
        //GIDSignIn.sharedInstance.signIn(with: <#GIDConfiguration#>, presenting: <#UIViewController#>)
    }
    
    @IBAction func linkEmailAction(_ sender: Any) {
        let vc = EmailVerificationVC.instantiate(fromAppStoryboard: .UserAuthentication)
        self.show(vc, sender: true)
    }
    
    @IBAction func linkedInLoginBtnAction(_ sender: UIButton) {
        linkedInAuthVC()
    }
    @IBAction func skipAction(_ sender: Any) {
        navigateToNextScreen()
    }
    func navigateToNextScreen() {
        let vc = EditProfileViewController.instantiate(fromAppStoryboard: .UserAuthentication)
        self.show(vc, sender: true)
    }
}
//MARK:- inilization
extension EmailLinkingViewController {
    func  inilization() {
        googleDeleget()

    }
    private func googleDeleget(){
        //GIDSignIn.sharedInstance.presentingViewController = self
        //GIDSignIn.sharedInstance.delegate = self
    }
}
//MARK:- Linkdin Methods
extension EmailLinkingViewController {
    func linkedInAuthVC() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        // Create linkedIn Auth ViewController
        let linkedInVC = UIViewController()
        // Create WebView
        let webView = WKWebView()
        webView.navigationDelegate = self
        linkedInVC.view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: linkedInVC.view.topAnchor),
            webView.leadingAnchor.constraint(equalTo: linkedInVC.view.leadingAnchor),
            webView.bottomAnchor.constraint(equalTo: linkedInVC.view.bottomAnchor),
            webView.trailingAnchor.constraint(equalTo: linkedInVC.view.trailingAnchor)
        ])
        
        let state = "linkedin\(Int(NSDate().timeIntervalSince1970))"
        
        let authURLFull = LinkedInConstants.AUTHURL + "?response_type=code&client_id=" + LinkedInConstants.CLIENT_ID + "&scope=" + LinkedInConstants.SCOPE + "&state=" + state + "&redirect_uri=" + LinkedInConstants.REDIRECT_URI
        
        
        let urlRequest = URLRequest.init(url: URL.init(string: authURLFull)!)
        webView.load(urlRequest)
        
        // Create Navigation Controller
        let navController = UINavigationController(rootViewController: linkedInVC)
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelAction))
        linkedInVC.navigationItem.leftBarButtonItem = cancelButton
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(self.refreshAction))
        linkedInVC.navigationItem.rightBarButtonItem = refreshButton
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navController.navigationBar.titleTextAttributes = textAttributes
        linkedInVC.navigationItem.title = "linkedin.com"
        navController.navigationBar.isTranslucent = false
        navController.navigationBar.tintColor = UIColor.white
        navController.navigationBar.barTintColor = UIColor.black
        navController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        navController.modalTransitionStyle = .coverVertical
        
        self.present(navController, animated: true, completion: nil)
    }
    @objc func cancelAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func refreshAction() {
        self.webView.reload()
    }
}
//MARK:-GIDSignInDelegate
//extension EmailLinkingViewController: GIDSignInDelegate {
//    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
//
//        if (error == nil) {
//            // Perform any operations on signed in user here.
//            // ...
//            print("\(String(describing: user.profile?.description))")
//            DispatchQueue.main.async {
//
//                MBProgressHUD.showAdded(to: self.view, animated: true)
//            }
//
////            ProfileHandler.shared.updateModelWithGoogleModel(userModel: user)
//            ProfileHandler.shared.synchroniseProfileData() { (status) in
//
//                MBProgressHUD.hide(for: self.view, animated: true)
//
//                if status == true {
//                    //save current
//                    UserDefaults.standard.set(true, forKey: AppState.email_connected.rawValue)
//                    UserDefaults.standard.set(true, forKey: AppState.gmail_connected.rawValue)
//                    UserDefaults.standard.synchronize()
//                    //open profile screen
//                    self.navigateToNextScreen()
//                }
//                else {
//                    //something went wrong
//                    self.view.makeToast(error?.localizedDescription, duration: 3.0, position: .bottom)
//                    print("something went wrong")
//                }
//            }
//
//        } else {
//            print("\(error.localizedDescription)")
//        }
//    }
//
    private func saveInTOFireBase(){
        ///Adding into Profile model
        
//        guard let userID = UserDefaults.standard.string(forKey: "UserId") else {print("No userId");return}
//
//        let manager = AuthenticationInteractor.init(manager: AuthenticationFirebase(userID: userID)).manager
//        DispatchQueue.main.async {
//
//            MBProgressHUD.showAdded(to: self.view, animated: true)
//
//        }
//
//        manager.add(profile: ProfileHandler.shared.profileModel) { [weak self] in
//            guard let self = self else {print("no self ");return}
//            MBProgressHUD.hide(for: self.view, animated: true)
//
//
//            self.navigateToNextScreen()
//
//        } onError: { [weak self] (error)  in
//            guard let self = self else {print("no self ");return}
//            MBProgressHUD.hide(for: self.view, animated: true)
//
//            self.view.makeToast(error.localizedDescription, duration: 3.0, position: .bottom)
//        }
//
//    }
//
}
extension EmailLinkingViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        RequestForCallbackURL(request: navigationAction.request)
        
        //Close the View Controller after getting the authorization code
        if let urlStr = navigationAction.request.url?.absoluteString {
            if urlStr.contains("?code=") {
                self.dismiss(animated: true, completion: nil)
            }
        }
        decisionHandler(.allow)
    }
    func RequestForCallbackURL(request: URLRequest) {
        // Get the authorization code string after the '?code=' and before '&state='
        let requestURLString = (request.url?.absoluteString)! as String
        if requestURLString.hasPrefix(LinkedInConstants.REDIRECT_URI) {
            if requestURLString.contains("?code=") {
                if let range = requestURLString.range(of: "=") {
                    let linkedinCode = requestURLString[range.upperBound...]
                    if let range = linkedinCode.range(of: "&state=") {
                        let linkedinCodeFinal = linkedinCode[..<range.lowerBound]
                        handleAuth(linkedInAuthorizationCode: String(linkedinCodeFinal))
                    }
                }
            }
        }
    }
    func handleAuth(linkedInAuthorizationCode: String) {
        linkedinRequestForAccessToken(authCode: linkedInAuthorizationCode)
    }
    func linkedinRequestForAccessToken(authCode: String) {
        let grantType = "authorization_code"
        
        // Set the POST parameters.
        let postParams = "grant_type=" + grantType + "&code=" + authCode + "&redirect_uri=" + LinkedInConstants.REDIRECT_URI + "&client_id=" + LinkedInConstants.CLIENT_ID + "&client_secret=" + LinkedInConstants.CLIENT_SECRET
        let postData = postParams.data(using: String.Encoding.utf8)
        let request = NSMutableURLRequest(url: URL(string: LinkedInConstants.TOKENURL)!)
        request.httpMethod = "POST"
        request.httpBody = postData
        request.addValue("application/x-www-form-urlencoded;", forHTTPHeaderField: "Content-Type")
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task: URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            let statusCode = (response as! HTTPURLResponse).statusCode
            if statusCode == 200 {
                let results = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [AnyHashable: Any]
                
                let accessToken = results?["access_token"] as! String
                print("accessToken is: \(accessToken)")
                
                let expiresIn = results?["expires_in"] as! Int
                print("expires in: \(expiresIn)")
                
                // Get user's id, first name, last name, profile pic url
                self.fetchLinkedInUserProfile(accessToken: accessToken)
            }
        }
        task.resume()
    }
    func fetchLinkedInUserProfile(accessToken: String) {
        let tokenURLFull = "https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))&oauth2_access_token=\(accessToken)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let verify: NSURL = NSURL(string: tokenURLFull!)!
        let request: NSMutableURLRequest = NSMutableURLRequest(url: verify as URL)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            if error == nil {
                let linkedInProfileModel = try? JSONDecoder().decode(LinkedInProfileModel.self, from: data!)
                
                //AccessToken
                print("LinkedIn Access Token: \(accessToken)")
                
                // LinkedIn Id
                let linkedinId: String! = linkedInProfileModel?.id
                print("LinkedIn Id: \(linkedinId ?? "")")
                
                // LinkedIn First Name
                let linkedinFirstName: String! = linkedInProfileModel?.firstName.localized.enUS
                print("LinkedIn First Name: \(linkedinFirstName ?? "")")
                
                // LinkedIn Last Name
                let linkedinLastName: String! = linkedInProfileModel?.lastName.localized.enUS
                print("LinkedIn Last Name: \(linkedinLastName ?? "")")
                
                // LinkedIn Profile Picture URL
                let linkedinProfilePic: String!
                
                /*
                 Change row of the 'elements' array to get diffrent size of the profile url
                 elements[0] = 100x100
                 elements[1] = 200x200
                 elements[2] = 400x400
                 elements[3] = 800x800
                 */
                if let pictureUrls = linkedInProfileModel?.profilePicture.displayImage.elements[2].identifiers[0].identifier {
                    linkedinProfilePic = pictureUrls
                } else {
                    linkedinProfilePic = "Not exists"
                }
                print("LinkedIn Profile Avatar URL: \(linkedinProfilePic ?? "")")
                
                // Get user's email address
                self.fetchLinkedInEmailAddress(accessToken: accessToken , linkedInProfileModel : linkedInProfileModel)
            }
        }
        task.resume()
    }
    
    func fetchLinkedInEmailAddress(accessToken: String , linkedInProfileModel : LinkedInProfileModel?) {
        let tokenURLFull = "https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))&oauth2_access_token=\(accessToken)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let verify: NSURL = NSURL(string: tokenURLFull!)!
        let request: NSMutableURLRequest = NSMutableURLRequest(url: verify as URL)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            if error == nil {
                let linkedInEmailModel = try? JSONDecoder().decode(LinkedInEmailModel.self, from: data!)

                // LinkedIn Email
                let linkedinEmail: String! = linkedInEmailModel?.elements[0].elementHandle.emailAddress
                print("LinkedIn Email: \(linkedinEmail ?? "")")
                
                //save into profile handler and save into firebase
                DispatchQueue.main.async {
                    
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                }
                ProfileHandler.shared.updateModelWithLinkedInModel(linkedInEmailModel: linkedInEmailModel, linkedInProfileModel: linkedInProfileModel)
                
                ProfileHandler.shared.synchroniseProfileData() { (status) in
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    if status == true {
                        //save current
                        UserDefaults.standard.set(true, forKey: AppState.email_connected.rawValue)
                        UserDefaults.standard.set(true, forKey: AppState.linkedin_connected.rawValue)
                        UserDefaults.standard.synchronize()
                        
                        //open profile screen
                        self.navigateToNextScreen()
                    }
                    else {
                        
                        self.view.makeToast(error?.localizedDescription, duration: 3.0, position: .top)
                        print("something went wrong")
                    }
                }
            }
        }
        task.resume()
    }
}
