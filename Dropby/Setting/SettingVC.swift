//
//  SettingVC.swift
//  Dropby
//
//  Created by Junaid on 08/12/2020.
//

import UIKit

class SettingVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let data = [SettingModel(name: "Visibility Range", icon: "Visibility Range"), SettingModel(name: "Settings" , icon : "Settings"), SettingModel(name: "Privacy Policy", icon: "Privacy Policy"), SettingModel(name: "Terms and Conditions", icon: "Terms and Conditions") , SettingModel(name: "Contact Us", icon: "Contact Us"), SettingModel(name: "Log Out", icon: "Log Out")]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationItem.hidesBackButton = true
        
        
        let newBackButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back))
        newBackButton.image = UIImage(named: "back")
        self.navigationItem.leftBarButtonItem = newBackButton
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @objc func back(sender: UIBarButtonItem) {
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromRight//animates from right to left
        self.navigationController?.view.layer.add(transition, forKey: nil)//adds the animation
        self.navigationController?.popViewController(animated: true)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = true
        self.navigationItem.hidesBackButton = true
        self.navigationItem.titleView = UIView()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }

}

//MARK:- TableView

extension SettingVC : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {return 1}
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return (data.count) + 1}
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:

            guard let cell : SettingUserCell = tableView.dequeueReusableCell(withIdentifier: "SettingUserCell", for: indexPath) as? SettingUserCell else{return UITableViewCell()}
            let model = ProfileHandler.shared.profileModel
            cell.user = model
            return cell
            
        
            

        default:
            guard let cell : SettingCell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as? SettingCell else{return UITableViewCell()}
            
            cell.model = data[indexPath.row - 1]
            cell.forwardArrowImageView.isHidden = false
            if indexPath.row == 6 {
                cell.forwardArrowImageView.isHidden = true
            }
            return cell

        }
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0:
            return 150
        default:
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > 0 {
            self.handleNavigate(model: data[indexPath.row - 1])
        }
        else {
                        
//            let vc : DetailEditProfile = DetailEditProfile.instantiate(fromAppStoryboard: .UserAuthentication)
////            vc.editMode = true
//            self.show(vc, sender: true)
            
            let profileVC = ProfileVC.instantiate(fromAppStoryboard: .DropBy)
            profileVC.comeFromSetting = true
            let userModel = ProfileHandler.shared.profileModel
            profileVC.user = userModel
            
            self.show(profileVC, sender: true)
            
        }
        
        
    }

}

//MARK:- Navigation Handling
extension SettingVC{
    
    func handleNavigate(model : SettingModel){
        
        if model.name == "Settings" {
            self.performSegue(withIdentifier: "SettingToDetails", sender: self)
        }else if model.name == "Terms and Conditions"{
            let vc = CustomWebViewController.instantiate(fromAppStoryboard: .UserAuthentication)
            vc.shouldHideBackBtn = true
            vc.type = .terms
            self.show(vc, sender: true)
//            self.performSegue(withIdentifier: "settingToTermsCondition", sender: self)
        }else if model.name == "Privacy Policy"{
            let vc = CustomWebViewController.instantiate(fromAppStoryboard: .UserAuthentication)
            vc.shouldHideBackBtn = true
            vc.type = .privacy
            self.show(vc, sender: true)
//            self.performSegue(withIdentifier: "privacyPolicySegue", sender: self)
        }else if model.name == "Contact Us"{
            self.performSegue(withIdentifier: "SettingToContactUs", sender: self)
        }else if model.name == "Visibility Range"{
            
            if let vc = UIStoryboard(name: "SettingDetail", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChangeVisibilityVC") as? ChangeVisibilityVC{
                
                vc.modalPresentationStyle =  .overFullScreen
                self.present(vc, animated: true, completion: nil)
            }
//            self.performSegue(withIdentifier: "SettingToVisibilityVC", sender: self)
            //
        }
        else if model.name == "Log Out" {
            ProfileHandler.shared.logout(view: self.view) {
                self.navigationController?.popViewController(animated: true)
            }                        
        }
        
    }
}



struct SettingModel{
    
    var name : String
    var icon : UIImage?
    
    
    init(name : String , icon : String) {
        
        self.name = name
        self.icon = UIImage(named: icon)
         
    }
}
