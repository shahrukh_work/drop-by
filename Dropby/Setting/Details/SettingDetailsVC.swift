//
//  SettingDetailsVC.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 1/26/21.
//

import UIKit

class SettingDetailsVC: UIViewController {
    
    @IBOutlet weak var emailView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title = "Settings"
        
        let newBackButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back))
        newBackButton.image = UIImage(named: "back")
        
        
        self.navigationItem.leftBarButtonItem = newBackButton
        
        let email = ProfileHandler.shared.profileModel.email
        if email.isEmpty {
            self.emailView.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationItem.hidesBackButton = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
  
    
    @objc func back(sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteAccount(_ sender: Any) {
        
        let vc = DeleteAccountVC.instantiate(fromAppStoryboard: .PopUp)
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true) {
            
            
            vc.btnActionCallback = {
                                
            }
            UIView.animate(withDuration: 0.1) {
                
                vc.backgroundView.alpha = 0.5
            }
        }
    }
    
}
