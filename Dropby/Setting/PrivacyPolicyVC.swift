//
//  PrivacyPolicyVC.swift
//  Dropby
//
//  Created by Junaid on 08/12/2020.
//

import UIKit

class PrivacyPolicyVC: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title = "Privacy Policy"
        
        let newBackButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back))
        newBackButton.image = UIImage(named: "back")
        self.navigationItem.leftBarButtonItem = newBackButton
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.tabBarController?.tabBar.isHidden = true
        self.navigationItem.hidesBackButton = true

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @objc func back(sender: UIBarButtonItem) {

        self.navigationController?.popViewController(animated: true)
    }
    
}
