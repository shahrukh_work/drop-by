//
//  ChnageNumberVC.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 2/15/21.
//

import UIKit
import Toast_Swift
import Firebase
import FirebaseAuth
import MBProgressHUD
import ADCountryPicker

class ChnageNumberVC: UIViewController {

    
    @IBOutlet weak var btnOldNumber: DesignableButton!
    @IBOutlet weak var btnNewNumber: DesignableButton!
    
    @IBOutlet weak var txtOldNumber: DesignableTextFeild!
    @IBOutlet weak var txtNewNumber: DesignableTextFeild!
    
    var selectedCountryCode = "+1"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Change Number"
        addBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    private func addBackButton(){
        let newBackButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back))
        newBackButton.image = UIImage(named: "back")
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    @objc func back(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func didTapPickCountryCode(_ sender: Any) {
        
//        pickerViewOldNumber.isHidden = false
        showCountryCodesAction(sender)
    }
    
    @IBAction func didTapPickNewNumber(_ sender: Any) {
//        pickerViewNewNumber.isHidden = false
        showCountryCodesAction(sender)
    }
    
    func showCountryCodesAction(_ sender: Any) {
        
        if let button = sender as? UIButton {
            
            let picker = ADCountryPicker()
            picker.showCallingCodes = true
            picker.defaultCountryCode = "US"
            picker.font = UIFont(name: "Roboto-Regular", size: 15)
            
            picker.didSelectCountryWithCallingCodeClosure = { name, code, dialCode in
                    print(dialCode)
                self.selectedCountryCode = dialCode // selected item
                button.setTitle(self.selectedCountryCode, for: UIControl.State())
                button.titleLabel?.text = self.selectedCountryCode
                picker.dismiss(animated: true, completion: nil)
            }
            
            let pickerNavigationController = UINavigationController(rootViewController: picker)
            self.present(pickerNavigationController, animated: true, completion: nil)
        }
    }
    
    @IBAction func didTapNextBtn(_ sender: Any) {
        
        if txtOldNumber.text == nil || txtNewNumber.text == nil {
            return
        }
        
        if txtOldNumber.text!.isEmpty {
            
            self.view.makeToast("Old number field cannot be empty", duration: 3.0, position: .top)
            return
        }
        
        if txtNewNumber.text!.isEmpty {
            
            self.view.makeToast("New number field cannot be empty", duration: 3.0, position: .top)
            return
        }
        var oldNum = btnOldNumber.title(for: UIControl.State()) ?? ""
        oldNum = oldNum + txtOldNumber.text!
        
        var newNum = btnNewNumber.title(for: UIControl.State()) ?? ""
        newNum = newNum + txtNewNumber.text!
        
        if oldNum == newNum {
            self.view.makeToast("Numbers cannot be same", duration: 3.0, position: .top)
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        PhoneAuthProvider.provider().verifyPhoneNumber(newNum, uiDelegate: nil) {[weak self] (verificationID, error) in
            guard let self = self else {print("no self");return}
            
            
            if let error = error {
                                
                self.view.makeToast(error.localizedDescription, duration: 3.0, position: .top)
                MBProgressHUD.hide(for: self.view, animated: true)
                return
            }
            MBProgressHUD.hide(for: self.view, animated: true)
            ///save id in user defualt
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            
            let vc = OTPViewController.instantiate(fromAppStoryboard: .UserAuthentication)
            vc.phn = newNum
            vc.isUpdate = true
            vc.callBack = {
                
                let vc = GenericPopup.instantiate(fromAppStoryboard: .PopUp)
                vc.modalPresentationStyle = .overFullScreen
                vc.titleText = "Phone Updated"
                vc.descriptionText = "Your phone number is updated successfully"
                vc.shouldHideOk = true
                vc.cancelButtonText = "OK"
                
                self.present(vc, animated: true) {
                    
                    
                    
                    vc.btnActionCallback = { action in
                        
                        if action == .cancel {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    UIView.animate(withDuration: 0.1) {
                        
                        vc.backgroundView.alpha = 0.5
                    }
                }
            }
            self.show(vc, sender: true)
        }
    }
    
}


//extension ChnageNumberVC: UIPickerViewDelegate, UIPickerViewDataSource {
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
//    
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        return countryCodes.count
//    }
//    
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return countryCodes[row] // dropdown item
//    }
//    
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        
//        selectedCountryCode = countryCodes[row] // selected item
//        
//        if pickerOldNumber == pickerView{
//            btnOldNumber.setTitle(selectedCountryCode, for: UIControl.State())
//            btnOldNumber.titleLabel?.text = selectedCountryCode
//            pickerViewOldNumber.isHidden = true
//
//        }else{
//            btnNewNumber.setTitle(selectedCountryCode, for: UIControl.State())
//            btnNewNumber.titleLabel?.text = selectedCountryCode
//            pickerViewNewNumber.isHidden = true
//        }
//    }
//    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
//        var label = UILabel()
//        if let v = view as? UILabel { label = v }
//        label.font = UIFont (name: "Roboto-Bold", size: 13)
//        label.text =  countryCodes[row]
//        label.textAlignment = .center
//        return label
//    }
//}
