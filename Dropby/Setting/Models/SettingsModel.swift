//
//  SettingsModel.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 04/03/2021.
//

import UIKit

class SettingsModel: NSObject {

    var visibilityRadius    = ""
    var notifications       = NotificationSettings()
    
    init(visibilityRadius: String, notifyModel: NotificationSettings) {
        self.visibilityRadius   = visibilityRadius
        self.notifications      = notifyModel
    }
    
    convenience override init(){
        self.init(visibilityRadius: "", notifyModel: NotificationSettings())
    }
}

extension SettingsModel {
    class func mapper(snapshot: [String:Any]) -> SettingsModel? {
                
        let noti        = NotificationSettings.mapper(snapshot: snapshot["notifications"] as! [String : Any]) ?? NotificationSettings()
        let visibililty = snapshot["visibilityRadius"] as? String ?? ""
        
        return SettingsModel.init(visibilityRadius: visibililty, notifyModel: noti)
    }
    class func toDict(settingModel: SettingsModel) -> [String: Any] {
        
        var dict = [String: Any]()
        dict["notifications"]       = NotificationSettings.toDict(notModel: settingModel.notifications)
        dict["visibilityRadius"]    = settingModel.visibilityRadius
        dict["personalUserId"]      = ProfileHandler.shared.profileModel.userInfoModel.userId
        
        return dict
    }
}



class NotificationSettings : NSObject {
    
    var connectionRequests  = true
    var messages            = true
    var pause               = false
    
    init(connectionRequests: Bool, messages: Bool, pause: Bool) {
        self.connectionRequests     = connectionRequests
        self.messages               = messages
        self.pause                  = pause
    }
    
    convenience override init(){
        self.init(connectionRequests: true, messages: true, pause: false)
    }
    
}

extension NotificationSettings {
    class func mapper(snapshot: [String: Any]) -> NotificationSettings? {
                
        let conRequests = snapshot["connectionRequests"] as? Bool ?? true
        let messages    = snapshot["messages"] as? Bool ?? true
        let pause       = snapshot["pause"] as? Bool ?? false
        
        return NotificationSettings.init(connectionRequests: conRequests, messages: messages, pause: pause)
    }
    
    class func toDict(notModel: NotificationSettings) -> [String: Any] {
        
        var dict = [String: Bool]()
        dict["connectionRequests"]  = notModel.connectionRequests
        dict["messages"]            = notModel.messages
        dict["pause"]               = notModel.pause
        return dict
    }
}
