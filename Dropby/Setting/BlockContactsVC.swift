//
//  BlockContactsVC.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 2/7/21.
//

import UIKit
import MBProgressHUD

class BlockContactsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var blockedContacts = [GenericUserModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Block Contacts"
        
        addBackButton()

        tableView.delegate = self
        tableView.dataSource = self
        fetchBlockedList()
    }
    
    private func addBackButton(){
        let newBackButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back))
        newBackButton.image = UIImage(named: "back")
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    @objc func back(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func fetchBlockedList() {
        
        self.blockedContacts.removeAll()
        MBProgressHUD.showAdded(to: self.view, animated: true)
        APIManager.shared.fetchBlockList { (dataObject) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                if dataObject != nil {
                    
                    for contact in dataObject! {
                        let userObject = GenericUserModel.mapper(snapshot: contact) ?? GenericUserModel()
                        self.blockedContacts.append(userObject)
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }

}

extension BlockContactsVC : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.blockedContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "BlockContactCell", for: indexPath) as? BlockContactCell{
            
            let model = blockedContacts[indexPath.row]
            cell.profileModel = model
            cell.callback = {
                self.fetchBlockedList()
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
    
}
