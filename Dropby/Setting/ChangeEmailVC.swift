//
//  ChangeEmailVC.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 2/15/21.
//

import UIKit
import MBProgressHUD
import Firebase

class ChangeEmailVC: UIViewController {

    @IBOutlet weak var emailAddressTF: UITextField!
    var originalEmailAddress = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Manage account"
        self.addBackButton()
        self.emailAddressTF.text = ProfileHandler.shared.profileModel.email
        originalEmailAddress = ProfileHandler.shared.profileModel.email
    }
    
    @IBAction func saveBtnTapped(_ sender: Any) {
        
        if self.emailAddressTF.text != nil && originalEmailAddress != self.emailAddressTF.text {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            APIManager.shared.checkEmailAllowed(email: self.emailAddressTF.text!) { (object) in
                
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                            
                    var status = false
                    if object != nil {
                        status = object!["status"] as? Bool ?? false
                    }
                                    
                    let vc = VerifyAccountPopup.instantiate(fromAppStoryboard: .PopUp)
                    vc.modalPresentationStyle = .overFullScreen
                    vc.descriptionTxt = "You need to verify your email. A verification link has been sent to email you shared."
                    vc.imageViewHidden = true
                    vc.cancelBtnTitle = "MODIFY"
                    vc.okBtnTitle = "OK"
                    
                    if status == true {
                        //allowed - show verification popup
                        vc.descriptionTxt = "You need to verify your email. A verification link has been sent to email you shared."
                        vc.imageViewHidden = true
                        vc.cancelBtnTitle = "MODIFY"
                        vc.okBtnTitle = "OK"
                    }
                    else {
                        vc.descriptionTxt = "The entered email is not allowed to register on Dropby."
                        vc.imageViewHidden = false
                        vc.cancelBtnTitle = "MODIFY"
                        vc.cancelBtnHidden = true
                        vc.okBtnTitle = "OK"
                    }
                    
                    self.present(vc, animated: true) {
                                        
                        
                        
                        vc.callback = { action in
                            
                            if action == .ok {
                                if status == true {
                                    
                                    MBProgressHUD.showAdded(to: self.view, animated: true)
                                    Auth.auth().currentUser?.updateEmail(to: self.emailAddressTF.text!, completion: { (error) in
                                        if error == nil {
                                            
                                            ProfileHandler.shared.updateUserModelWithEmail(email: self.emailAddressTF.text!)
                                            
                                            ProfileHandler.shared.synchroniseProfileData() { (status) in
                                                
                                                MBProgressHUD.hide(for: self.view, animated: true)
                                                
                                                if status {
                                                    
                                                    //MARK:- CALL API
                                                    APIManager.shared.sendVerificationEmail { (json) in
                                                        print(json?.description)
                                                    }
                                                    UserDefaults.standard.set(true, forKey: AppState.email_connected.rawValue)
                                                    UserDefaults.standard.set(true, forKey: AppState.gmail_connected.rawValue)
                                                    UserDefaults.standard.synchronize()
                                                    
                                                    self.view.makeToast("Email updated successfully.", duration: 3.0, position: .top)
                                                }
                                                else {
                                                    self.view.makeToast("Something went wrong. Please try again", duration: 3.0, position: .top)
                                                    print("something went wrong")
                                                }
                                            }
                                        }
                                        else {
                                            MBProgressHUD.hide(for: self.view, animated: true)
                                            self.view.makeToast(error?.localizedDescription, duration: 3.0, position: .top)
                                        }
                                    })
                                }
                                else {
                                    //dismiss only
                                    //no action
                                }
                            }
                            else {
                                if status == true {
                                    //dismiss only
                                    //no action
                                    //this is the case modify is pressed
                                }
                                else {
                                    //not possible
                                }
                            }
                        }
                        
                        UIView.animate(withDuration: 0.1) {
                                                                    
                            vc.backgroundView.alpha = 0.5
                        }
                    }
                }
            }
        }
    }
    
    private func addBackButton(){
        let newBackButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back))
        newBackButton.image = UIImage(named: "back")
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    @objc func back(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}
