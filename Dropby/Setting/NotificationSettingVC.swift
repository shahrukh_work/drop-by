//
//  NotificationSettingVC.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 2/7/21.
//

import UIKit

class NotificationSettingVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var notificationSettings = ProfileHandler.shared.userSettings.notifications
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Notifications"
        addBackButton()
        
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    private func addBackButton(){
        let newBackButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back))
        newBackButton.image = UIImage(named: "back")
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    @objc func back(sender: UIBarButtonItem) {
        
        ProfileHandler.shared.authInteractor.updateSettings(setting: ProfileHandler.shared.userSettings)
        self.navigationController?.popViewController(animated: true)
    }

}

extension NotificationSettingVC : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let cell : NotificationSettingCell = tableView.dequeueReusableCell(withIdentifier: "NotificationSettingCell", for: indexPath) as? NotificationSettingCell{
         
            switch indexPath.row {
            case 0:
                
                cell.lblTitle.text = "Pause All"
                
                cell.switchBtn.setOn(self.notificationSettings.pause, animated: true)
                cell.callback = { status in
                    
                    ProfileHandler.shared.userSettings.notifications.pause = status
                    ProfileHandler.shared.userSettings.notifications.messages = !status
                    ProfileHandler.shared.userSettings.notifications.connectionRequests = !status
                    ProfileHandler.shared.authInteractor.updateSettings(setting: ProfileHandler.shared.userSettings)
                    self.tableView.reloadData()
                }
                break
            case 1:
                cell.lblTitle.text = "Notify me about new messages"
                cell.switchBtn.setOn(self.notificationSettings.messages, animated: true)
                cell.callback = { status in
                    ProfileHandler.shared.userSettings.notifications.messages = status
                    if status == true {
                        ProfileHandler.shared.userSettings.notifications.pause = !status
                        self.tableView.reloadData()
                    }
                    ProfileHandler.shared.authInteractor.updateSettings(setting: ProfileHandler.shared.userSettings)
                }
                break
            case 2:
                cell.lblTitle.text = "Notify me when I make a new connection"
                cell.switchBtn.setOn(self.notificationSettings.connectionRequests, animated: true)
                cell.callback = { status in
                    ProfileHandler.shared.userSettings.notifications.connectionRequests = status
                    if status == true {
                        ProfileHandler.shared.userSettings.notifications.pause = !status
                        self.tableView.reloadData()
                    }
                    ProfileHandler.shared.authInteractor.updateSettings(setting: ProfileHandler.shared.userSettings)
                }
                break
            default:
                break
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
    
}


