//
//  ContactUs.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 04/01/2021.
//

import UIKit
import Firebase
import MBProgressHUD

class ContactUs: UIViewController {

    
    @IBOutlet weak var descriptionTF: UITextView!
    @IBOutlet weak var titleTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

//        self.tabBarController?.tabBar.isHidden = false

    }
    @IBAction func sendBtnTapped(_ sender: Any) {
        
        if (titleTF.text!.isReallyEmpty || titleTF.text!.isEmpty) {
            self.view.makeToast("Title cannot be empty", duration: 3.0, position: .top)
            return
        }
        
        if (descriptionTF.text.isReallyEmpty || descriptionTF.text!.isEmpty) {
            self.view.makeToast("Description cannot be empty", duration: 3.0, position: .top)
            return
        }
        let contactModel = ContactUsModel(title: self.titleTF.text ?? "", reportDescription: self.descriptionTF.text ?? "", reportedBy: ProfileHandler.shared.profileModel.userInfoModel.userId, platform: "iOS")
        
        self.saveToFirebase(model: contactModel)
    }
    
    func saveToFirebase(model: ContactUsModel) {
                
        ProfileHandler.shared.authInteractor.contactUs(model: model) { (success) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            let vc = GenericPopup.instantiate(fromAppStoryboard: .PopUp)
            vc.modalPresentationStyle = .overFullScreen
            vc.shouldHideOk = true
            vc.cancelButtonText = "OK"
            vc.titleText = "Contact Us"
            vc.descriptionText = "Your query has been sent successfully"
            
            self.present(vc, animated: true) {
                
                UIView.animate(withDuration: 0.1) {
                    vc.backgroundView.alpha = 0.5
                }
                vc.btnActionCallback = { action in
                    self.navigationController?.popViewController(animated: true)
                }
            }
            print(success)
        }
        
    }
    
}
class ContactUsModel :NSObject {
    
    var title               = String() //title of report
    var contactDescription  = String() //description/text of report
    var contactedBy         = String() //current user userif
    var platform            = String() // iOS or Android
//    var flag                = false
    
    init(title : String, reportDescription : String, reportedBy: String, platform: String) {
        self.title              = title
        self.contactDescription = reportDescription
        self.contactedBy        = reportedBy
        self.platform           = platform
    }
    
    override convenience init(){
        self.init(title: "", reportDescription: "", reportedBy: "", platform: "")
    }
}

extension ContactUsModel {
    class func mapper(snapshot: [String:Any]) -> ContactUsModel? {
        
        let title               = snapshot["title"] as? String ?? ""
        let reportDescription   = snapshot["description"] as? String ?? ""
        let reportedBy          = snapshot["contactedBy"] as? String ?? ""
        let platform            = snapshot["platform"] as? String ?? ""
        
        return ContactUsModel.init(title: title, reportDescription: reportDescription , reportedBy: reportedBy, platform: platform)
    }
    class func toDict(rModel: ContactUsModel) -> [String: String] {
        
        var dict = [String: String]()
        dict["title"]           = rModel.title
        dict["description"]     = rModel.contactDescription
        dict["contactedBy"]      = rModel.contactedBy
        dict["platform"]        = rModel.platform
        return dict
    }
}

