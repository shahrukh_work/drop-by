//
//  ChangeVisibilityVC.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 2/17/21.
//

import UIKit

class ChangeVisibilityVC: UIViewController {
    @IBOutlet weak var milesLabel: UILabel!
    
    @IBOutlet weak var pickerviewContainer: UIView!
    var selectedIndex : Int = 0
    
    let rangeArray = ["1","2","3","4","5","6","7","8","9"]
    let pickerView = UIPickerView()
    var rotationAngle: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let visibility = Int((Double(ProfileHandler.shared.userSettings.visibilityRadius) ?? 1.60)/1.60)
        
        selectedIndex = rangeArray.firstIndex(of: String(visibility)) ?? 0
        rotationAngle = -90 * (.pi/180)
        
        pickerView.transform = CGAffineTransform(rotationAngle: rotationAngle)
        pickerView.frame = CGRect(x: -17, y: 0, width: self.pickerviewContainer.frame.size.width, height: self.pickerviewContainer.frame.size.height)
//        pickerView.backgroundColor = UIColor.clear
//        pickerView.tintColor = UIColor.clear
        
        self.pickerviewContainer.addSubview(pickerView)
        pickerView.delegate = self
        pickerView.dataSource = self
//        pickerView.subviews.first?.subviews.last?.backgroundColor = .white
        pickerView.selectRow(selectedIndex, inComponent: 0, animated: true)
    }

    @IBAction func didTapBackground(_ sender: Any) {
        saveSetting()
        self.dismiss(animated: true, completion: nil)
    }
    
    func saveSetting() {
        
        let value = rangeArray[selectedIndex]
        let doubleValue = ((Double(value) ?? 1) * 1.60)
        ProfileHandler.shared.userSettings.visibilityRadius = String(format: "%.2f", doubleValue)
        ProfileHandler.shared.authInteractor.updateSettings(setting: ProfileHandler.shared.userSettings)
        
    }
    
}


//extension ChangeVisibilityVC : UICollectionViewDelegate , UICollectionViewDataSource{
//
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return rangeArray.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        if let cell : VisibilityCell = collectionView.dequeueReusableCell(withReuseIdentifier: "visibilityCell", for: indexPath) as? VisibilityCell{
//
//            cell.lblTitle.text = rangeArray[indexPath.item]
//
//            if indexPath.item == selectedIndex {
//                cell.lblTitle.textColor = UIColor(named: "BlueType")
//                cell.lblSubTitle.isHidden = false
//                cell.lblTitle.borderColor = UIColor(named: "BlueType")
//            }
//            else {
//
//                cell.lblTitle.textColor = .label
//                cell.lblSubTitle.isHidden = true
//                cell.lblTitle.borderColor = .systemBackground
//            }
//
//
//        return cell
//
//        }
//
//        return UICollectionViewCell()
//
//    }
//
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        if let cell : VisibilityCell = collectionView.cellForItem(at: indexPath) as? VisibilityCell{
//
//            selectedIndex = indexPath.item
//            collectionView.reloadData()
//
//        }
//
//    }
//
//
//}


extension ChangeVisibilityVC : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return rangeArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return rangeArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 80.0
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 80.0
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {

//        var myView = view
//        if myView == nil {
//            myView = UIView()
//        }
        
//        myView!.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
//        myView!.backgroundColor = UIColor.white

        let label = UILabel()
        label.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        label.textAlignment = .center
        label.text = rangeArray[row]
        label.font = UIFont(name: "Roboto-Regular", size: 20)
        label.backgroundColor = UIColor.white
        
//        myView!.addSubview(label)

        rotationAngle = 90 * (.pi/180)
        label.transform = CGAffineTransform(rotationAngle: rotationAngle)

        return label
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedIndex = row
    }
}
