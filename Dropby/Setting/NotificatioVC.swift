//
//  NotificatioVC.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 1/25/21.
//

import UIKit

class NotificatioVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var allNotifications = [NotificationsModel]()
    @IBOutlet weak var thereNoNotification: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hidesBottomBarWhenPushed=true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        updateNotifications()
        let shouldShowBadge = UserDefaults.standard.bool(forKey: "notification")
        if shouldShowBadge {
            UserDefaults.standard.set(false, forKey: "notification")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hidesBottomBarWhenPushed = false
    }
    
    func updateNotifications(){
        allNotifications = ProfileHandler.shared.notifications
        if self.allNotifications.count == 0 {
            self.thereNoNotification?.isHidden = false
        }
        else
        {
            self.thereNoNotification?.isHidden = true
//            self.tableView.scrollToRow(at: IndexPath(row: allNotifications.count, section: 0), at: .bottom, animated: true)
            
        }
    }
    
}

extension NotificatioVC : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return allNotifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell : NotificationCell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as? NotificationCell else{return UITableViewCell()}
        
        let model = allNotifications.reversed()[indexPath.row]
            cell.lblTitle.text = model.body
                
            cell.notificationImageView.sd_setImage(with: URL(string: model.basicInfoModel.profilePicURL), placeholderImage: UIImage(named: "avatar_1"), options: .refreshCached, context: nil)

        return cell
        
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        header.backgroundColor = .systemBackground
        
        let label = UILabel(frame: CGRect(x: 20, y: 0, width: self.view.frame.size.width, height: 50))
        
        if section == 1{
            label.text = "Recent"
        }else{
            label.text = "This Week"
        }
        label.backgroundColor = .systemBackground
        
        header.addSubview(label)
        
        return header
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            return 0
        }
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = ProfileHandler.shared.nearByAllUsers.filter { profile in
            
            if profile.userInfoModel.userId == allNotifications[indexPath.row].basicInfoModel.friendUserId {
                return true
            }
            return false
        }
        
        let profileVC = ProfileVC.instantiate(fromAppStoryboard: .DropBy)
        
        profileVC.user = data.first
        profileVC.loadViewIfNeeded()
        profileVC.btnEditProfile.isHidden = true
        self.show(profileVC, sender: true)
    }
}
