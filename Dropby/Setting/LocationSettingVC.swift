//
//  LocationSettingVC.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 2/7/21.
//

import UIKit
import MBProgressHUD

class LocationSettingVC: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    var selectedVisibility = ProfileHandler.shared.profileModel.visibilityModel.status
    var dropByUsers = [GenericUserModel]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Privacy"
        self.addBackButton()
        
        tableview.delegate = self
        tableview.dataSource = self
        fetchHiddenDropbyList()
    }
    
    
    private func addBackButton(){
        let newBackButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: #selector(back))
        newBackButton.image = UIImage(named: "back")
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    @objc func back(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func fetchHiddenDropbyList() {
        
        self.dropByUsers.removeAll()
        MBProgressHUD.showAdded(to: self.view, animated: true)
        APIManager.shared.fetchHiddenDropbyFeatures { (dataObject) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                if dataObject != nil {
                    
                    for contact in dataObject! {
                        let userObject = GenericUserModel.mapper(snapshot: contact) ?? GenericUserModel()
                        self.dropByUsers.append(userObject)
                    }
                    self.tableview.reloadData()
                }
            }
        }
    }
}

extension LocationSettingVC : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 4
        default:
            return self.dropByUsers.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 1 {
            return 100
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            if let cell : ShareLocationCell = tableview.dequeueReusableCell(withIdentifier: "ShareLocationCell", for: indexPath) as? ShareLocationCell{
                
                switch indexPath.row {
                case 0:
                    cell.lblTitle.text = "GO OFFLINE"
                    cell.visibilityStatus = .GoOffline
                    cell.lblSubtitle.text = ""
                    break
                case 1:
                    cell.lblTitle.text = "FRIENDS ONLY"
                    cell.visibilityStatus = .Friends
                    cell.lblSubtitle.text = ""
                    break
                case 2:
                    cell.lblTitle.text = "FRIENDS OF FRIENDS"
                    cell.visibilityStatus = .FriendsOfFriends
                    cell.lblSubtitle.text = "Your phone contacts and their friends, plus your Connections can see you, except for those hidden and blocked"
                    break
                case 3:
                    cell.lblTitle.text = "EVERYONE"
                    cell.visibilityStatus = .Everyone
                    cell.lblSubtitle.text = ""
                    break
                default:
                    break
                }
                
                if selectedVisibility == cell.visibilityStatus.rawValue {
                    cell.btnSelection.isSelected = true
                }
                else {
                    cell.btnSelection.isSelected = false
                }
                return cell
            }
            break
        case 1:
            
            if let cell : LocationVisibilityCell = tableview.dequeueReusableCell(withIdentifier: "LocationVisibilityCell", for: indexPath) as? LocationVisibilityCell{
                let model = self.dropByUsers[indexPath.row]
                cell.profileModel = model
                cell.callback = {
                    self.fetchHiddenDropbyList()
                }
                
                return cell
            }
            break
        default:
            break
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var title = ""
        
        if section == 0{
            title = "Nearby Visibility"
        }else{
            title = "Dropby Visibility"
        }
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 200))
        let label = UILabel(frame: CGRect(x: 20, y: 10, width: self.view.frame.width, height: 40))
        label.text = title
        label.font = UIFont(name: "Roboto-Regular", size: 20)
        view.addSubview(label)
        
        if section == 1 {
            let label2 = UILabel(frame: CGRect(x: 20, y: 50, width: self.view.frame.width, height: 40))
            label2.text = "Dropby Map Hidden Users"
            label2.font = UIFont(name: "Roboto-Bold", size: 13)
            
            view.addSubview(label2)
        }
        
        return view
    }
    
}
