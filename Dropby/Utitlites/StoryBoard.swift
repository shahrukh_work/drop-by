//
//  StoryBoard.swift
//  Quartrly
//
//  Created by Narsun on 22/02/2018.
//  Copyright © 2020 Qazi Mudassar. All rights reserved.
//

import Foundation
import  UIKit

extension UIViewController {
    
    class var storyboardID : String {
        return "\(self)"
    }
    
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
}
 

    enum AppStoryboard : String {
        case UserAuthentication , Main, PopUp, DropBy, Message
        
        var instance : UIStoryboard {
            return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
        }
    
    
    func viewController<T : UIViewController>(viewControllerClass : T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID        
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            fatalError("ViewController with identifier \(storyboardID), not found in \(self.rawValue) Storyboard")
        }
    
        
        
        return scene
    }
        
        
        
    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
}


