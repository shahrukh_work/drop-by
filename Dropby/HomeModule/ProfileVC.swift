//
//  ProfileVC.swift
//  Dropby
//
//  Created by Junaid on 01/12/2020.
//

import UIKit
import TagListView
import MBProgressHUD
import FirebaseFirestore
var globalProfile : ProfileVC?
class ProfileVC: UIViewController {
    
    @IBOutlet weak var ImgUserIcon: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblDetail1: UILabel!
    @IBOutlet weak var infoViewStack: UIStackView!
    
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var btnAddFriend: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnEditProfile: UIButton!
    
    @IBOutlet weak var bioLabel: UILabel!
    

    @IBOutlet weak var collectionViewHeightConstant: NSLayoutConstraint!

    @IBOutlet weak var noCommonConnection: UILabel!
    let newDottedButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: #selector(dottedAction))
   
    var comeFromSetting = Bool()
    
   // self.navigationItem.rightBarButtonItem = newDottedButton
    
        
    @IBOutlet weak var viewAllBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var commonFriends = [GenericUserModel]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tagLists: TagListView!
    
    @IBOutlet weak var commonNetworkTitleStack: UIStackView!
    
    @objc func dottedAction(sender: UIBarButtonItem) {
        
        //self.navigationController?.popViewController(animated: true)
    }
    var user : ProfileModel?
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        self.commonNetworkTitleStack.isHidden = false
        self.collectionView.isHidden = true
        self.viewAllBtn.isHidden = true
        self.noCommonConnection.isHidden = true
        prefillViews()
        collectionView.dataSource = self
        collectionView.delegate = self
        
        let yourBackImage = UIImage(named: "back")
        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.addNotificationObservers()
        if let _ = user{
            ref = Firestore.firestore()
            ref?.collection("friendsRequests").document(user!.userInfoModel.userId).addSnapshotListener({ doc, error in
                print("friend Request snapshot working")
               // MBProgressHUD.showAdded(to: self.view, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.updateButtonsStatus()
                }
            })
        }
    }
    
    fileprivate func prefillViews() {
        
        let name = (user?.userInfoModel.firstName
            ?? "") + " " + (user?.userInfoModel.lastName ?? "")
        lblUserName.text = name
        lblDetail1.text = user!.userInfoModel.instituteName
        
        ImgUserIcon.sd_setImage(with: URL(string: user!.userInfoModel.profilePicURL)) { (image, error, SDImageCacheTypeDisk, url) in
             
        }
     //   updateButtonsStatus()
        
        bioLabel.text = user?.userInfoModel.basicInfo
        
        if let userModel = user {
            if userModel.userInfoModel.userId != ProfileHandler.shared.profileModel.userInfoModel.userId {
                fetchMutualContacts()
            }
            else {
                self.commonNetworkTitleStack.isHidden = false
                self.noCommonConnection.isHidden = false
                self.collectionView.isHidden = true
            }
        }

        if let userModel = user {
            self.tagLists.removeAllTags()
            self.tagLists.addTags(userModel.tagsModel.tagsList)
        }
            
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden=false
    }
    
    @IBAction func viewAllTapped(_ sender: Any) {
        
        let vc = ViewAllVC.instantiate(fromAppStoryboard: .DropBy)
        self.show(vc, sender: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func actionTapped(_ sender: Any) {
        
        if let usr = user {
            if let btn = sender as? UIButton {
                if btn.tag == 1 {
                    //send message
                    var isBlocked = false
                    for blockedContact in ProfileHandler.shared.blockedContacts {
                        if blockedContact == usr.userInfoModel.userId {
                            
                            //cannot add
                            self.presentDidSelect(forUser: usr)
                            isBlocked = true
                            break
                        }
                    }
                    if !isBlocked {
                        self.messageUser(user: usr)
                    }
                    else {
                        
                    }
                }
                else if btn.tag == 2 {
                    //send friend request
                    var isBlocked = false
                    for blockedContact in ProfileHandler.shared.blockedContacts {
                        if blockedContact == usr.userInfoModel.userId {
                            
                            //cannot add
                            self.presentDidSelect(forUser: usr)
                            isBlocked = true
                            break
                        }
                    }
                    if isBlocked == false {
                        
                        MBProgressHUD.showAdded(to: self.view, animated: true)
                        ProfileHandler.shared.checkUserFriendStatus(userId: user!.userInfoModel.userId) { (status) in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if status != nil {
                                
                                if status == .friends {
                                    MBProgressHUD.showAdded(to: self.view, animated: true)
                                    ProfileHandler.shared.authInteractor.removeConnection(user: self.user ?? ProfileModel()) { (success) in
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                            self.updateButtonsStatus()
                                        }
                                    }
                                }
                                else if status == .notFriends {
                                    //add friend
                                    ProfileHandler.shared.sendFriendRequest(user: usr) { (success) in
                                        self.updateButtonsStatus()
                                    }
                                } else if status == .friendRequested {
                                    MBProgressHUD.showAdded(to: self.view, animated: true)
                                    ProfileHandler.shared.authInteractor.cancelConnectionRequest(user: self.user ?? ProfileModel()) { (success) in
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                    }
                                } else if status == .friendRequested {
                                    MBProgressHUD.showAdded(to: self.view, animated: true)
                                    ProfileHandler.shared.authInteractor.cancelConnectionRequest(user: self.user ?? ProfileModel()) { (success) in
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                        self.updateButtonsStatus()
                                    }
                                }
                            }
                        }
                    }
                }
                else if btn.tag == 3 {
                    //more menu
                    let userModel = self.user!
                    let vc = MoreActionSheet.instantiate(fromAppStoryboard: .PopUp)
                    vc.userModel = userModel
                    vc.callBack = { type in
                        
                        DispatchQueue.main.async {
                            vc.closeMenu()
                        }
                        if type == .report {
                            
                            let storyBoard = UIStoryboard(name: "Feedback", bundle: nil)
                            guard let reportVC = storyBoard.instantiateViewController(identifier: "ReportVC") as? ReportVC else{return}
                            reportVC.againstUserId = userModel.userInfoModel.userId
                            self.show(reportVC, sender: self)
                        }
                        else if type == .block {
                            
                            MBProgressHUD.showAdded(to: self.view, animated: true)
                            //first check with API if iam blocked?
                            
                            ProfileHandler.shared.authInteractor.addToBlockList(model: userModel) { (success) in
                                
                                DispatchQueue.main.async {
                                    
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    //Call popup
                                    self.afterSteps(title: "Contact Blocked", text: " was successfully blocked. You can no longer interact with this user", userName: userModel.userInfoModel.firstName)
                                }
                            }
                        }
                        else if type == .invisible {
                            
                            MBProgressHUD.showAdded(to: self.view, animated: true)
                            
                            ProfileHandler.shared.authInteractor.addToNearbyInvisibleList(userID: userModel.userInfoModel.userId) { (success) in
                                DispatchQueue.main.async {
                                    
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    self.afterSteps(title: "Invisible", text: " was successfully marked invisible. You can no longer interact with this user", userName: userModel.userInfoModel.firstName)
                                }
                            }
                        }
                        else if type == .friendUnFriend {
                            ProfileHandler.shared.checkUserFriendStatus(userId: self.user!.userInfoModel.userId) { (status) in
                                
                                if status == .friends {
                                    ProfileHandler.shared.authInteractor.removeConnection(user: userModel) { status in
                                        
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                            self.updateButtonsStatus()
                                        }
                                    }
                                    
                                } else {
                                    //  self.updateButtonsStatus()
                                }
                            }
                        }
                    }
                    vc.modalPresentationStyle = .overFullScreen
                    self.present(vc, animated: true) {
                        UIView.animate(withDuration: 0.1) {
                            vc.backgroundView.alpha = 0.5
                        }
                    }
                }
                else {
                    let vc : DetailEditProfile = DetailEditProfile.instantiate(fromAppStoryboard: .UserAuthentication)
                   //vc.editMode = false
                    self.show(vc, sender: true)
                }
            }
        }
    }
    
}

extension ProfileVC {
    
    func presentDidSelect(forUser usr: ProfileModel) {
        
        let vc = GenericPopup.instantiate(fromAppStoryboard: .PopUp)
        vc.modalPresentationStyle = .overFullScreen
        vc.titleText = "Blocked Contact"
        vc.descriptionText = usr.userInfoModel.firstName+" cannot be added as contact as it is blocked."
        vc.shouldHideCancel = true
        vc.okButtonText = "OK"
        
        self.present(vc, animated: true) {
            
            UIView.animate(withDuration: 0.1) {
                vc.backgroundView.alpha = 0.5
            }
        }
    }
    
    func messageUser(user: ProfileModel) {
                
        //MBProgressHUD.showAdded(to: self.view, animated: true)
        //check if already a chat thread exists
        MessagingHandler.shared.fetchChatDetailFor(userWithId: user.userInfoModel.userId) { (chatModel) in
            
            
            MBProgressHUD.hide(for: self.view, animated: true)
            let detailMessageVc = MessageDetailVC.instantiate(fromAppStoryboard: .Message)
            self.show(detailMessageVc, sender: true)
            if chatModel != nil {
                
                detailMessageVc.chatListModel = chatModel!
                //means already a thread exists
//                MessagingHandler.shared.getAndUpdateMessages(withId: chatID) { (chatListObject) in
//
//                }
                detailMessageVc.chatID = chatModel!.chatID
                detailMessageVc.userObject = user
            }
            else {
                //create new chat
                detailMessageVc.userObject = user
            }
        }
    }
    
    func updateButtonsStatus() {
     //check if he is friend or not
        if let userModel = user {
            if userModel.userInfoModel.userId == ProfileHandler.shared.profileModel.userInfoModel.userId {
                self.btnEditProfile.isHidden = false
                
            }
            else {
                // MBProgressHUD.showAdded(to: self.view, animated: true)
                ProfileHandler.shared.checkUserFriendStatus(userId: (user?.userInfoModel.userId)!) { [self] (status) in
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if status != nil {
                        if status == .notFriends{
                                                
                            self.btnMessage.isHidden = true
                            self.btnAddFriend.isHidden = false
                            self.btnAddFriend.isSelected = false
                            self.btnAddFriend.isEnabled = true
                            self.btnAddFriend.isSelected = false
                            self.btnMore.isHidden = false
                        }
                        else if status == .friends {
                            self.btnMessage.isHidden = false
                            self.btnAddFriend.isHidden = true
                            self.btnMore.isHidden = false
                            self.btnMore.isEnabled = true
                            
                        }
                        else if status == .friendRequested {
                            //change to cancel
                            self.btnMessage.isHidden = true
                            self.btnAddFriend.isHidden = false
                            self.btnAddFriend.isSelected = true
                            self.btnMore.isHidden = false
                            self.btnMore.isEnabled = true
                        }
                    }
                }
            }
        }
    }
    
    func afterSteps(title: String, text: String, userName: String) {
                
        let vc = GenericPopup.instantiate(fromAppStoryboard: .PopUp)
        vc.modalPresentationStyle = .overFullScreen
        vc.titleText = title
        vc.descriptionText = userName+text
        vc.shouldHideOk = true
        vc.cancelButtonText = "OK"
        
        self.present(vc, animated: true) {
                        
            vc.btnActionCallback = { action in
                
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
                
            }
            UIView.animate(withDuration: 0.1) {
                
                vc.backgroundView.alpha = 0.5
            }
        }
    }
    
    func fetchMutualContacts() {
        let userId = user?.userInfoModel.userId ?? ""
        if userId.isEmpty {
            return
        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ProfileHandler.shared.getCommonFriendsWith(userID: userId) { (common) in
            
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if common != nil {
                    
                    self.commonFriends = common!
                    if self.commonFriends.count < 5 {
                        self.viewAllBtn.isHidden = true
                        if self.commonFriends.count == 0 {
                            
                            self.commonNetworkTitleStack.isHidden = false
                            self.collectionView.isHidden = true
                            self.noCommonConnection.isHidden = false
                        }
                        else{
                            self.commonNetworkTitleStack.isHidden = false
                            self.collectionView.isHidden = false
                            self.noCommonConnection.isHidden = true
                            
                        }
                    }
                    else{
                        self.viewAllBtn.isHidden = false
                    }
                    self.collectionView.reloadData()
                }
            }
        }
    }
}

extension ProfileVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return commonFriends.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell : DropByActivityCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DropByActivityCell", for: indexPath) as? DropByActivityCell else{return UICollectionViewCell()}
        let friend = commonFriends[indexPath.item]
        cell.iconImageView.sd_setImage(with: URL(string: friend.profilePicURL)) { (image, error, type, url) in
            
        }
        cell.nameLbl.text = friend.friendUserName
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = collectionView.bounds.size.height
        return CGSize(width: height, height: height)
    }
    
}


//MARK:- TableView
//extension ProfileVC : UITableViewDataSource , UITableViewDelegate, PresesntVCCellProtocol{
//
//
//
//
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 4
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        switch indexPath.row {
//        case 0:
//            return 300
//        case 1:
//            return 250
//        case 2:
//            return 140
//        case 3:
//            return UITableView.automaticDimension
//        default:
//            return 0
//        }
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        switch indexPath.row {
//        case 0:
//
//            guard let cell : ProfileHeaderCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath) as? ProfileHeaderCell else{return UITableViewCell()}
//            cell.delegage = self
//            cell.user = user
//
//            return cell
//
//        case 1:
//            guard let cell : ProfileInfoCell = tableView.dequeueReusableCell(withIdentifier: "ProfileInfoCell", for: indexPath) as? ProfileInfoCell else{return UITableViewCell()}
//            cell.bioLabel.text = user?.userInfoModel.basicInfo
//            return cell
//
//        case 2:
//            guard let cell : ProfileNetworkCell = tableView.dequeueReusableCell(withIdentifier: "ProfileNetworkCell", for: indexPath) as? ProfileNetworkCell else{return UITableViewCell()}
//            if let userModel = user {
//                cell.userId = userModel.userInfoModel.userId
//                cell.fetchMutualContacts()
//            }
//
//            return cell
//
//        case 3:
//            guard let cell : ProfileLabelsCell = tableView.dequeueReusableCell(withIdentifier: "ProfileLabelsCell", for: indexPath) as? ProfileLabelsCell else{return UITableViewCell()}
//
//            if let userModel = user {
//                cell.tagsList.removeAllTags()
//                cell.tagsList.addTags(userModel.tagsModel.tagsList)
//            }
//
//            return cell
//
//        default:
//            return UITableViewCell()
//        }
//
//    }
//
//
//}
