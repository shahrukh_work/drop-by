//
//  ReportVC.swift
//  Dropby
//
//  Created by Junaid on 01/12/2020.
//

import UIKit
import MBProgressHUD

class ReportVC: UIViewController {

    @IBOutlet weak var descriptionTV: UITextView!
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var screenShotImage: UIImageView!
    var againstUserId: String = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func addScreenShot(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func sendTapped(_ sender: Any) {
        
        if (titleTF.text == nil || titleTF.text!.isEmpty) {
            self.view.makeToast("Title cannot be empty", duration: 3.0, position: .top)
            return
        }
        
        if (descriptionTV.text == nil || descriptionTV.text!.isEmpty) {
            self.view.makeToast("Description cannot be empty", duration: 3.0, position: .top)
            return
        }
        
//        if screenShotImage.isHidden == true {
//            self.view.makeToast("Please attach a screenshot", duration: 3.0, position: .bottom)
//            return
//        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let destinationPath = URL(fileURLWithPath: documentsPath).appendingPathComponent("filename.png")

        debugPrint("destination path is",destinationPath)

        do {
            try screenShotImage.image?.pngData()?.write(to: destinationPath)
        } catch {
            debugPrint("writing file error", error)
        }
        
        let reportModel = ReportModel(title: self.titleTF.text ?? "", reportDescription: self.descriptionTV.text ?? "", against: self.againstUserId, reportedBy: ProfileHandler.shared.profileModel.userInfoModel.userId, screenShotUrl: "", platform: "iOS")
        
        if screenShotImage.isHidden == true {
            self.saveToFirebase(model: reportModel)
        }
        else {
            ProfileHandler.shared.authInteractor.uploadScreenShotToStorage(url: destinationPath) { (urlString, status) in
                
                if status == true {
                    //all good
                    let reportModel = ReportModel(title: self.titleTF.text ?? "", reportDescription: self.descriptionTV.text ?? "", against: self.againstUserId, reportedBy: ProfileHandler.shared.profileModel.userInfoModel.userId, screenShotUrl: urlString!, platform: "iOS")
                    self.saveToFirebase(model: reportModel)
                }
                else {
                    
                }
            }
        }
        //upload to server
        
    }
    
    func saveToFirebase(model: ReportModel) {
                
        ProfileHandler.shared.authInteractor.reportUser(model: model) { (success) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            let vc = GenericPopup.instantiate(fromAppStoryboard: .PopUp)
            vc.modalPresentationStyle = .overFullScreen
            vc.cancelButtonText = "OK"
            vc.shouldHideOk = true
            vc.titleText = "Report"
            vc.descriptionText = "User has been reported successfully"
            
            self.present(vc, animated: true) {
                
                UIView.animate(withDuration: 0.1) {
                    vc.backgroundView.alpha = 0.5
                }
                vc.btnActionCallback = { action in
                    self.navigationController?.popViewController(animated: true)
                }
            }
            print(success)
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK:- UITextViewDelegate
extension ReportVC: UITextViewDelegate, UINavigationControllerDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars <= 250    // 10 Limit Value
    }
}

//MARK:- UIImagePickerControllerDelegate
extension ReportVC: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            screenShotImage.isHidden = false
            screenShotImage.image = pickedImage
            screenShotImage.contentMode = .scaleAspectFit
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

class ReportModel :NSObject {
    
    var title               = String() //title of report
    var reportDescription   = String() //description/text of report
    var against             = String() //userid of reported person
    var reportedBy          = String() //current user userif
    var screenShotUrl       = String() //url for screenshot, currently one supported.
    var platform            = String() // iOS or Android
    
    init(title : String, reportDescription : String, against : String, reportedBy: String, screenShotUrl: String, platform: String) {
        self.title              = title
        self.reportDescription  = reportDescription
        self.against            = against
        self.reportedBy         = reportedBy
        self.screenShotUrl      = screenShotUrl
        self.platform           = platform
    }
    
    override convenience init(){
        self.init(title: "" , reportDescription: "" , against: "", reportedBy: "", screenShotUrl: "", platform: "")
    }
}

extension ReportModel {
    class func mapper(snapshot: [String:Any]) -> ReportModel? {
        
        let title               = snapshot["title"] as? String ?? ""
        let reportDescription   = snapshot["description"] as? String ?? ""
        let against             = snapshot["against"] as? String ?? ""
        let reportedBy          = snapshot["reportedBy"] as? String ?? ""
        let screenShotUrl       = snapshot["screenShotUrl"] as? String ?? ""
        let platform            = snapshot["platform"] as? String ?? ""
        
        return ReportModel.init(title: title, reportDescription: reportDescription , against: against, reportedBy: reportedBy, screenShotUrl: screenShotUrl, platform: platform)
    }
    class func toDict(rModel: ReportModel) -> [String: String] {
        
        var dict = [String: String]()
        dict["title"]           = rModel.title
        dict["description"]     = rModel.reportDescription
        dict["against"]         = rModel.against
        dict["reportedBy"]      = rModel.reportedBy
        dict["screenShotUrl"]   = rModel.screenShotUrl
        dict["platform"]        = rModel.platform
        return dict
    }
}
