//
//  HomeVC.swift
//  Dropby
//
//  Created by Junaid on 19/11/2020.
//

import UIKit
import MBProgressHUD
import CoreLocation
import FirebaseFirestore
import FirebaseFirestore
var globalHomeVC:HomeVC?
class HomeVC: SuperViewController {
    
    enum toggleStates: String {
        
        case toggle_default
        case toggle_connected
        case toggle_notconnected
    }
    
    @IBOutlet weak var notificationIcon: UIBarButtonItem!
    @IBOutlet weak var emptyListLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var filterButton: DesignableButton!
    @IBOutlet weak var filterToggle: UIImageView!
    @IBOutlet weak var notificationBarButton: UIBarButtonItem!
        var shouldShowNotificationView = true
    var filterSelectedState = toggleStates.toggle_default.rawValue
    var filterName = "All"
    var usersToShow = [ProfileModel]()
    var hasViewLoaded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        globalHomeVC = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("message"), object: nil)
        ref = Firestore.firestore()
        ref?.collection("friendsRequests").addSnapshotListener { snap, error in
            NotificationCenter.default.post(name: Notification.Name("Requested"), object: nil)

        }
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.addNotificationObservers()
        initialization()
        ProfileHandler.shared.fetchChatList {
            self.messagesPrerequisite()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkAppVersion(notification:)), name: versionNotName, object: nil)
        self.refreshNearbyScenario()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
        shouldShowNotificationView = false
                if ref == nil {
                    ref = Firestore.firestore()
                }
                ref?.collection("notifications").document(ProfileHandler.shared.profileModel.userInfoModel.userId).addSnapshotListener { doc, error in
                    if self.shouldShowNotificationView == false{
                        self.shouldShowNotificationView = true
                        let shouldShowBadge = UserDefaults.standard.bool(forKey: "notification")
                        if shouldShowBadge {
                            self.notificationBarButton.setBadge()
                        }
                        else{
                            self.notificationBarButton.removeBadge()
                        }
                    }
                    else{
                        self.notificationBarButton.setBadge()
                        UserDefaults.standard.set(true, forKey: "notification")
                    }
                }
    }
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        tabBarController?.tabBar.barTintColor = UIColor.white
        navigationController?.navigationBar.isHidden = false

        if hasViewLoaded {
            refreshNearbyScenario()
        }
        hasViewLoaded = true
        if ProfileHandler.shared.location != nil {
            ProfileHandler.shared.updateUserLocation(location: ProfileHandler.shared.location)
        }
        
        //MARK:- call refresh only if 2 mins have passed
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
//        self.tabBarController?.selectedIndex = 3
        
        guard let data =  notification.userInfo!["data"] as? [String:String] else {
                    return
                        }
//        let chatId = data["chatId"]!
        let id = data["id"]!
     ///   moveToDetail(userId: id)
        let userFilter = ProfileHandler.shared.nearByAllUsers.filter { profile in
            
            if profile.userInfoModel.userId == id {
                return true
            }
            return false
        }
        
        moveToDetail(userId: id, user: userFilter.first!)
        
    }
    
    
    func moveToDetail(userId:String,user:ProfileModel) {
        
        MessagingHandler.shared.fetchChatDetailFor(userWithId: userId) { (chatModel) in
            
            
            MBProgressHUD.hide(for: self.view, animated: true)
            let detailMessageVc = MessageDetailVC.instantiate(fromAppStoryboard: .Message)
            if chatModel != nil {
                detailMessageVc.chatListModel = chatModel!
                detailMessageVc.chatID = chatModel!.chatID
                detailMessageVc.userObject = user
            }
            else {
                //create new chat
                detailMessageVc.userObject = user
            }
            self.tabBarController?.present(detailMessageVc, animated: true, completion: nil)

        }
    }
    
    func messagesPrerequisite() {
                        
        let unreadCount = ProfileHandler.shared.getUnreadCount()
        if unreadCount > 0 {
            
            if let tabItems = self.tabBarController?.tabBar.items {
                let messageTab = tabItems[3]
                messageTab.badgeValue = String(unreadCount)
            }
        }
    }
    
    private func refreshNearbyScenario() {
        
        ProfileHandler.shared.userListingDelegate = self
        ProfileHandler.shared.shouldRefreshListing()
    }
    
    @IBAction func didSelectLeftMenu(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Settings", bundle: nil)
        guard let VC = storyBoard.instantiateViewController(identifier: "SettingVC") as? SettingVC else{return}
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @objc private func checkAppVersion(notification: NSNotification) {
        
      // Take Action on Notification
        let object = notification.userInfo
        if let version = object?["version"] as? String {
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            if version != appVersion {
//                self.pushAppUpdateScreen()
            }
        }
    }

    func updateNearbyListing() {
                
        self.usersToShow.removeAll()
        self.usersToShow = ProfileHandler.shared.nearByAllUsers
        
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
            if self.usersToShow.isEmpty {
                self.tableView.isHidden = true
                self.emptyListLabel.isHidden = false
            }
            else {
                self.tableView.isHidden = false
                self.emptyListLabel.isHidden = true
            }
            self.tableView.reloadData()
        }
    }
    
    func pushAppUpdateScreen() {
        
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        guard let updateVC = storyBoard.instantiateViewController(identifier: "UpdateAppScreen") as? UpdateAppScreen else{return}
        updateVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(updateVC, animated: true)
    }
    
    @IBAction func filterAction(_ sender: Any) {
        /// filter vc here
        
        if filterSelectedState == toggleStates.toggle_default.rawValue {
            filterSelectedState = toggleStates.toggle_connected.rawValue
            filterName = "Friends"
        }
        else if filterSelectedState == toggleStates.toggle_connected.rawValue{
            filterSelectedState = toggleStates.toggle_notconnected.rawValue
            filterName = "Others"
        }
        else {
            filterSelectedState = toggleStates.toggle_default.rawValue
            filterName = "All"
        }
        
        updateAfterFilterChange()
    }
    
    func updateAfterFilterChange () {
        //apply filter changes here
        filterButton.setTitle(filterName, for: UIControl.State())
        filterToggle.image = UIImage.init(named: filterSelectedState)
        updateTableViewWithFilter()
    }
    
    func updateTableViewWithFilter() {
                
        if filterName == "Friends" {
            var friendsOnly = [ProfileModel]()
            for user in ProfileHandler.shared.nearByAllUsers {
                for friend in ProfileHandler.shared.friends {
                    if friend == user.userInfoModel.userId {
                        friendsOnly.append(user)
                    }
                }
            }
            usersToShow.removeAll()
            usersToShow = friendsOnly
        }
        else if filterName == "Others" {
            var othersOnly = [ProfileModel]()
            for user in ProfileHandler.shared.nearByAllUsers {
                var isOtherUser = true
                for friend in ProfileHandler.shared.friends {
                    if friend == user.userInfoModel.userId {
                        isOtherUser = false
                        break
                    }
                }
                if isOtherUser {
                    othersOnly.append(user)
                }
            }
            usersToShow.removeAll()
            usersToShow = othersOnly
        }
        else {
            self.usersToShow = ProfileHandler.shared.nearByAllUsers
        }
        DispatchQueue.main.async{
            self.tableView.reloadData()
        }
    }
    
    @IBAction func bellAction(_ sender: Any) {
        /// bell vc here
    }
    @IBAction func myVisibilityTapped(_ sender: Any) {
        let vc = MyVisibilityPopup.instantiate(fromAppStoryboard: .PopUp)
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true) {
            UIView.animate(withDuration: 0.1) {
                vc.backGroundView.alpha = 0.5
            }
        }
        vc.successCallback = { status in
            self.refreshNearbyScenario()
        }
    }
}
//MARK:- Basic Method
extension HomeVC {
    func initialization(){
        updateAfterFilterChange()
    }
    
}
//MARK:- UITableViewDataSource
extension HomeVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.usersToShow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as? HomeCell
        cell?.profileModel = self.usersToShow[indexPath.item]
        
        if cell == nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileInfoCell", for: indexPath) as? ProfileInfoCell
            cell?.bioLabel.text = ProfileHandler.shared.profileModel.userInfoModel.basicInfo
        }
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
                
        let messageAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success: @escaping (Bool) -> Void) in
            
            /// add vc here
            let userModel = self.usersToShow[indexPath.row]
            let status = ProfileHandler.shared.fetchFriendsSatus(forUser: userModel.userInfoModel.userId)
            MBProgressHUD.showAdded(to: self.view, animated: true)
        
            APIManager.shared.amIVisibleTo(userWith: userModel.userInfoModel.userId, for: VisibilityCheck.amiVisible.rawValue) { (isVisible) in
                
                DispatchQueue.main.async {
                    if isVisible {
                                                
                        
                        if status == .friends {
                            //open message                            
                            //check if already a chat thread exists
                            MessagingHandler.shared.fetchChatDetailFor(userWithId: userModel.userInfoModel.userId) { (chatModel) in
                                
                                self.notificationIcon.image = UIImage(contentsOfFile: "notification.png")
                                
                                MBProgressHUD.hide(for: self.view, animated: true)
                                let detailMessageVc = MessageDetailVC.instantiate(fromAppStoryboard: .Message)
                                self.show(detailMessageVc, sender: true)
                                if chatModel != nil {
                                    
                                    detailMessageVc.chatListModel = chatModel!
                                    //means already a thread exists
                    //                MessagingHandler.shared.getAndUpdateMessages(withId: chatID) { (chatListObject) in
                    //
                    //                }
                                    detailMessageVc.chatID = chatModel!.chatID
                                    detailMessageVc.userObject = userModel
                                }
                                else {
                                    //create new chat
                                    detailMessageVc.userObject = userModel
                                }
                            }
                            
                        }
                        else if status == .friendRequested {
                            //do nothing for now
                            self.afterSteps(title: "Requested", text: " has already been requested for connection.", userName: userModel.userInfoModel.firstName)
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        else {
                            //send friend request
                            //TODO:- friend request
                            ProfileHandler.shared.sendFriendRequest(user: userModel) { (success) in
                                
                                MBProgressHUD.hide(for: self.view, animated: true)
                            }
                        }
                        success(true)
                    }
                    else {
                        //show popup that friend is not visible.
                        //Refresh list
                        self.afterSteps(title: "User not visible", text: "You cannot communicate with an invisible user", userName: "")
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                }
            }
        })
        let userModel = self.usersToShow[indexPath.row]
        let status = ProfileHandler.shared.fetchFriendsSatus(forUser: userModel.userInfoModel.userId)
        if status == .friends {
            messageAction.image = UIImage(named:"message")
            messageAction.backgroundColor = #colorLiteral(red: 0.1858788133, green: 0.7518694997, blue: 0.3267681003, alpha: 1)
        }
        else if status == .friendRequested {
            messageAction.image = UIImage(named:"add-user")
            messageAction.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        }
        else {
            
            messageAction.image = UIImage(named:"add-user")
            messageAction.backgroundColor = #colorLiteral(red: 0.3294117647, green: 0.3843137255, blue: 0.8823529412, alpha: 1)
            
        }
        
        
        return UISwipeActionsConfiguration(actions: [  messageAction])
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let editAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            /// add vc here
            let vc = MoreActionSheet.instantiate(fromAppStoryboard: .PopUp)
            let userModel = self.usersToShow[indexPath.row]
            vc.userModel = userModel
            
            vc.callBack = { type in
                
                DispatchQueue.main.async {
                    vc.closeMenu()
                }
                if type == .report {
                    
                    let storyBoard = UIStoryboard(name: "Feedback", bundle: nil)
                    guard let reportVC = storyBoard.instantiateViewController(identifier: "ReportVC") as? ReportVC else{return}
                    reportVC.againstUserId = userModel.userInfoModel.userId
                    self.navigationController?.pushViewController(reportVC, animated: true)
                }
                else if type == .block {
                    
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    //first check with API if iam blocked?
                    
                    ProfileHandler.shared.authInteractor.addToBlockList(model: userModel) { (success) in
                        
                        DispatchQueue.main.async {
                            
                            MBProgressHUD.hide(for: self.view, animated: true)
                            //Call popup
                            self.afterSteps(title: "Contact Blocked", text: " was successfully blocked.", userName: userModel.userInfoModel.firstName)
                        }
                    }
                }
                else if type == .invisible {
                    
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    
                    ProfileHandler.shared.authInteractor.addToNearbyInvisibleList(userID: userModel.userInfoModel.userId) { (success) in
                        DispatchQueue.main.async {
                            
                            MBProgressHUD.hide(for: self.view, animated: true)
                            self.afterSteps(title: "Invisible", text: " was successfully marked invisible.", userName: userModel.userInfoModel.firstName)
                        }
                    }
                }
            }
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true) {
                UIView.animate(withDuration: 0.1) {
                    vc.backgroundView.alpha = 0.5
                }
                
            }
            success(true)
        })
        
        editAction.image = UIImage(named:"more")
        editAction.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [  editAction])
    }
    
    func afterSteps(title: String, text: String, userName: String) {
                
        let vc = GenericPopup.instantiate(fromAppStoryboard: .PopUp)
        vc.modalPresentationStyle = .overFullScreen
        vc.titleText = title
        vc.descriptionText = userName+text
        vc.shouldHideOk = true
        vc.cancelButtonText = "OK"
            
        self.present(vc, animated: true) {
            

            vc.btnActionCallback = { action in
                
//                DispatchQueue.main.async {
//                    MBProgressHUD.showAdded(to: self.view, animated: true)
//                }
                self.refreshNearbyScenario()
            }
            UIView.animate(withDuration: 0.1) {
                
                vc.backgroundView.alpha = 0.5
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let profileVC = ProfileVC.instantiate(fromAppStoryboard: .DropBy)
        
        let userModel = self.usersToShow[indexPath.row]
        profileVC.user = userModel
        profileVC.loadViewIfNeeded()
        profileVC.btnEditProfile.isHidden = true
        self.show(profileVC, sender: true)
    }
    
}
//MARK:- UITableViewDelegate
extension HomeVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- Location Manager Handling

extension HomeVC: UserListingRefreshProtocol {
    func dropbyUsersDidUpdate() {
        
    }
    
    func nearbyUserDidUpdate() {
        self.updateNearbyListing()
    }
    
    func locationUpdated(location: CLLocation) {
        
    }
    
}




