//
//  BlockListModel.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 10/02/2021.
//

import UIKit

class BlockListModel: NSObject {
    
    var isBlocked = false
    var personUserID = ""
    var powerUserIdToUnBlock = ""
    
    init(isBlocked: Bool, personUserID: String, powerUserToUnblock: String) {
        self.isBlocked              = isBlocked
        self.personUserID           = personUserID
        self.powerUserIdToUnBlock   = powerUserToUnblock
        
    }
    
    override convenience init() {
        self.init(isBlocked: false, personUserID: String(), powerUserToUnblock: String())
    }
}

//MARK:- MAPPER
extension BlockListModel {
    
    class func mapper(snapshot: [String:Any]) -> BlockListModel? {
        
        let isNearbyVisible     = snapshot["isBlocked"] as? Bool ?? true
        let personUserID        = snapshot["personUserId"] as? String ?? String()
        let powerUserIdToUnBlock = snapshot["powerUserIdToUnBlock"] as? String ?? String()
        return BlockListModel.init(isBlocked: isNearbyVisible, personUserID: personUserID, powerUserToUnblock: powerUserIdToUnBlock)
    }
    
    class func toDict(dModel: BlockListModel) -> [String: Any] {
        
        var dict = [String: Any]()
        
        dict["isBlocked"]     = dModel.isBlocked
        dict["personUserId"]        = dModel.personUserID
        dict["powerUserIdToUnBlock"] = dModel.powerUserIdToUnBlock
        return dict
    }
}
