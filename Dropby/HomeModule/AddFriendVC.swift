//
//  AddFriendVC.swift
//  Dropby
//
//  Created by Macbook Pro on 11/08/2021.
//

import UIKit

class AddFriendVC: UIViewController {

    @IBOutlet weak var addFriendRequest: UIView!

    @IBOutlet weak var addFriend: UIButton!
    @IBOutlet weak var cancel: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func addFriendTapped(_ sender: UIButton) {
    }
    @IBAction func cancelTapped(_ sender: UIButton) {
    }
    
    func setup(){
        addFriendRequest.shadowOpacity = 0.5
        cancel.layer.cornerRadius = 10
        addFriend.layer.cornerRadius = 10
        addFriendRequest.layer.cornerRadius = 10
    }
}
