//
//  NearbyVisibilityModel.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 10/02/2021.
//

import UIKit

class NearbyVisibilityModel: NSObject {
    
    var isNearbyVisible = true
    var personUserID = ""
    var powerUserIdToUnhide = ""
    
    init(isNearbyVisible: Bool, personUserID: String, powerUserIdToUnhide: String) {
        self.isNearbyVisible        = isNearbyVisible
        self.personUserID           = personUserID
        self.powerUserIdToUnhide    = powerUserIdToUnhide
        
    }
    
    override convenience init() {
        self.init(isNearbyVisible: true, personUserID: String(), powerUserIdToUnhide: String())
    }
}

//MARK:- MAPPER
extension NearbyVisibilityModel {
    
    class func mapper(snapshot: [String:Any]) -> NearbyVisibilityModel? {
        
        let isNearbyVisible     = snapshot["isNearbyVisible"] as? Bool ?? true
        let personUserID        = snapshot["personUserId"] as? String ?? String()
        let powerUserIdToUnhide = snapshot["powerUserIdToUnhide"] as? String ?? String()
        return NearbyVisibilityModel.init(isNearbyVisible: isNearbyVisible, personUserID: personUserID, powerUserIdToUnhide: powerUserIdToUnhide)
    }
    
    class func toDict(dModel: NearbyVisibilityModel) -> [String: Any] {
        
        var dict = [String: Any]()
        
        dict["isNearbyVisible"]     = dModel.isNearbyVisible
        dict["personUserId"]        = dModel.personUserID
        dict["powerUserIdToUnhide"] = dModel.powerUserIdToUnhide
        return dict
    }
    
}
