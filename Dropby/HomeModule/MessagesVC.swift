//
//  MessagesVC.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 08/01/2021.
//

import UIKit
import MBProgressHUD
import FirebaseFirestore

var globalMessageVC : MessagesVC?
class MessagesVC: SuperViewController {
    
    
    @IBOutlet weak var sadlyNoMessage: UILabel!
    
    @IBOutlet weak var collectioView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pinnedContactsView: UIView!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var notificationBarButton: UIBarButtonItem!
    var shouldShowNotificationView = true
    var pinnedContacts  = [String]()
    var chatList        = [MessageListModel]()
    var pinnedList      = [MessageListModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
        fetchChatList()
        fetchPinnedContacts()
        clearBadgeAndNotifications()
        globalMessageVC = self
        shouldShowNotificationView = false
        if ref == nil {
            ref = Firestore.firestore()
        }
        ref?.collection("notifications").document(ProfileHandler.shared.profileModel.userInfoModel.userId).addSnapshotListener { doc, error in
            if self.shouldShowNotificationView == false{
                self.shouldShowNotificationView = true
                let shouldShowBadge = UserDefaults.standard.bool(forKey: "notification")
                if shouldShowBadge {
                    self.notificationBarButton.setBadge()
                }
                else{
                    self.notificationBarButton.removeBadge()
                }
            }
            else{
                self.notificationBarButton.setBadge()
                UserDefaults.standard.set(true, forKey: "notification")
            }
        }
    }

    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    @IBAction func didTapSideMenu(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Settings", bundle: nil)
        guard let vc = storyBoard.instantiateViewController(identifier: "SettingVC") as? SettingVC else{return}
        vc.hidesBottomBarWhenPushed = true
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    fileprivate func clearBadgeAndNotifications() {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        
        if let tabItems = self.tabBarController?.tabBar.items {
            let messageTab = tabItems[3]
            messageTab.badgeValue = nil
        }
    }
    func fetchChatList() {
        
        ProfileHandler.shared.fetchChatList {
            self.chatList = ProfileHandler.shared.chatList
            self.filterPinnedContacts()
            self.fetchPinnedContacts()
            if self.chatList.count == 0 {
                self.sadlyNoMessage?.isHidden = false
            }
            else
            {
                self.sadlyNoMessage?.isHidden = true
            }
            self.tableView.reloadData()
        }
                
    }
    func filterPinnedContacts() {
        let array = UserDefaults.standard.array(forKey: "PinnedContacts")
        if array != nil && array!.count > 0{
            let tempPinnedContacts = array as! [String]
            var pinnedContacts = [String]()
            for chat in chatList{
                for userId in chat.participants{
                    if tempPinnedContacts.contains(userId) {
                        pinnedContacts.append(userId)
                    }
                }
            }
            UserDefaults.standard.setValue(pinnedContacts, forKey: "PinnedContacts")
            UserDefaults.standard.synchronize()
        }
    }
    func fetchPinnedContacts() {
        
        let array = UserDefaults.standard.array(forKey: "PinnedContacts")
        if array != nil && array!.count > 0{
//            UIView.animate(withDuration: 0.3) {
                self.pinnedContactsView.isHidden = false
                self.loadViewIfNeeded()
//            }
            self.pinnedContacts = array as! [String]
        }
        else {
//            UIView.animate(withDuration: 0.3) {
                self.pinnedContactsView.isHidden = true
//            }
        }
        self.collectioView.reloadData()
    }
    
    func addRemovePinned(userId: String, chatModel: MessageListModel, pinnedArray: [String]?) {
        if pinnedArray != nil {
            var array = pinnedArray!
            if array.count == 5 {
                return
                    self.view.makeToast("Maximum 5 pinned contacts are allowed", duration: 3.0, position: .bottom)
            }
            if array.contains(userId) {
                array.remove(at: array.firstIndex(of: userId)!)
                if let object = self.pinnedList.firstIndex(of: chatModel) {
                    self.pinnedList.remove(at: object)
                }
                else {
                    self.pinnedList.removeAll()
                }
                
            }
            else {
                array.append(userId)
                self.pinnedList.append(chatModel)
            }
            UserDefaults.standard.setValue(array, forKey: "PinnedContacts")
            UserDefaults.standard.synchronize()
        }
        else {
            var array = [String]()
            array.append(userId)
            self.pinnedList.append(chatModel)
            UserDefaults.standard.setValue(array, forKey: "PinnedContacts")
            UserDefaults.standard.synchronize()
        }
        fetchPinnedContacts()
        fetchPinnedContacts()
    }
}

//MARK:- CollectionView for making pinned chats

extension MessagesVC : UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.pinnedContacts.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
	
        guard let cell : PinnedConatctCell = collectionView.dequeueReusableCell(withReuseIdentifier: "pinnedContactCell", for: indexPath) as? PinnedConatctCell else{return UICollectionViewCell()}
        cell.userID = self.pinnedContacts[indexPath.item]
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let userID = self.pinnedContacts[indexPath.item]
        
        //Action Here
        for messageObject in self.chatList {
            
            if messageObject.participants.first == userID || messageObject.participants.last == userID {
                //found
                moveToDetail(messageObject: messageObject)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: 90)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
}

//MARK:- TableView


extension MessagesVC : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell : MessageCell =  tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as? MessageCell  else{return UITableViewCell()}
        
        cell.messageObject = self.chatList[indexPath.row]
        cell.selectionStyle = .none
        let chatListModel = self.chatList[indexPath.row]
        
        cell.unreadCountLbl.text = String(0)
        cell.unreadCountLbl.isHidden = true
        
        if chatListModel.lastMessage.sender != ProfileHandler.shared.profileModel.userInfoModel.userId {
            
            let unread = MessagingHandler.shared.getUnreadCountForChat(chatListModel: chatListModel)
            cell.unreadCountLbl.text = String(unread)
            if unread == 0 {
                            
                cell.unreadCountLbl.isHidden = true
            }
            else {
                cell.unreadCountLbl.isHidden = false
            }
            return cell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                        
        let messageObject = self.chatList[indexPath.row]
        moveToDetail(messageObject: messageObject)
    }
    
    func moveToDetail(messageObject: MessageListModel) {
        
        var userID = ""
        let detailMessageVc = MessageDetailVC.instantiate(fromAppStoryboard: .Message)
    //    MBProgressHUD.showAdded(to: self.view, animated: true)
        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
        if messageObject.participants.first != myUserId {
            userID = messageObject.participants.first!
        }
        else {
            userID = messageObject.participants.last!
        }
        
        ProfileHandler.shared.authInteractor.fetchUser(withUserId: userID) { (profile) in
            if profile != nil {
                MBProgressHUD.hide(for: self.view, animated: true)
                detailMessageVc.userObject = profile
            }
        }

        detailMessageVc.chatID = messageObject.chatID
        detailMessageVc.chatListModel = messageObject
        self.show(detailMessageVc, sender: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if  editingStyle == .delete {
            print("Delete Pressed")
        }
    }
    
    
    
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        
        
        
        let messageAction = UIContextualAction(style: .destructive, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            let name = ProfileHandler.shared.profileModel.userInfoModel.firstName
            let vc = GenericPopup.instantiate(fromAppStoryboard: .PopUp)
            vc.modalPresentationStyle = .overFullScreen
            vc.cancelButtonText = "CANCEL"
            vc.okButtonText = "YES"
            vc.titleText = "Confirm"
            vc.descriptionText = "Are you sure you want to delete chat with "+name+"?"
            
            self.present(vc, animated: true) {
                
                UIView.animate(withDuration: 0.1) {
                    vc.backgroundView.alpha = 0.5
                }
                vc.btnActionCallback = { action in
                   
                    if action == .ok {
                        var userId = ""
                        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
                        
                       // MBProgressHUD.showAdded(to: self.view, animated: true)
                        let messageObject = self.chatList[indexPath.row]
                        MessagingHandler.shared.deleteChatThread(withID: messageObject.chatID) { (success) in
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        
                        
                        if messageObject.participants.first != myUserId {
                            userId = messageObject.participants.first ?? ""
                        }
                        else {
                            userId = messageObject.participants.last ?? ""
                        }
                        let pinnedArray = UserDefaults.standard.array(forKey: "PinnedContacts") as? [String]
                        if pinnedArray != nil {
                            var array = pinnedArray!
                            if array.contains(userId) {
                                array.remove(at: array.firstIndex(of: userId)!)
                                UserDefaults.standard.setValue(array, forKey: "PinnedContacts")
                                UserDefaults.standard.synchronize()
                            }
                        }
                        self.fetchPinnedContacts()
                    }
                    self.dismiss(animated: true, completion: nil)
                }
            }
            success(true)
        })
        
        
        messageAction.image = UIImage(named: "delete")
        messageAction.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [  messageAction])
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        var userId = ""
        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
        let messageObject = self.chatList[indexPath.row]
        if messageObject.participants.first != myUserId {
            userId = messageObject.participants.first ?? ""
        }
        else {
            userId = messageObject.participants.last ?? ""
        }
        let pinnedArray = UserDefaults.standard.array(forKey: "PinnedContacts") as? [String]
        
        //Action Here
        let editAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.addRemovePinned(userId: userId, chatModel: messageObject, pinnedArray: pinnedArray)
            success(true)
        })
        if pinnedArray != nil {
            if pinnedArray!.contains(userId) {
                editAction.image = UIImage(named: "unpinned_icon")
            }
            else{
                editAction.image = UIImage(named: "slider_pin")
            }
        }
        else{
            editAction.image = UIImage(named: "slider_pin")
        }
        editAction.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [  editAction])
    }
}



