//
//  MessgeDetailVC.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 14/01/2021.
//

import UIKit
import FirebaseFirestore

class MessageDetailVC: UIViewController {
    
    @IBOutlet weak var chatViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var chatStackView: UIStackView!
    @IBOutlet weak var chatView: UIView!
    
    @IBOutlet weak var typeYourMessage: UILabel!
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var onlineStatus: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    var chatListModel : MessageListModel?
    var isUnreadSet = false
    var isSendPressed = false
    var wifiReachability:Reachability?
    
    var allMessages: [MessageDetail]? {
        didSet {
            
        }
    }
    var chatID: String? {
        didSet {
            fetchMessageObject()
        }
    }
    
    var userObject: ProfileModel? {
        didSet {
            updateViews()
        }
    }
    
    
    @IBAction func didPressSend(_ sender: Any) {
        if (textView.text!.isReallyEmpty || textView.text!.isEmpty) {
            self.view.makeToast("Message cannot be empty", duration: 3.0, position: .top)
            return
        }
        sendMessage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
      //self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.dataSource = self
        configureTextView()
        self.updateViews()
       
        startMonitoringReachability()
        textViewDidChange(textView)
        //self.textField.setLeftPaddingPoints(10)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden=true
        self.tabBarController?.tabBar.isHidden=true
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.addNotificationObservers()

        NotificationCenter.default.addObserver(self, selector: #selector(self.onlineView(notifications:)), name: Notification.Name("isOnline"), object: nil)

    }
    fileprivate func configureTextView() {
        textView.text = "type your message"
        textView.textColor = UIColor.lightGray
        self.textView.delegate = self
        self.textView.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    @objc func onlineView(notifications : Notification){
        if UserDefaults.standard.bool(forKey: "isOnline"){
            self.onlineStatus.backgroundColor = .green
        }
        else{
            self.onlineStatus.backgroundColor = .gray
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden=false
        self.tabBarController?.tabBar.isHidden=false
        self.isUnreadSet = true
        self.manageReadCount()
    }
    func startMonitoringReachability()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
        if ((wifiReachability == nil)) {
            wifiReachability = Reachability.forInternetConnection()
        }
        wifiReachability?.startNotifier()
        reachabilityChanged()
    }
    @objc func reachabilityChanged()
    {
        if wifiReachability?.currentReachabilityStatus() == .NotReachable {
            self.onlineStatus.backgroundColor = .gray
        }
        else{
            self.onlineStatus.backgroundColor = .green
        }
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func fetchMessageObject() {
        self.typeYourMessage?.isHidden = false
        
        MessagingHandler.shared.getAndUpdateMessages(withId: chatID!) { (messages) in
            self.allMessages = messages
            self.chatListModel?.lastMessage = self.allMessages?.last ?? MessageDetail()
            
            if !self.isUnreadSet {
                self.setUnreadCount()
            } else {
                self.isUnreadSet = true
            }
            if self.tableView != nil {
                self.tableView.reloadData()
                DispatchQueue.main.async {
                    if self.allMessages!.count > 0
                    {
                        self.typeYourMessage?.isHidden = true
                        let index = IndexPath(row: self.allMessages!.count-1, section: 0)
                        self.tableView.scrollToRow(at: index, at: .bottom, animated: false)
                    }
                }
            }
        }
    }
       
    func setUnreadCount () {
        for unread in self.chatListModel!.unreadCount {
            
            if unread.key != ProfileHandler.shared.profileModel.userInfoModel.userId {
                self.chatListModel!.unreadCount.updateValue(0, forKey: unread.key)
                
            }
        }
    }
        
    func updateViews() {
     
        if self.imgView == nil || userObject == nil{
            return
        }
        self.imgView.sd_setImage(with: URL(string: self.userObject!.userInfoModel.profilePicURL ), placeholderImage: UIImage(named: "avatar_1"), options: .refreshCached, context: nil)
        
        self.titleLbl.text = self.userObject!.userInfoModel.firstName+" "+self.userObject!.userInfoModel.lastName
        
        self.tableView.reloadData()
        if self.allMessages != nil && self.allMessages!.count > 0 {
            DispatchQueue.main.async {
                let index = IndexPath(row: self.allMessages!.count-1, section: 0)
                self.tableView.scrollToRow(at: index, at: .bottom, animated: true)
            }
        }
        
        self.manageReadCount()
    }
    
    func manageReadCount() {
        //set read count to 0 if sender is not you
        if chatListModel != nil {
                                                
            for unread in chatListModel!.unreadCount {
                
                if unread.key == ProfileHandler.shared.profileModel.userInfoModel.userId {
                    
                    self.chatListModel!.unreadCount.updateValue(0, forKey: unread.key)
                    
                    MessagingHandler.shared.updateChatListWithLastMessage(forChatID: self.chatID!, andListModel: self.chatListModel!) { (success) in
                        if success {
                            print("all good")
                        }
                    }
                    break
                }
            }
        }
        
//        if self.chatListModel != nil && self.chatListModel!.lastMessage.sender != ProfileHandler.shared.profileModel.userInfoModel.userId{
//            self.chatListModel!.lastMessage.isRead = true
//            self.chatListModel!.unreadCount = 0
//
//            MessagingHandler.shared.updateChatListWithLastMessage(forChatID: self.chatID!, andListModel: self.chatListModel!) { (success) in
//                if success {
//                    print("all good")
//                }
//            }
//        }
    }

    func getMessageObject (withBody body:String) -> MessageDetail{
        
        let messageobject = MessageDetail.init(body: body, createdAt: Timestamp.init(), sender: ProfileHandler.shared.profileModel.userInfoModel.userId)
        return messageobject
    }
    
    func getMessageWithListModel(withText text: String) -> MessageListModel {
        
        let object = self.getMessageObject(withBody: text)
        self.allMessages = [MessageDetail]()
        self.allMessages!.append(object)
        self.updateViews()
        
        var unreadMap = [String:Int]()
        unreadMap[ProfileHandler.shared.profileModel.userInfoModel.userId] = 0
        unreadMap[self.userObject!.userInfoModel.userId] = 1
        
        let model =  MessageListModel.init(createdAt: Timestamp.init(), participants: [ProfileHandler.shared.profileModel.userInfoModel.userId, self.userObject!.userInfoModel.userId], chatid: "", lastMsg: self.getMessageObject(withBody: text), unread: unreadMap)
        
        if self.chatListModel == nil {
            self.chatListModel = model
        }
        
        return model
    }
    
    func addListenerToChatThread() {
        
        MessagingHandler.shared.fetchChatDetailFor(userWithId: userObject!.userInfoModel.userId) { (receivedModel) in
            if receivedModel != nil {
                self.chatListModel = receivedModel!
                self.chatID = receivedModel!.chatID
            }
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.chatViewBottomConstraint.constant = keyboardSize.height + 2
        }
    }
    @objc func keyboardDidShow(notification: NSNotification) {
        self.setTableViewOffset()
    }
    func setTableViewOffset(){
        DispatchQueue.main.async {
            var yScrollPoint = self.tableView.contentSize.height - self.tableView.frame.size.height
            if yScrollPoint < 0{
                yScrollPoint = 0
            }
            let scrollPoint = CGPoint(x: 0, y: yScrollPoint)
            self.tableView.setContentOffset(scrollPoint, animated: true)
            self.tableView.layoutIfNeeded()
            self.view.layoutIfNeeded()
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        self.chatViewBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.tableView.layoutIfNeeded()
            self.view.layoutIfNeeded()
        }
    }
}



extension MessageDetailVC: UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegateFlowLayout {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allMessages?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
        let message = allMessages![indexPath.row]
        
        if message.sender == myUserId {
            
            guard let cell : MessageSenderTableViewCell =  tableView.dequeueReusableCell(withIdentifier: "MessageSender", for: indexPath) as? MessageSenderTableViewCell  else{return UITableViewCell()}
            cell.titleLbl.text = ProfileHandler.shared.profileModel.userInfoModel.firstName
            
            cell.messageObject = message
            cell.selectionStyle = .none
            return cell
        }
        else {
            
            guard let cell : MessageReceiverTableViewCell =  tableView.dequeueReusableCell(withIdentifier: "MessageReceiver", for: indexPath) as? MessageReceiverTableViewCell  else{return UITableViewCell()}
            cell.titleLbl.text = userObject?.userInfoModel.firstName ?? ""
            
            cell.messageObject = message
            cell.selectionStyle = .none
            return cell
        }
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
//        let message = allMessages![indexPath.row]
//        
//        if message.sender == myUserId {
//            
//            let cell =  tableView.dequeueReusableCell(withIdentifier: "MessageSender") as? MessageSenderTableViewCell
//            
//            if cell != nil {
//                let height = message.body.height(withConstrainedWidth: cell!.containerView.frame.width, font: UIFont(name: "Roboto", size: 20)!)
//                print(height)
//                return height
//            }
//        }
//        else {
//            
//            let cell =  tableView.dequeueReusableCell(withIdentifier: "MessageReceiver") as? MessageReceiverTableViewCell
//            
//            if cell != nil {
//                let height = message.body.height(withConstrainedWidth: cell!.containerView.frame.width, font: UIFont(name: "Roboto", size: 20)!)
//                print(height)
//                return height
//            }
//        }
//        return 50
//    }
    
    func sendMessage() {
        guard self.userObject != nil else {
            return
        }
        if textView.text == "type your message" {
            return
        }
        ProfileHandler.shared.checkUserFriendStatus(userId: self.userObject!.userInfoModel.userId) { [weak self] (status) in
            
            //            MBProgressHUD.hide(for: self.view, animated: true)
            if status != nil {
                if status != .friends{
                    self?.view.makeToast("Sorry, you cannot send messages because you are not friends.", duration: 3.0, position: .top)
                    return
                }
                else{
                    if let self = self{
                        let text = self.textView.text!
                        self.textView.text = ""
                        
                        if self.allMessages == nil || self.allMessages!.count == 0 {
                            
                            //create new chat
                            MessagingHandler.shared.createNewChat(withUser: self.userObject!.userInfoModel.userId, messageListModel: self.getMessageWithListModel(withText: text)) { (chatModel) in
                                self.addListenerToChatThread()
                                
                                //MARK:- TODO: TESTING in the morning
                                //Should get Chat ID here first
                                if chatModel != nil {
                                    
                                    self.chatListModel = chatModel
                                    self.chatID = chatModel!.chatID
                                    MessagingHandler.shared.updateChatListWithLastMessage(forChatID: self.chatID!, andListModel: self.chatListModel ?? MessageListModel()) { (success) in
                                        
                                        if success {
                                            print("all good")
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            
                            //push message to existing chat thread
                            MessagingHandler.shared.pushMessage(withFriend: self.userObject!.userInfoModel.userId, forChatID: self.chatID!, andMessage: self.getMessageObject(withBody: text)) { (success) in
                                // self.isSendPressed = true
                                self.isUnreadSet = false
                                //increment read count
                                if self.chatListModel != nil {
                                    self.isUnreadSet = false
                                    self.chatListModel!.lastMessage = self.getMessageObject(withBody: text)
                                    self.chatListModel!.createdAt = Timestamp()
                                    for unread in self.chatListModel!.unreadCount {
                                        
                                        if unread.key != ProfileHandler.shared.profileModel.userInfoModel.userId {
                                            let unreadValue = unread.value + 1
                                            self.chatListModel!.unreadCount.updateValue(unreadValue, forKey: unread.key)
                                        }
                                    }
                                }
                                
                                //now update chat list model
                                MessagingHandler.shared.updateChatListWithLastMessage(forChatID: self.chatID!, andListModel: self.chatListModel ?? MessageListModel()) { (success) in
                                    
                                    if success {
                                        print("all good")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
       // self.textViewDidChange(self.textView)
//        if textView.text.isEmpty {
//            textView.text = "type your message"
//            textView.textColor = UIColor.lightGray
//        }
    }
}

extension MessageDetailVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        let size = CGSize(width: textView.frame.width, height: textView.frame.height)
        let estimatedsize = textView.sizeThatFits(size)
        textView.constraints.forEach { constraint in
            if constraint.firstAttribute == .height{
                if estimatedsize.height < 100 {
                constraint.constant = estimatedsize.height
                    self.setTableViewOffset()
                }
            }
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type your message"
            textView.textColor = UIColor.lightGray
        }
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
    
        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)

        return ceil(boundingBox.width)
    }
    
    func capitalizingFirstLetter() -> String {
            return prefix(1).capitalized + dropFirst()
        }

        mutating func capitalizeFirstLetter() {
            self = self.capitalizingFirstLetter()
        }
    
}
