//
//  ViewAllVC.swift
//  
//
//  Created by Junaid Mukhtar on 07/12/2020.
//

import UIKit
import MBProgressHUD

class ViewAllVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.        
    }
    
    @IBAction func backTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK:- UITableViewDataSource
extension ViewAllVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProfileHandler.shared.nearByAllUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ViewAllCell", for: indexPath)
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        
        
        
        let messageAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            /// add vc here
            success(true)
        })
        
        
        messageAction.image = UIImage(named:"message")
        messageAction.backgroundColor = #colorLiteral(red: 0.1858788133, green: 0.7518694997, blue: 0.3267681003, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [  messageAction])
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let editAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            /// add vc here
            let vc = MoreActionSheet.instantiate(fromAppStoryboard: .PopUp)
            vc.callBack = { type in
                let userModel = ProfileHandler.shared.nearByAllUsers[indexPath.row]
                
                if type == .report {
                    let storyBoard = UIStoryboard(name: "Feedback", bundle: nil)
                    let reportVC = storyBoard.instantiateViewController(identifier: "ReportVC")
                    
                    self.navigationController?.pushViewController(reportVC, animated: true)
                }
                else if type == .block {
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    ProfileHandler.shared.authInteractor.addToBlockList(model: userModel) { (success) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                }
                
            }
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true) {
                UIView.animate(withDuration: 0.1) {
                    vc.backgroundView.alpha = 0.5
                }
                
            }
            success(true)
        })
        
        editAction.image = UIImage(named:"more")
        editAction.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [  editAction])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}
//MARK:- UITableViewDelegate
extension ViewAllVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
