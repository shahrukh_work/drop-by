//
//  ContactsVC.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 06/01/2021.
//

import UIKit
import Contacts
import MBProgressHUD
import FirebaseFirestore

class ContactsVC: SuperViewController {
    
    @IBOutlet weak var contactsView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var notificationBarButton: UIBarButtonItem!
        var shouldShowNotificationView = true
    var filtered:[GenericUserModel] = []
    var searchActive : Bool = false
    
    let validTypes = [
            CNLabelPhoneNumberiPhone,
            CNLabelPhoneNumberMobile,
            CNLabelPhoneNumberMain
        ]
    
    var contacts = [CNContact]()
    var contactsToDisplay = [GenericUserModel]()
    let refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initViews()
        fetchContacts()
        fetchLocalContacts()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.addNotificationObservers()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        shouldShowNotificationView = false
                if ref == nil {
                    ref = Firestore.firestore()
                }
                ref?.collection("notifications").document(ProfileHandler.shared.profileModel.userInfoModel.userId).addSnapshotListener { doc, error in
                    if self.shouldShowNotificationView == false{
                        self.shouldShowNotificationView = true
                        let shouldShowBadge = UserDefaults.standard.bool(forKey: "notification")
                        if shouldShowBadge {
                            self.notificationBarButton.setBadge()
                        }
                        else{
                            self.notificationBarButton.removeBadge()
                        }
                    }
                    else{
                        self.notificationBarButton.setBadge()
                        UserDefaults.standard.set(true, forKey: "notification")
                    }
                }
    }
    private func initViews(){
        
        emptyView.isHidden=true
        tableView.delegate = self
        tableView.dataSource = self
        
        if contacts.count == 0 {
            showEmptyView()
        }else{
            hideEmptyView()
        }
    }
    
    private func showEmptyView(){
        self.emptyView.isHidden=false
        self.contactsView.isHidden=true
        
    }
    
    private func hideEmptyView(){
        if contactsToDisplay.count > 0 {
            self.emptyView.isHidden=true
            self.contactsView.isHidden = false
        }
    }
    
    @IBAction func didTapSync(_ sender: Any) {
        
        contacts = [CNContact]()
        fetchContacts()
        
    }
    
    func uploadContactsAndSync() {
        
        var contactNumbersArray = [String]()
        for contact in contacts {
            let numbers = contact.phoneNumbers.compactMap { phoneNumber -> String? in
                    guard let label = phoneNumber.label, validTypes.contains(label) else { return nil }
                return phoneNumber.value.stringValue
                }
            for number in numbers {
                
                
                let trimmedString = number.filter("0123456789".contains)
                
                contactNumbersArray.append("+" + trimmedString)
            }
        }
        print(contactNumbersArray.count)
        
        
        DispatchQueue.main.async {
//            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.view.makeToast("It would take sometime to sync contacts. Please wait.")
        }
        
        APIManager.shared.uploadContacts(contacts: contactNumbersArray) { (success) in
            
            if success {
                APIManager.shared.fetchContactsDetail { (contactsss) in
                    DispatchQueue.main.async {
                        
                        self.contactsToDisplay.removeAll()
                        if contactsss != nil {
                            
                            for contact in contactsss! {
                                                                
                                let obj = GenericUserModel.mapper(snapshot: contact) ?? GenericUserModel()
                                if obj.friendUserId != ProfileHandler.shared.profileModel.userInfoModel.userId {
                                    self.contactsToDisplay.append(obj)
                                }
                            }
                        }
                        self.saveContacts()
                        self.hideEmptyView()
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.tableView.reloadData()
                    }
                }
            }
            else {
                
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }
            
        }
    }
    //MARK:- Saving Users Defaults
    func saveContacts() {
        
        do {
            let archivedContacts = try NSKeyedArchiver.archivedData(withRootObject: self.contactsToDisplay, requiringSecureCoding: false)
            UserDefaults.standard.set(archivedContacts, forKey: kContacts)
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    //MARK:- Fetching Users Defaults
    func fetchLocalContacts() {
        //MBProgressHUD.hide(for: self.view, animated: true)
        guard let archivedContacts = UserDefaults.standard.object(forKey: kContacts) as? NSData else {
                print("Contacts not found in UserDefaults")
                return
            }
            
            do {
                guard let archives = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(archivedContacts as Data) as? [GenericUserModel] else { return }
                print(archives)
                self.contactsToDisplay = archives
                self.hideEmptyView()
                
                self.tableView.reloadData()
            } catch {
                print(error.localizedDescription)
            }
    }
    
    @IBAction func didTapSideMenu(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Settings", bundle: nil)
        guard let vc = storyBoard.instantiateViewController(identifier: "SettingVC") as? SettingVC else{return}
        vc.hidesBottomBarWhenPushed = true
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
}

extension ContactsVC : UITableViewDelegate , UITableViewDataSource, UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            searchActive = false
            
            view.endEditing(true)
            
            tableView.reloadData()
        } else {
            searchActive = true
            
            
            filtered = contactsToDisplay.filter {
                $0.friendUserName.range(of: searchBar.text!, options: [.caseInsensitive, .diacriticInsensitive ]) != nil
            }
            
            tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {return 1}
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            return filtered.count
        }
        return contactsToDisplay.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {return 80}
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell : ContactCell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as? ContactCell else{return UITableViewCell()}
        if searchActive {
            if filtered.count > 0 {
                cell.contact = filtered[indexPath.row]
            }            
        }
        else {
            cell.contact = contactsToDisplay[indexPath.row]
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchBar.resignFirstResponder()
        let contact = contactsToDisplay[indexPath.row]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        APIManager.shared.getUserProfile(withUserId: contact.friendUserId) { (json) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                if json != nil {
                    if let msg = json!["message"] as? String  {
                        self.view.makeToast(msg)
                    }
                    else {
                        let profileVC = ProfileVC.instantiate(fromAppStoryboard: .DropBy)
                        let profile = ProfileModel.publicMapper(snapshot: json!, model: ProfileModel())
                        profileVC.user = profile
                        self.show(profileVC, sender: true)
                    }
                }
            }
        }
    }
}


//Contact Sync
extension ContactsVC{
    
    private func fetchContacts() {
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { [weak self] (granted, error) in
            
            
            if let strongSelf = self{
                if let error = error {
                    print("failed to request access", error)
                    strongSelf.showSettingsAlert()
                }
                if granted {
                    let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
                    let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                    do {
                        try store.enumerateContacts(with: request, usingBlock: { (contact, stopPointer) in
                            strongSelf.contacts.append(contact)
                        })
                        strongSelf.uploadContactsAndSync()
                        
                    } catch let error {
                        print("Failed to enumerate contact", error)
                    }
                } else {
                    print("access denied")
                    strongSelf.showSettingsAlert()
                }
            }
        }
    }
    
    
    private func showSettingsAlert() {
        
        DispatchQueue.main.async {
            
            let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Go to Settings to grant access.", preferredStyle: .alert)
            if
                let settings = URL(string: UIApplication.openSettingsURLString),
                UIApplication.shared.canOpenURL(settings) {
                alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
                    UIApplication.shared.open(settings)
                })
            }
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
                
            })
            self.present(alert, animated: true)
            
        }
    }
    
}
