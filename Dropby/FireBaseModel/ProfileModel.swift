//
//  ProfileModel.swift
//  Dropby
//
//  Created by Junaid on 12/11/2020.
//

import Foundation
import FirebaseFirestore
import FirebaseDatabase

class ProfileModel{
    
    var userInfoModel       = UserInfoModel()
//    var googleModel         = GoogleModel()
    var linkModel           = LinkedInModel()
//    var locationModel       = LocationModel()
    var onlineModel         = OnlineModel()
    var tagsModel           = TagsModel()
    var visibilityModel     = VisibilityModel()
    var geoLocationModel    = GeoLocationModel()
    var dropByActivityModel = DropByActivityModel()
    var dropByActivity      = String()
    var email               = String()
    //locationMdl: LocationModel
    //googleModel : GoogleModel
    init(userInfoModel: UserInfoModel , linkModel : LinkedInModel, onlineMdl: OnlineModel, tagsMdl: TagsModel) {
        
        self.userInfoModel      = userInfoModel
//        self.googleModel        = googleModel
        self.linkModel          = linkModel
//        self.locationModel      = locationMdl
        self.onlineModel        = onlineMdl
        self.tagsModel          = tagsMdl
    }
    //googleModel : GoogleModel
    init(userInfoModel: UserInfoModel , linkModel : LinkedInModel, onlineMdl: OnlineModel, tagsMdl: TagsModel, email: String) {
        
        self.userInfoModel      = userInfoModel
//        self.googleModel        = googleModel
        self.linkModel          = linkModel
        self.email              = email
        self.onlineModel        = onlineMdl
        self.tagsModel          = tagsMdl
    }
    
    func initPublic(userInfoModel: UserInfoModel, onlineMdl: OnlineModel, tagsMdl: TagsModel, visibility: VisibilityModel) {
        
        self.userInfoModel      = userInfoModel
//        self.locationModel      = locationMdl
        self.onlineModel        = onlineMdl
        self.tagsModel          = tagsMdl
        self.visibilityModel    = visibility
    }
    
    convenience init(){
        self.init(userInfoModel: UserInfoModel(),
//                  googleModel : GoogleModel() ,
                  linkModel : LinkedInModel(),
//                  locationMdl: LocationModel(),
                  onlineMdl: OnlineModel(),
                  tagsMdl: TagsModel())
    }
}

//MARK:-Mapper
extension ProfileModel {
    class func mapper(snapshot: [String:Any]) -> ProfileModel? {
        
//        let google          = GoogleModel()
        let linkDin         = LinkedInModel.mapper(snapshot: snapshot["linkedInModel"] as! [String : Any]) ?? LinkedInModel()
        let userID          = snapshot["personalUserId"] as? String ?? String()
        let email           = snapshot["email"] as? String ?? String()
        
        let model = ProfileModel()
        
        model.email                 = email
//        model.googleModel           = google
        model.linkModel             = linkDin
        model.userInfoModel.userId  = userID
        
        return model
    }
    
    class func publicMapper(snapshot: [String:Any], model: ProfileModel) -> ProfileModel? {

        let userInfoModel   = UserInfoModel.mapper(snapshot: snapshot["userInfoModel"] as? [String : Any] ?? [String : Any]()) ?? UserInfoModel()
        let oModel          = OnlineModel.mapper(snapshot: snapshot["onlineModel"] as? [String : Any] ?? [String : Any]()) ?? OnlineModel()
        let tagsModel       = TagsModel.mapper(snapshot: snapshot["tagsModel"] as? [String : [String]] ?? [String : [String]]()) ?? TagsModel()
        let visibility      = snapshot["visibilityStatus"] as? String ?? String()
                
        model.userInfoModel     = userInfoModel
        model.onlineModel       = oModel
        model.tagsModel         = tagsModel
        model.visibilityModel.status   = visibility
        
        return model
         
    }
    
    class func dropbyMapper(snapshot: [String:Any])-> ProfileModel? {
        
        let userInfoModel   = UserInfoModel.mapper(snapshot: snapshot["userInfoModel"] as! [String : Any]) ?? UserInfoModel()
        let oModel          = OnlineModel.mapper(snapshot: snapshot["onlineModel"] as! [String : Any]) ?? OnlineModel()
        let tagsModel       = TagsModel.mapper(snapshot: snapshot["tagsModel"] as! [String : [String]]) ?? TagsModel()
        let visibility      = snapshot["visibilityStatus"] as? String ?? String()
        let geoModel        = GeoLocationModel.mapper(snapshot: snapshot) ?? GeoLocationModel()
        var activity        = snapshot["activity"] as? String ?? String()
        let dropbyActivity  = DropByActivityModel.mapper(snapshot: snapshot["activityModel"] as? [String : Any] ?? [String : Any]()) ?? DropByActivityModel()
        activity            = activity.lowercased()
        
        let model = ProfileModel()
        
        model.dropByActivityModel       = dropbyActivity
        model.userInfoModel             = userInfoModel
        model.onlineModel               = oModel
        model.tagsModel                 = tagsModel
        model.visibilityModel.status    = visibility
        model.geoLocationModel          = geoModel
        model.dropByActivity            = activity
        
        return model
    }
    
    class func toDict(profile: ProfileModel) -> [String: Any] {
        
        var dict = [String: Any]()
        
//        let google  = GoogleModel.toDict(gModel: profile.googleModel)
        let linkDin = LinkedInModel.toDict(linkModel: profile.linkModel)
        let userId  = profile.userInfoModel.userId
        let email   = profile.email
        
        dict["email"]           = email
//        dict["googleModel"]     = google
        dict["linkedInModel"]   = linkDin
        dict["personalUserId"]  = userId
        return dict
    }
    
    class func publicToDict(profile: ProfileModel) -> [String: Any] {
        
        var dict = [String: Any]()
        let userInfoModel   = UserInfoModel.toDict(profile: profile.userInfoModel)
//        let locMdl          = LocationModel.toDict(locModel: profile.locationModel)
        let oMdl            = OnlineModel.toDict(oModel: profile.onlineModel)
        let tagMdl          = TagsModel.toDict(tagsModel: profile.tagsModel)
        
        dict["userInfoModel"] = userInfoModel
        dict["onlineModel"] = oMdl
//        dict["locationModel"] = locMdl
        dict["tagsModel"] = tagMdl
        dict["visibilityStatus"] = profile.visibilityModel.status
        return dict
    }
}

class UserInfoModel {
    
    var firstName       : String
    var lastName        : String
    var avatarId        : String
    var basicInfo       : String
    var instituteName   : String
    var userId          : String
    var profilePicURL   : String
    
    init(firstName : String , lastName : String , avatarId : String, basicInfo: String, instituteName: String, userID: String, profilePicURL: String) {
        
        self.firstName      = firstName
        self.lastName       = lastName
        self.avatarId       = avatarId
        self.basicInfo      = basicInfo
        self.userId         = userID
        self.instituteName  = instituteName
        self.profilePicURL  = profilePicURL
    }
    
    convenience init(){
   
        self.init(firstName: "",
              lastName: "",
              avatarId: "",
              basicInfo: "",
              instituteName: "",
              userID: "",
              profilePicURL: "")
    }
}

//MARK:-Mapper
extension UserInfoModel {
    class func mapper(snapshot: [String:Any]) -> UserInfoModel? {
        let firstName       = snapshot["firstName"] as? String ?? ""
        let lastName        = snapshot["lastName"] as? String  ?? ""
        let avatarId        = snapshot["avatarId"] as? String ?? ""
        let userID          = snapshot["userId"] as? String ?? ""
        let instituteName   = snapshot["instituteName"] as? String ?? ""
        let basicInfo       = snapshot["basicInfo"] as? String ?? ""
        let profilePicURL   = snapshot["profilePicURL"] as? String ?? ""
        
        return UserInfoModel.init(firstName: firstName, lastName: lastName, avatarId: avatarId, basicInfo: basicInfo, instituteName: instituteName, userID: userID, profilePicURL: profilePicURL)
    }
    
    class func toDict(profile: UserInfoModel) -> [String: Any] {
        
        var dict = [String: Any]()
        dict["firstName"] = profile.firstName
        dict["lastName"] = profile.lastName
        dict["avatarId"] = profile.avatarId
        dict["userId"] = profile.userId
        dict["instituteName"] = profile.instituteName
        dict["basicInfo"] = profile.basicInfo
        dict["profilePicURL"] = profile.profilePicURL
        return dict
    }
}

class GoogleModel {
    
    var displayName : String
    var email       : String
    var familyName  : String
    var givenName   : String
    var id          : String
    var photoUrl    : String
    
    init(displayName : String , email : String , familyName : String, givenName: String, id: String, photoUrl: String) {
        self.displayName    = displayName
        self.email          = email
        self.familyName     = familyName
        self.givenName      = givenName
        self.id             = id
        self.photoUrl       = photoUrl
    }
    
    convenience init(){
        self.init(displayName: "" , email: "" , familyName: "", givenName: "", id: "", photoUrl: "")
    }
}

extension GoogleModel {
    class func mapper(snapshot: [String:Any]) -> GoogleModel? {
        
        let displayName = snapshot["displayName"] as? String ?? ""
        let email = snapshot["email"] as? String ?? ""
        let familyName = snapshot["familyName"] as? String ?? ""
        let givenName = snapshot["givenName"] as? String ?? ""
        let id = snapshot["id"] as? String ?? ""
        let photoUrl = snapshot["photoUrl"] as? String ?? ""
        
        return GoogleModel.init(displayName: displayName , email: email, familyName: familyName, givenName: givenName, id: id, photoUrl: photoUrl)
    }
    class func toDict(gModel: GoogleModel) -> [String: String] {
        
        var dict = [String: String]()
        dict["displayName"] = gModel.displayName
        dict["email"]       = gModel.email
        dict["familyName"]  = gModel.familyName
        dict["givenName"]   = gModel.givenName
        dict["id"]          = gModel.id
        dict["photoUrl"]    = gModel.photoUrl
        return dict
    }
}


class LinkedInModel {
    
    var id          : String
    var email       : String
    var photoUrl    : String
    var lastName    : String
    var firstName   : String
    
    
    init(firstName : String , lastName : String , id : String, email: String, photoUrl: String) {
        self.id         = id
        self.email      = email
        self.firstName  = firstName
        self.lastName   = lastName
        self.photoUrl   = photoUrl
    }
    
    init(firstName : String , email : String , lastName : String, id: String, photoUrl: String) {
        self.firstName  = firstName
        self.email      = email
        self.lastName   = lastName
        self.id             = id
        self.photoUrl       = photoUrl
    }
    
    convenience init(){
        self.init(firstName: "" , email: "" , lastName: "", id: "", photoUrl: "")
    }
}

extension LinkedInModel {
    class func mapper(snapshot: [String:Any]) -> LinkedInModel? {
        
        let firstName = snapshot["firstName"] as? String ?? ""
        let email = snapshot["email"] as? String ?? ""
        let lastName = snapshot["lastName"] as? String ?? ""
        let id = snapshot["id"] as? String ?? ""
        let photoUrl = snapshot["photoUrl"] as? String ?? ""
        
        return LinkedInModel.init(firstName: firstName , email: email, lastName: lastName, id: id, photoUrl: photoUrl)
    }
    class func toDict(linkModel: LinkedInModel) -> [String: String] {
        
        var dict = [String: String]()
        dict["firstName"]   = linkModel.firstName
        dict["email"]       = linkModel.email
        dict["lastName"]    = linkModel.lastName
        dict["id"]          = linkModel.id
        dict["photoUrl"]    = linkModel.photoUrl
        return dict
    }
}

class LocationModel {
    var latitude = 0.0
    var longitude = 0.0

    init(lat : Double, long: Double) {
        self.latitude   = lat
        self.longitude  = long
    }

    convenience init(){
        self.init(lat: 0.0, long: 0.0)
    }
}
extension LocationModel {
    class func mapper(snapshot: [String:Any]) -> LocationModel? {

        let locationMap = snapshot["location"] as? [String: Any] ?? [String: Any]()
        let latitude    = locationMap["latitude"] as? Double ?? 0.0
        let longitude   = locationMap["longitude"] as? Double ?? 0.0

        return LocationModel.init(lat: latitude, long: longitude)
    }
    
    class func toDict(locModel: LocationModel) -> [String: Any] {

        var dict = [String: Any]()
        dict["location"] = [
            "latitude" : locModel.latitude,
            "longitude": locModel.longitude
        ]
        dict["lastUpdatedTime"] = Timestamp.init()
        return dict
    }
}

class GeoLocationModel {
    var coordinates = GeoPoint.init(latitude: 0.0, longitude: 0.0)
    var geohash = ""
    
    init(coordinates : GeoPoint, geohash: String) {
        self.coordinates    = coordinates
        self.geohash        = geohash
    }
    
    convenience init(){
        self.init(coordinates: GeoPoint.init(latitude: 0.0, longitude: 0.0), geohash: "")
    }
}
extension GeoLocationModel {
    class func mapper(snapshot: [String:Any]) -> GeoLocationModel? {
        
        let coordinatesObj: [String:Any] = snapshot["coordinates"] as? [String : Any] ?? [String:Any]()
        let lat:Double  = coordinatesObj["_latitude"] as? Double ?? Double.init()
        let lon:Double  = coordinatesObj["_longitude"] as? Double ?? Double.init()
        let gPoint = GeoPoint.init(latitude: lat, longitude: lon)
        let gDict   = snapshot["g"] as? [String:Any] ?? [String:Any]()
        let geoHash = gDict["geohash"] as? String ?? ""
        
        return GeoLocationModel.init(coordinates: gPoint, geohash: geoHash)
    }
    class func toDict(locModel: GeoLocationModel) -> [String: Any] {
        
        var dict = [String: Any]()
        dict["coordinates"] = locModel.coordinates
        let geoHash = locModel.geohash
        dict["g"]   = ["geohash":geoHash,
                       "geopoint":locModel.coordinates]
        return dict
    }
}


class OnlineModel: NSObject {
    var currentTime = Timestamp.init()
    var isOnline = false
            
    init(time : Timestamp, onlineStatus: Bool) {
        self.currentTime    = time
        self.isOnline       = onlineStatus
    }
    
    convenience override init(){
        self.init(time: Timestamp.init(), onlineStatus: false)
    }
}
extension OnlineModel {
    class func mapper(snapshot: [String:Any]) -> OnlineModel? {
        
        let currentTime = snapshot["currentTime"] as? Timestamp ?? Timestamp.init()
        let isOnline    = snapshot["isOnline"] as? Bool ?? true
        
        return OnlineModel.init(time: currentTime, onlineStatus: isOnline)
    }
    class func toDict(oModel: OnlineModel) -> [String: Any] {
        
        var dict = [String: Any]()
        dict["currentTime"] = oModel.currentTime
        dict["isOnline"]    = oModel.isOnline
        return dict
    }
}

class TagsModel {
    var tagsList = [String]()
    init(tags: [String]) {
        self.tagsList    = tags
    }
    
    convenience init(){
        self.init(tags: [""])
    }
}
extension TagsModel {
    class func mapper(snapshot: [String:[String]]) -> TagsModel? {
        
        let tags = snapshot["tagsList"] ?? [String]()
        return TagsModel.init(tags: tags)
    }
    class func toDict(tagsModel: TagsModel) -> [String: [String]] {
        
        var dict = [String: [String]]()
        dict["tagsList"] = tagsModel.tagsList
        return dict
    }
}

class VisibilityModel {
    var status = String()
    
    init(status: String) {
        self.status    = status
    }
    
    convenience init(){
        self.init(status: String())
    }
}
extension VisibilityModel {
    class func mapper(snapshot: [String:String]) -> VisibilityModel? {
        
        let stat = snapshot["status"] ?? String()
        return VisibilityModel.init(status: stat)
    }
    class func toDict(visibilityModel: VisibilityModel) -> [String: String] {
        
        var dict = [String: String]()
        dict["status"] = visibilityModel.status
        return dict
    }
}
