//
//  MessagingHandler.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 25/01/2021.
//

import UIKit
import FirebaseFirestore

class MessagingHandler: NSObject {

    static let shared = MessagingHandler()
    var ref: Firestore
    
    required override init() {
        ref = Firestore.firestore()
    }
    
    //MARK:- Step1 - To fetch complete list of chats
    func fetchMyChatList(completion: @escaping(([MessageListModel])->Void)) {
        
        guard let userID = UserDefaults.standard.string(forKey: "UserId") else {print("No userId");return}
        ref.collection("chats").whereField("participantsList", arrayContains: userID).order(by: "createdAt", descending: true).addSnapshotListener { (snapshot, error) in
            
            if let _ = snapshot?.documents {
                
                var allChats = [MessageListModel]()
                for document in snapshot!.documents {
                    
            let chat = MessageListModel.mapper(snapshot: document.data()) ?? MessageListModel()
                    chat.chatID = document.documentID
                    allChats.append(chat)
                }
                completion(allChats)
            }
        }
    }
    
    //MARK:- Step2 - Function if we have open a chat from chat list and send a message
    func getAndUpdateMessages(withId chatId: String, completion: @escaping(([MessageDetail])->Void)) {
        ref.collection("chats").document(chatId).collection("messages").order(by: "createdAt").addSnapshotListener { (querySnapShot, error) in
            
            if let snapshot = querySnapShot {
                
                var allMessages = [MessageDetail]()
                for messageObj in snapshot.documents {
                    
                    let messageDetail = MessageDetail.mapper(snapshot: messageObj.data()) ?? MessageDetail()
                    messageDetail.messageID = messageObj.documentID
                    allMessages.append(messageDetail)
                }
                completion(allMessages)
            }
        }
    }
    
    //MARK:- Step 3 - Function to call when message icon is pressed from profile - checks if any chat exists.
    func fetchChatDetailFor(userWithId userId: String, completion: @escaping ((MessageListModel?)->Void)) {
        
        //read only
        //before starting a new chat from a person profile, check if chat exists
        guard let myUserID = UserDefaults.standard.string(forKey: "UserId") else {print("No userId");return}
        
        ref.collection("chats").whereField("participants."+myUserID, isEqualTo: true).whereField("participants."+userId, isEqualTo: true).getDocuments { (querySnapShot, error) in
            if let query = querySnapShot {
                if query.documents.count == 0 {
                    completion(nil)
                }
                else {
                 
                    for object in query.documents {
                        
                        let chat = MessageListModel.mapper(snapshot: object.data()) ?? MessageListModel()
                        chat.chatID = object.documentID
                        print(object.documentID)
                        completion(chat)
                        break
                    }
                }
            }
        }
    }
    
    //MARK:- Step 4 - Function to call when chat doesn't exist and new chat is to opened.
    func createNewChat(withUser userId: String, messageListModel: MessageListModel, completion: @escaping((MessageListModel?)->Void)) {
        
        self.ref.collection("chats").addDocument(data: MessageListModel.toDict(listModel: messageListModel)).collection("messages").addDocument(data: MessageDetail.toDict(messageModel: messageListModel.lastMessage)) { (error) in
        
            if error == nil {
                self.fetchChatDetailFor(userWithId: userId) { (chatModel) in
                    
                    completion(chatModel)
                }
            }
            else {
                completion(nil)
            }
        }
    }

    //MARK:- Step 5 - Function to call to push a message to chat list
    func pushMessage(withFriend friendID: String, forChatID chatID: String, andMessage messageObject: MessageDetail, completion: @escaping((Bool)->Void)) {
        
        ref.collection("chats").document(chatID).collection("messages").addDocument(data: MessageDetail.toDict(messageModel: messageObject)) { (error) in
            print("message pushed")
            if error == nil {
                completion(true)
            }
            else {
                completion(false)
            }
        }        
    }
    
    //MARK:- Step 6 - Function to call after message is pushed and should update chat list object as well
    func updateChatListWithLastMessage(forChatID chatID: String, andListModel listModel: MessageListModel, completion: @escaping((Bool)->Void)) {
        
        ref.collection("chats").document(chatID).updateData(MessageListModel.toDict(listModel: listModel)) { (error) in
            if error == nil {
                completion(true)
            }
            else {
                completion(false)
            }
        }
    }
    
    func updateReadCount(withID chatID: String, withUser userId: String) {
        ref.collection("chats").document(chatID).setData(["unreadCounts": [userId: 0]])
    }
    
//    func getReadCount(withID chatID: String, withUser userId: String) {
//        ref.collection("chats").document(chatID).collection("unreadCounts").getDocuments { query, Error in
//
//            print(query)
//        }
//    }
    
    func deleteChatThread(withID chatID: String, completion: @escaping((Bool)->Void)) {
        
//        ref.collection("chats").document(chatID).collection("messages")

        ref.collection("chats").document(chatID).delete { (error) in

            if error == nil {
                completion(true)
            }
            else {
                completion(false)
            }
        }
    }
    
    func getUnreadCountForChat(chatListModel: MessageListModel)->Int{
        
        let unreadCount = chatListModel.unreadCount
        
        for unread in unreadCount {
                if unread.key == ProfileHandler.shared.profileModel.userInfoModel.userId {
                    return unread.value
                }
            }
        return 0
    }
}
