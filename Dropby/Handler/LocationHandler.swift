//
//  LocationHandler.swift
//  Dropby
//
//  Created by Junaid on 28/11/2020.
//

import UIKit
import CoreLocation


protocol LocationProtocol {
    
    func updateUserLocation(location : CLLocation)
    func shouldRefreshListing()
}

class LocationHandler: NSObject {
    
    static let shared = LocationHandler()
    let locationManager = CLLocationManager()
    var location: CLLocation!
    var locationUpdateDelegate : LocationProtocol!
    var lastLocation: CLLocation?
    
}

extension LocationHandler : CLLocationManagerDelegate{
    
    // Location Manager helper stuff
    func initLocationManager() {
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let location = locations.last{
            
            //            self.locationManager.stopUpdatingLocation()
            self.location = location
            
            if lastLocation != nil {
                let distance = lastLocation!.distance(from: location)
                lastLocation = location
                if distance > 50 {
                    print("Location = %@" , location)
                    locationUpdateDelegate.updateUserLocation(location: location)
                    locationUpdateDelegate.shouldRefreshListing()
                   
                }
            }
            else {
                lastLocation = location
                locationUpdateDelegate.updateUserLocation(location: location)
                locationUpdateDelegate.shouldRefreshListing()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Errors: " + error.localizedDescription)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        
        switch status {
        case .restricted , .denied , .notDetermined:
            showLocationPermissionAlert()
        default:
            locationManager.startUpdatingLocation()
        }
        
    }
    
    func stopLocationUpdate(){
        locationManager.stopUpdatingLocation()
    }
    
    
    private func showLocationPermissionAlert(){
        
        let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
            //Redirect to Settings app
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        alertController.addAction(cancelAction)
        
        alertController.addAction(okAction)
        
        if let rootVC = UIApplication.shared.windows.first?.rootViewController{
            
            rootVC.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    func calculateDistance(to: CLLocation) -> CLLocationDistance {
        let distance = location.distance(from: to)
        return distance
    }
}
