//
//  APIManager.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 12/01/2021.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseFirestore

typealias completion = (Bool) -> Void
let kBaseURL = "https://us-central1-dropby-7acd3.cloudfunctions.net/"

class APIManager: NSObject {

    static let shared = APIManager()
    
    var check = false
    func fetchNearbyUsers(callback: @escaping completion){
        let session = URLSession.shared
        let url = URL(string: kBaseURL+"nearby/")!
        
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }
            print(idToken!)

            // Send token to your backend via HTTPS
            // ...
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let jsonString = String(data: responseData, encoding: .utf8) {
                        
                        ProfileHandler.shared.nearByAllUsers.removeAll()
                        
                        let jsonData = jsonString.toJSON() as? [[String:Any]]
                        if jsonData != nil {
                            for object in jsonData! {
                                let userObject = ProfileModel.publicMapper(snapshot: object, model: ProfileModel()) ?? ProfileModel()
                                ProfileHandler.shared.nearByAllUsers.append(userObject)
                            }
                            callback(true)
                        }
                        else {
                            callback(false)
                        }
                    }
                    else {
                        callback(false)
                    }
                }
                else{
                    callback(false)
                }
            }.resume()
          }
    }
    
    func fetchDropbyUsers(callback: @escaping completion){
        let session = URLSession.shared
        let url = URL(string: kBaseURL+"dropby/")!
        
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }
            print(idToken!)

            // Send token to your backend via HTTPS
            // ...
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let jsonString = String(data: responseData, encoding: .utf8) {
                        
                        ProfileHandler.shared.dropbyUsers.removeAll()
                        
                        let jsonData = jsonString.toJSON() as? [[String:Any]]
                        if jsonData != nil {
                            for object in jsonData! {
                                let userObject = ProfileModel.dropbyMapper(snapshot: object ) ?? ProfileModel()
                                ProfileHandler.shared.dropbyUsers.append(userObject)
                            }
                            callback(true)
                        }
                        else {
                            callback(false)
                        }                        
                    }
                    else {
                        callback(false)
                    }
                }
                else{
                    callback(false)
                }
            }.resume()
          }
    }
    
    func pushLocation(latitude: Double, longitude: Double, callback: @escaping completion) {
        let session = URLSession.shared
        let url = URL(string: kBaseURL+"visibility/user/location/")
        if url == nil {
            return
        }
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }
            print(idToken!)

            // Send token to your backend via HTTPS
            // ...
            let params: [String: Any] = ["latitude": latitude,
                                         "longitude" : longitude]
            
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
            }
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")

            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let _ = String(data: responseData, encoding: .utf8) {
                                                
                        callback(true)
                    }
                    callback(false)
                }
                callback(false)
            }.resume()
          }
    }
    
    func fetchMutualFriendsList(with user: String, callback: @escaping ((Bool, [[String:Any]]?)->Void)) {
        
        let session = URLSession.shared
        let url = URL(string: kBaseURL+"friend/mutualFriendList/"+user)!
        
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }
            print(idToken!)

            // Send token to your backend via HTTPS
            // ...
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let jsonString = String(data: responseData, encoding: .utf8) {
                        
                        
                        
                        let jsonData = jsonString.toJSON() as? [[String:Any]]
                        
                        callback(true, jsonData)
                    }
                    else {
                        callback(false, nil)
                    }
                }
                else{
                    callback(false, nil)
                }
            }.resume()
          }
    }
    
    func amIVisibleTo(userWith userID: String, for type: String, callback: @escaping ((Bool)->Void)) {
        
        let session = URLSession.shared
        let url = URL(string: kBaseURL+"visibility/user/"+type+"/"+userID)!
        
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }
            print(idToken!)

            // Send token to your backend via HTTPS
            // ...
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let jsonString = String(data: responseData, encoding: .utf8) {
                        
                        
                        
                        let jsonData = jsonString.toJSON() as? [String:Any]
                        let isVisible = jsonData?["isVisible"] as? Bool ?? true
                        callback(isVisible)
                    }
                    else {
                        callback(false)
                    }
                }
                else{
                    callback(false)
                }
            }.resume()
          }
    }
    
    func fetchBlockList(callback: @escaping (([[String:Any]]?)->Void)) {
        
        let session = URLSession.shared
        let url = URL(string: kBaseURL+"user/block-list")!
        
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }
            print(idToken!)

            // Send token to your backend via HTTPS
            // ...
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let jsonString = String(data: responseData, encoding: .utf8) {
                        
                        
                        
                        let jsonData = jsonString.toJSON() as? [[String:Any]]
                        
                        callback(jsonData)
                    }
                    else {
                        callback(nil)
                    }
                }
                else{
                    callback(nil)
                }
            }.resume()
          }
    }
    
    func fetchHiddenDropbyFeatures(callback: @escaping (([[String:Any]]?)->Void)) {
        
        let session = URLSession.shared
        let url = URL(string: kBaseURL+"visibility/hidden/dropby")!
        
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }

            // Send token to your backend via HTTPS
            // ...
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let jsonString = String(data: responseData, encoding: .utf8) {
                        
                        
                        
                        let jsonData = jsonString.toJSON() as? [[String:Any]]
                        
                        callback(jsonData)
                    }
                    else {
                        callback(nil)
                    }
                }
                else{
                    callback(nil)
                }
            }.resume()
          }
    }


    func fetchHiddenNearbyFeatures(callback: @escaping (([[String:Any]]?)->Void)) {
        
        let session = URLSession.shared
        let url = URL(string: kBaseURL+"visibility/hidden/nearby")!
        
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }
            print(idToken!)

            // Send token to your backend via HTTPS
            // ...
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let jsonString = String(data: responseData, encoding: .utf8) {
                        
                        
                        
                        let jsonData = jsonString.toJSON() as? [[String:Any]]
                        
                        callback(jsonData)
                    }
                    else {
                        callback(nil)
                    }
                }
                else{
                    callback(nil)
                }
            }.resume()
          }
    }
    
    func softDeleteAccount(callback: @escaping ((Bool)->Void)) {
        
        let session = URLSession.shared
        let url = URL(string: kBaseURL+"user/delete")
        if url == nil {
            return
        }
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }
            print(idToken!)

            // Send token to your backend via HTTPS
            // ...
            let params: [String: Any] = [String:Any]()
            
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
            }
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")

            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let _ = String(data: responseData, encoding: .utf8) {
                                                
                        callback(true)
                    }
                    callback(false)
                }
                callback(false)
            }.resume()
          }
    }
    
    func uploadContacts(contacts: [String], callback: @escaping ((Bool)->Void)) {
        
        let session = URLSession.shared
        let url = URL(string: kBaseURL+"contactPost")
        if url == nil {
            return
        }
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }
            // Send token to your backend via HTTPS
            // ...
            let params: [String: Any] = ["contactList" : contacts]
            
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
            }
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")

            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let _ = String(data: responseData, encoding: .utf8) {
                                                
                        callback(true)
                    }
                    else {
                        callback(false)
                    }
                }
                else {
                    callback(false)
                }
                
            }.resume()
          }
    }
    
    func fetchContactsDetail(callback: @escaping (([[String:Any]]?)->Void)) {
        
        let session = URLSession.shared
        let url = URL(string: kBaseURL+"contact")!
        
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }
            print(idToken!)

            // Send token to your backend via HTTPS
            // ...
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let jsonString = String(data: responseData, encoding: .utf8) {
                        
                        
                        
                        let jsonData = jsonString.toJSON() as? [[String:Any]]
                        
                        callback(jsonData)
                    }
                    else {
                        callback(nil)
                    }
                }
                else{
                    callback(nil)
                }
            }.resume()
          }
    }
    
    func checkEmailAllowed(email: String, callback: @escaping (([String:Any]?)->Void)) {
        
        let session = URLSession.shared
        let url = URL(string: kBaseURL+"email/checkEmail/"+email)!
        
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }
            print(idToken!)

            // Send token to your backend via HTTPS
            // ...
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let jsonString = String(data: responseData, encoding: .utf8) {
                        
                                                
                        let jsonData = jsonString.toJSON() as? [String:Any]
                        
                        callback(jsonData)
                    }
                    else {
                        callback(nil)
                    }
                }
                else{
                    callback(nil)
                }
            }.resume()
          }
    }
    
    //MARK:- Email Verification
    func verifyEmail(callback: @escaping (([String:Any]?)->Void)) {
                
        let session = URLSession.shared
        let url = URL(string: kBaseURL+"email/isVerified")!
        
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }
            print(idToken!)

            // Send token to your backend via HTTPS
            // ...
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let jsonString = String(data: responseData, encoding: .utf8) {
                        
                                                
                        let jsonData = jsonString.toJSON() as? [String:Any]
                        
                        callback(jsonData)
                    }
                    else {
                        callback(nil)
                    }
                }
                else{
                    callback(nil)
                }
            }.resume()
          }
    }
    
    func sendVerificationEmail(callback: @escaping (([String:Any]?)->Void)) {
        
        let session = URLSession.shared
        let url = URL(string: kBaseURL+"email/sendVerificationEmail")!
        
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }
            print(idToken!)

            // Send token to your backend via HTTPS
            // ...
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let jsonString = String(data: responseData, encoding: .utf8) {
                        
                                                
                        let jsonData = jsonString.toJSON() as? [String:Any]
                        
                        callback(jsonData)
                    }
                    else {
                        callback(nil)
                    }
                }
                else{
                    callback(nil)
                }
            }.resume()
          }
    }
    
    func getWebContent(type: WebViewType,  callback: @escaping ((String?)->Void)) {
        
        let session = URLSession.shared
        
        var typeString = ""
        if type == .terms {
            typeString = "/termsAndConditions"
        }else if type == .about_us {
            typeString = "/aboutUs"
        }
        else  if type == .privacy {
            typeString = "/privicyPolicy"
        }
        
        let url = URL(string: kBaseURL+"/common"+typeString)!
        
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }
            print(idToken!)

            // Send token to your backend via HTTPS
            // ...
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let jsonString = String(data: responseData, encoding: .utf8) {
                                                
                        callback(jsonString)
                    }
                    else {
                        callback(nil)
                    }
                }
                else{
                    callback(nil)
                }
            }.resume()
          }
    }
    
    func getUserProfile(withUserId userID: String, callback: @escaping (([String:Any]?)->Void)) {
        
        let session = URLSession.shared
        let url = URL(string: kBaseURL+"user/info/"+userID)!
        
        Auth.auth().currentUser?.getIDTokenForcingRefresh(false) { idToken, error in
            if error != nil {
              // Handle error
              return;
            }
            print(idToken!)

            // Send token to your backend via HTTPS
            // ...
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("Bearer "+idToken!, forHTTPHeaderField: "Authorization")
            
            let _ = session.dataTask(with: request) { (data, response, error) in
                
                if let responseData = data {
                    if let jsonString = String(data: responseData, encoding: .utf8) {
                        
                                                
                        let jsonData = jsonString.toJSON() as? [String:Any]
                        
                        callback(jsonData)
                    }
                    else {
                        callback(nil)
                    }
                }
                else{
                    callback(nil)
                }
            }.resume()
          }
    }
}

extension String {
    func toJSON() -> Any? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    }
}
