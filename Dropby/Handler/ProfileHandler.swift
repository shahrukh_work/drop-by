//
//  ProfileHandler.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 17/11/2020.
//

import UIKit
import GoogleSignIn
import FirebaseFirestore
import MBProgressHUD
import FirebaseMessaging
//import Geofirestore
//import GeoFire
import CoreLocation
import Firebase

protocol UserListingRefreshProtocol {
    
    func dropbyUsersDidUpdate()
    func nearbyUserDidUpdate()
    func locationUpdated(location: CLLocation)
}


class ProfileHandler: NSObject {

    private override init(){}
    static let shared = ProfileHandler()
    
    var profileModel    = ProfileModel ()
    var authInteractor  = AuthenticationInteractor.init(manager: AuthenticationFirebase()).manager
    var nearByAllUsers  = [ProfileModel]()
    var dropbyUsers     = [ProfileModel]()
    var friendRequests  = [String]()
    var friends         = [String]()
    var blockedContacts = [String]()
    var dropByActivity: DropByActivityModel?
    var allActivities   = [DropByActivityModel]()
    var userSettings    = SettingsModel()    
    var userListingDelegate: UserListingRefreshProtocol?
    var location: CLLocation!
    var notifications   = [NotificationsModel]()
    var chatList        = [MessageListModel]()
}

extension ProfileHandler {
    
    func setupLocationListener() {
        
        ProfileHandler.shared.fetchUserSettings { (status) in
            
            LocationHandler.shared.initLocationManager()
            LocationHandler.shared.locationUpdateDelegate = self
        }
    }
    
//    func updateModelWithGoogleModel (userModel: GIDGoogleUser){
//        
//        let dimension = round(100 * UIScreen.main.scale)
//        ProfileHandler.shared.profileModel.googleModel = GoogleModel.init(displayName: userModel.profile.name, email: userModel.profile.email, familyName: userModel.profile.familyName, givenName: userModel.profile.givenName, id: userModel.userID, photoUrl: userModel.profile.imageURL(withDimension: UInt(dimension))?.absoluteString ?? "")
//        ProfileHandler.shared.profileModel.userInfoModel.firstName = userModel.profile.givenName
//        ProfileHandler.shared.profileModel.userInfoModel.lastName = userModel.profile.familyName
//    }
    
    func updateUserModelWithEmail (email: String) {
        ProfileHandler.shared.profileModel.email = email
    }
    
    func updateModelWithLinkedInModel(linkedInEmailModel : LinkedInEmailModel? , linkedInProfileModel : LinkedInProfileModel?) {
        
        ProfileHandler.shared.profileModel.linkModel = LinkedInModel.init(firstName: linkedInProfileModel?.firstName.localized.enUS ?? "", lastName: linkedInProfileModel?.lastName.localized.enUS ?? "", id: linkedInProfileModel?.id ?? "", email: linkedInEmailModel?.elements[0].elementHandle.emailAddress ?? "", photoUrl: linkedInProfileModel?.profilePicture.displayImage.elements[2].identifiers[0].identifier ?? "")
        ProfileHandler.shared.profileModel.userInfoModel.firstName = linkedInProfileModel?.firstName.localized.enUS ?? ""
        ProfileHandler.shared.profileModel.userInfoModel.lastName = linkedInProfileModel?.lastName.localized.enUS ?? ""
    }
    
    func updateModelWithLocationModel(latitude: Double, longitude: Double, completion: @escaping((Bool)->Void)) {
        
//        APIManager.shared.pushLocation(latitude: latitude, longitude: longitude) { (success) in
//            completion(success)
//        }
        
        ProfileHandler.shared.authInteractor.updateLocation(latitude: latitude, longitude: longitude)
        completion(true)
    }
    
    func updateOnlineModelWithTStamp() {
                
        ProfileHandler.shared.profileModel.onlineModel = OnlineModel.init(time: Timestamp.init(), onlineStatus: true)
    }
    
    func synchroniseProfileData(callback: @escaping ((Bool)->Void)) {
        ///Adding into Profile model
        
        guard let userID = UserDefaults.standard.string(forKey: "UserId") else {print("No userId");return}
        ProfileHandler.shared.profileModel.userInfoModel.userId = userID
        
        authInteractor.add(profile: ProfileHandler.shared.profileModel) {
                        
            callback(true)
            
        } onError: { (error) in
                        
            callback(false)
        }
    }
    
    func fetchProfileModelFromServer(callback: @escaping ((Bool)->Void)) {
        
        authInteractor.fetch { (status) in
            callback(status)
        } onError: { (error) in
            callback(false)
        }
    }
    
    func fetchUserSettings(onSuccess: @escaping((Bool)->Void)) {
        authInteractor.getUserSettings { (object) in
            
            let settingsModel = SettingsModel.mapper(snapshot: object)
            if settingsModel != nil {
                ProfileHandler.shared.userSettings = settingsModel!
            }
            onSuccess(true)
        } onError: { (error) in
            onSuccess(false)
        }
    }
    
    func setVisibility(visibility: String, callback: @escaping((Bool)->Void)) {
        profileModel.visibilityModel.status = visibility
        ProfileHandler.shared.authInteractor.updateVisibility { (success) in
            callback(success)
        }
//        ProfileHandler.shared.synchroniseProfileData { (success) in
//            callback(success)
//        }
    }
    
    func checkProfileExists(userID: String) -> Bool {
        var found = false
        if nearByAllUsers.count > 0  {
            for object in nearByAllUsers {
                if object.userInfoModel.userId == userID {
                    found = true
                    break
                }
            }
            
        }
        return found
    }
    
    func sendFriendRequest(user: ProfileModel, completion: @escaping((Bool)->Void)) {
        
        //first check if the user has sent us any friend request
        //fetch all friend requests
        
        ProfileHandler.shared.authInteractor.fetchFriendRequestsSent(forUserID: ProfileHandler.shared.profileModel.userInfoModel.userId) { (success, requestsReceived) in
            if success && requestsReceived != nil {
                
                for request in requestsReceived! {
                    if request == user.userInfoModel.userId {
                        //meaning user has already sent me a friend request, so rather sending request i have to add them in my and his friends list
                        ProfileHandler.shared.authInteractor.addFriend(user: user) { (success) in
                            completion(success)
                            //add notid
                            self.addNotificationObjectToSelf(user: user)
                        }
                        return
                    }
                }
            }
            //send request
            ProfileHandler.shared.sendRequestNow(toUser: user.userInfoModel.userId) { (success) in
                completion(success)
                self.addNotificationObjectToUserForFriendRequest(user: user)
            }
            
        }
    }
    
    func addNotificationObjectToUserForFriendRequest(user: ProfileModel) {
        
        let notificationSelf = NotificationsModel.init(
            body: ProfileHandler.shared.profileModel.userInfoModel.firstName + " " + ProfileHandler.shared.profileModel.userInfoModel.lastName + " has sent you connection request",
            time: Timestamp.init(),
            model: GenericUserModel.init(
                friendUId: user.userInfoModel.userId,
                friendUName: user.userInfoModel.firstName,
                picUrl: ProfileHandler.shared.profileModel.userInfoModel.profilePicURL))
        
        
        ProfileHandler.shared.authInteractor.addToNotificationList(forUser: user.userInfoModel.userId, notificationModel: notificationSelf) { (success) in
            
        }
    }
    
    
    func addNotificationObjectToSelf(user: ProfileModel) {
        
        let notificationSelf = NotificationsModel.init(
            body: user.userInfoModel.firstName + " " + user.userInfoModel.lastName + " has accepted your connection request",
            time: Timestamp.init(),
            model: GenericUserModel.init(
                friendUId: user.userInfoModel.userId,
                friendUName: user.userInfoModel.firstName,
                picUrl: user.userInfoModel.profilePicURL))
        
        
        ProfileHandler.shared.authInteractor.addToNotificationList(forUser: ProfileHandler.shared.profileModel.userInfoModel.userId, notificationModel: notificationSelf) { (success) in
            
        }
        
        let notificationOther = NotificationsModel.init(
            body: ProfileHandler.shared.profileModel.userInfoModel.firstName + " " + ProfileHandler.shared.profileModel.userInfoModel.lastName + " has accepted your connection request",
            time: Timestamp.init(),
            model: GenericUserModel.init(
                friendUId: ProfileHandler.shared.profileModel.userInfoModel.userId,
                friendUName: ProfileHandler.shared.profileModel.userInfoModel.firstName,
                picUrl: ProfileHandler.shared.profileModel.userInfoModel.profilePicURL))
        
        ProfileHandler.shared.authInteractor.addToNotificationList(
            forUser: user.userInfoModel.userId,
            notificationModel: notificationOther) { (success) in
            
        }
    }
    
    private func sendRequestNow(toUser userID: String, completion: @escaping((Bool)->Void)) {
        //create a friend request model. and put it in
        let mySelf = ProfileHandler.shared.profileModel
        let requestObject = FriendRequestModel.init(friendUId: mySelf.userInfoModel.userId, friendUName: mySelf.userInfoModel.firstName, isIgnore: false, isIgnoreUpdated: -1, picUrl: mySelf.userInfoModel.profilePicURL)
        ProfileHandler.shared.authInteractor.sendFriendRequest(with: userID, and: requestObject) { (success) in
            completion(success)
        }
    }
    func checkUserFriendStatus(userId: String, completion: @escaping((FriendStatus?)->Void)) {
                
        //check friends table first
        var friendStatus = FriendStatus.notFriends
        guard let myuserID = UserDefaults.standard.string(forKey: "UserId") else {print("No userId");return}
        
        ProfileHandler.shared.authInteractor.fetchAllFriends(for: myuserID, and: true, onSuccess:{ (success, allFriends) in
            if success {
                ProfileHandler.shared.friends.removeAll()
                ProfileHandler.shared.friends = allFriends!
                for user in self.friends {
                    if user == userId {
                        
                        friendStatus = .friends
                        break
                    }
                }
            }
            if friendStatus == .notFriends {
                
                ProfileHandler.shared.authInteractor.fetchFriendRequestsSent(forUserID: userId) { (success, friendRequests) in
                    if success && friendRequests != nil {
                        for request in friendRequests! {
                            if request == ProfileHandler.shared.profileModel.userInfoModel.userId {
                                friendStatus = .friendRequested
                                break
                            }
                        }
                    }
                    completion(friendStatus)
                }
            }
            else {
                completion(friendStatus)
            }
            
        }, onError: { (error) in
            completion(nil)
        })
    }
    
    func fetchFriendsSatus(forUser userID: String) -> FriendStatus{
        
        for friend in friends {
            if friend == userID {
                //friends
                return .friends
            }
        }
        for requested in friendRequests {
            if requested == userID {
                //friends requested
                return .friendRequested
            }
        }
        return .notFriends
    }
    
    func getUser(byID userID: String) -> ProfileModel? {
        for object in nearByAllUsers {
            if object.userInfoModel.userId == userID {
                return object
            }
        }
        return nil
    }
    
    func getCommonFriendsWith(userID: String, completion: @escaping(([GenericUserModel]?)->Void)) {
        
        APIManager.shared.fetchMutualFriendsList(with: userID) { (success, jsonData) in
            
            if success && jsonData != nil {
                
                var mutualContacts = [GenericUserModel]()
                for contact in jsonData! {
                    let userObject = GenericUserModel.mapper(snapshot: contact) ?? GenericUserModel()
                    mutualContacts.append(userObject)
                }
                completion(mutualContacts)
            }
            else {
                completion(nil)
            }
        }
    }
    
    func fetchChatList(completion: @escaping(()->Void)) {
        MessagingHandler.shared.fetchMyChatList { chats in
                        
            self.chatList = chats
            completion()
        }
    }
    
    func getUnreadCount()->Int {
        var unreadCount = 0
        for chat in self.chatList {
            let unread = MessagingHandler.shared.getUnreadCountForChat(chatListModel: chat)
            unreadCount = unreadCount + unread
        }
        return unreadCount
    }
    
    func logout (view: UIView, completion: @escaping(()->Void)) {
        
        let myUserID = ProfileHandler.shared.profileModel.userInfoModel.userId
        
        MBProgressHUD.showAdded(to: view, animated: true)
        Messaging.messaging().unsubscribe(fromTopic: "message-"+myUserID) { (error) in
            
            MBProgressHUD.hide(for: view, animated: true)
            print("UnSubscribed from topic successfully")
            completion()
            
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            
            ProfileHandler.shared.nearByAllUsers.removeAll()
            ProfileHandler.shared.friendRequests.removeAll()
            ProfileHandler.shared.friends.removeAll()
            ProfileHandler.shared.blockedContacts.removeAll()
            ProfileHandler.shared.dropByActivity = nil
            ProfileHandler.shared.allActivities.removeAll()
            
            // start from beginning
            guard let nav = UIStoryboard(name: "UserAuthentication", bundle: nil).instantiateViewController(withIdentifier: "SignInNav") as? UINavigationController else {print("no home tab");return}
            
            let vc = LoginViewController.instantiate(fromAppStoryboard: .UserAuthentication)
            nav.pushViewController(vc, animated: true)
            UIApplication.shared.windows.first?.rootViewController = nav
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    }
}

extension ProfileHandler: LocationProtocol {
    
    func refreshNearbyListing() {
        if self.location != nil {
            ProfileHandler.shared.updateUserLocation(location: self.location)
        }
        
//        APIManager.shared.fetchNearbyUsers { (success) in
//            if success {
//                if let delegate = self.userListingDelegate {
//                    delegate.nearbyUserDidUpdate()
//                }
//            }
//        }
    }
    
    func refreshDropbyListing() {
        APIManager.shared.fetchDropbyUsers { (success) in
            if success {
                if let delegate = self.userListingDelegate {
                    delegate.dropbyUsersDidUpdate()
                }
            }
        }
    }
    
    
    func shouldRefreshListing() {
    
        // Refresh nearby
        self.refreshNearbyListing()
        
        // Refresh dropby
        self.refreshDropbyListing()
    }
    
    func updateUserLocation(location: CLLocation) {
        
        self.location = location
        ProfileHandler.shared.updateModelWithLocationModel(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude) { (success) in
            // do something
            // should refresh listing?
            if let delegate = self.userListingDelegate {
                delegate.locationUpdated(location: location)
            }
        }
    }
    
    //MARK:- Notifications
    func fetchNotifications() {
        
    }
    
    //TODO:- CALL to function missing
    func subscribeToTopic() {
        
        let myUserID = ProfileHandler.shared.profileModel.userInfoModel.userId
        
        Messaging.messaging().subscribe(toTopic: "message-"+myUserID) { error in
          print("Subscribed to topic")
        }
    }
    
    func checkEmailVerified(callback: @escaping ((Bool)->Void)) {
        
        APIManager.shared.verifyEmail { (json) in
            
            if json != nil {
                DispatchQueue.main.async {
                    callback(json!["status"] as? Bool ?? false)
                }
            }
        }
    }
}
