//
//  AppHandler.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 18/11/2020.
//

import UIKit
import MBProgressHUD

class AppHandler: NSObject {
    
    static let shared = AppHandler()
    private override init(){}
    

    func showEmailNotVerifiedPopup() -> VerifyAccountPopup {
                         
        let vc = VerifyAccountPopup.instantiate(fromAppStoryboard: .PopUp)
        vc.modalPresentationStyle   = .overFullScreen
        vc.imageViewHidden          = false
        vc.cancelBtnHidden          = false
        vc.okBtnTitle               = "OK"
        vc.cancelBtnTitle           = "RESEND"
        
        return vc
    }
}

extension AppHandler {
    
    func decideAppState () {
        
    }
    
}

extension Int {

    func secondsToTime() -> String {

        let (h,m,s) = (self / 3600, (self % 3600) / 60, (self % 3600) % 60)

        let h_string = h < 10 ? "0\(h)" : "\(h)"
        let m_string =  m < 10 ? "0\(m)" : "\(m)"
        let s_string =  s < 10 ? "0\(s)" : "\(s)"

        return "\(h_string):\(m_string):\(s_string)"
    }
}
