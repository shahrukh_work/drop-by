//
//  AuthenticationFirebase.swift
//  Dropby
//
//  Created by Junaid on 12/11/2020.
//

import Foundation
import Firebase
import FirebaseDatabase
import FirebaseFirestore


internal protocol  AuthenticationManager {
        
    func add(profile: ProfileModel, onSuccess: @escaping () -> Void, onError: ErrorClosure?)
    func fetch(onSuccess: @escaping (Bool) -> Void, onError: ErrorClosure?)
    func fetchUser(withUserId userID: String, onSuccess: @escaping(ProfileModel?)->Void)
    func fetchAllNearBy(onSuccess: @escaping ([[String:Any]]) -> Void, onError: ErrorClosure?)
    func uploadPictureToStorage(url: URL, completion: @escaping((String?, Bool)->Void))
    func uploadScreenShotToStorage (url: URL, completion: @escaping((String?, Bool)->Void))
    func realtimeUpdates()
    func sendFriendRequest(with userID: String, and object: FriendRequestModel, onSuccess: @escaping(Bool)->Void)
    func fetchAllFriends(for userID: String, and isSelf: Bool, onSuccess: @escaping(Bool, [String]?)->Void, onError: ErrorClosure?)
    func fetchFriendRequestsSent(forUserID userId: String, onSuccess: @escaping(Bool, [String]?)->Void)
    func addFriend(user: ProfileModel, onSuccess: @escaping(Bool)->Void)
    func reportUser(model: ReportModel, onSuccess: @escaping(Bool)->Void)
    func contactUs(model: ContactUsModel, onSuccess: @escaping(Bool)->Void)
    func addToBlockList(model: ProfileModel, onSuccess: @escaping(Bool)->Void)
    func setDropByActivity(model: DropByActivityModel, onSuccess: @escaping(Bool)->Void)
    func deleteDropbyActivity(onSuccess: @escaping(Bool)->Void)
    func fetchAllActivities(onSuccess: @escaping([[String:Any]])->Void, onError: ErrorClosure?)
    func addToNearbyInvisibleList(userID: String, onSuccess: @escaping(Bool)->Void)
    func addToDropbyInvisibleList(userID: String, onSuccess: @escaping(Bool)->Void)
    func updateVisibility(onSuccess: @escaping(Bool)->Void)
    func loadAllTables()
    func unBlock(user blockedID: String, forUser userId: String, dict: [String: Any], onSuccess: @escaping(Bool)->Void)
    func unHideDropby(user hiddenID: String, forUser userId: String, dict: [String: Any], onSuccess: @escaping(Bool)->Void)
    func getUserSettings(onSuccess: @escaping([String: Any])->Void, onError: ErrorClosure?)
    func updateSettings(setting: SettingsModel)
    func updateLocation(latitude: Double, longitude: Double)
    func addToNotificationList(forUser userId: String, notificationModel model:NotificationsModel, onSuccess: @escaping(Bool)->Void)
    func removeConnection(user: ProfileModel, onSuccess: @escaping(Bool)->Void)
    func cancelConnectionRequest(user: ProfileModel, onSuccess: @escaping(Bool)->Void)
}

class AuthenticationInteractor{
    
    var manager: AuthenticationManager
    
    init(manager: AuthenticationManager){
        self.manager = manager
    }
}

 class AuthenticationFirebase: AuthenticationManager {
    
    var ref: Firestore
    
    required init() {
        ref = Firestore.firestore()
    }
    
    func add(profile: ProfileModel, onSuccess: @escaping () -> Void, onError: ErrorClosure?){
        
        let child = ProfileModel.toDict(profile: profile)
        let childPublic = ProfileModel.publicToDict(profile: profile)
        /// add Into Public User
        let userID = ProfileHandler.shared.profileModel.userInfoModel.userId
        ref.collection("usersPrivate").document(userID).setData(child) { error  in
            if let err = error, let retError = onError {
                retError(err)
            }
            else {
                print("Success for private")
            }
            
            self.ref.collection("usersPublic").document(userID).setData(childPublic) { error  in
                if let err = error, let retError = onError {
                    retError(err)
                }
                else {
                    onSuccess()
                }
            }
            
        }
    }
    
    func updateVisibility(onSuccess: @escaping(Bool)->Void){
        
        let userID = ProfileHandler.shared.profileModel.userInfoModel.userId
        let visibilityStatus = ProfileHandler.shared.profileModel.visibilityModel.status
        ref.collection("usersPublic").document(userID).setData(["visibilityStatus" : visibilityStatus], merge: true)
        onSuccess(true)
    }
    
    func fetch(onSuccess: @escaping(Bool)->Void, onError: ErrorClosure?) {
        
        guard let userID = UserDefaults.standard.string(forKey: "UserId") else {print("No userId");return}
        ref.collection("usersPrivate").document(userID).getDocument { (document, error) in
            if let err = error, let retError = onError {
                retError(err)
            }
            else {
                if document?.data() != nil {
                    ProfileHandler.shared.profileModel = ProfileModel.mapper(snapshot: document?.data() ?? ["":""]) ?? ProfileModel()
                    self.ref.collection("usersPublic").document(userID).getDocument { (document, error) in
                        if let err = error, let retError = onError {
                            retError(err)
                        }
                        else {
                            if document?.data() != nil {
                                ProfileHandler.shared.profileModel = ProfileModel.publicMapper(snapshot: document?.data() ?? ["":""], model: ProfileHandler.shared.profileModel) ?? ProfileModel()
                                
                                onSuccess(true)
                            }
                            else {
                                onSuccess(false)
                            }
                        }
                    }
                }
                else {
                    onSuccess(false)
                }
            }
        }
    }
    
    func fetchUser(withUserId userID: String, onSuccess: @escaping(ProfileModel?)->Void) {
        
        self.ref.collection("usersPublic").document(userID).getDocument { (document, error) in
            
            if document?.data() != nil {
                
                let userModel = ProfileModel.publicMapper(snapshot: document?.data() ?? ["":""], model: ProfileModel()) ?? ProfileModel()
                onSuccess(userModel)
            }
            else {
                onSuccess(nil)
            }
        }
    }

    
    func fetchAllNearBy(onSuccess: @escaping([[String:Any]])->Void, onError: ErrorClosure?) {
                        
        ref.collection("usersPublic").getDocuments { (documents, error) in
            if let err = error, let retError = onError {
                retError(err)
            }
            else {
                
                if documents != nil {
                    
                    var arrayOfDict = [[String:Any]]()
                    
                    for obj in (documents?.documents)! {
                        print(obj.data())
                        arrayOfDict.append(obj.data())
                    }
                    onSuccess(arrayOfDict)
                }
            }
        }
    }
    
    func uploadPictureToStorage(url: URL, completion: @escaping((String?, Bool)->Void)) {
        
        let storage = Storage.storage()

        // Create a storage reference from our storage service
        let storageRef = storage.reference()
        
        // File located on disk
        let localFile = url

        // Create a reference to the file you want to upload
        let spaceRef = storageRef.child("profilePictures/profile-"+ProfileHandler.shared.profileModel.userInfoModel.userId+".jpg")
        // Upload the file to the path "images/rivers.jpg"
        spaceRef.putFile(from: localFile, metadata: nil) { metadata, error in
            guard metadata != nil else {
            // Uh-oh, an error occurred!
                
                completion(nil,false)
                print(error.debugDescription)
            return
          }
            spaceRef.downloadURL { (url, error) in
                if (error == nil) {
                    completion(url?.absoluteString, true)
                }
            }
            
        }
    }
    
    func uploadScreenShotToStorage (url: URL, completion: @escaping((String?, Bool)->Void)) {
        
        let storage = Storage.storage()

        // Create a storage reference from our storage service
        let storageRef = storage.reference()
        
        // File located on disk
        let localFile = url

        // Create a reference to the file you want to upload
        let spaceRef = storageRef.child("reportScreenShots/reportedBy-"+ProfileHandler.shared.profileModel.userInfoModel.userId+".jpg")
        // Upload the file to the path "images/rivers.jpg"
        spaceRef.putFile(from: localFile, metadata: nil) { metadata, error in
            guard metadata != nil else {
            // Uh-oh, an error occurred!
                
                completion(nil,false)
                print(error.debugDescription)
            return
          }
            spaceRef.downloadURL { (url, error) in
                if (error == nil) {
                    completion(url?.absoluteString, true)
                }
            }
            
        }
    }
    
    
    func fetchFriendRequestsSent(forUserID userId: String, onSuccess: @escaping(Bool, [String]?)->Void) {
                
        let myID = ProfileHandler.shared.profileModel.userInfoModel.userId
        if myID == userId && ProfileHandler.shared.friendRequests.count > 0 {
            onSuccess(true, ProfileHandler.shared.friendRequests)
            return
        }
        ref.collection("friendsRequests").document(userId).getDocument { (document, error) in
            if document?.data() != nil {
                                    
                var arrayOfRequests = [String]()
                
                for obj in (document?.data())! {
                    if obj.key == "peopleList" {
                        let array = obj.value as? [String] ?? [String]()
                        for realObject in array {
                            arrayOfRequests.append(realObject)
                        }
                    }
                }
                if myID == userId {
                    ProfileHandler.shared.friendRequests = arrayOfRequests
                }
                onSuccess(true, arrayOfRequests)
            }
            else {
                onSuccess(false, nil)
            }
        }
    }
    
    func fetchAllFriends(for userID: String, and isSelf: Bool, onSuccess: @escaping(Bool, [String]?)->Void, onError: ErrorClosure?) {
        
        if isSelf && ProfileHandler.shared.friends.count > 0 {
            onSuccess(true, ProfileHandler.shared.friends)
            return
        }
        guard let userID = UserDefaults.standard.string(forKey: "UserId") else {print("No userId");return}
        ref.collection("friends").document(userID).getDocument { (document, error) in
            if let err = error, let retError = onError {
                retError(err)
            }
            else {
                if document?.data() != nil {
                                        
                    var allFriends = [String]()
                    for obj in (document?.data())! {
                        if obj.key == "peopleList" {
                            let array = obj.value as? [String] ?? [String]()
                            for realObject in array {
                                allFriends.append(realObject)
                            }
                        }
                        
                    }
                    onSuccess(true, allFriends)
                }
                else {
                    onSuccess(false, nil)
                }
            }
        }
    }
    
    func sendFriendRequest(with userID: String, and object: FriendRequestModel, onSuccess: @escaping(Bool)->Void) {
                        
        ref.collection("friendsRequests").document(userID).getDocument { (document, error) in
            
            let dict = object.friendUserId
            guard (document?.data()) != nil else {
                print("Document data was empty.")
                self.ref.collection("friendsRequests").document(userID).setData([
                                                                                    "personalUserId" : userID,
                    "peopleList" : FieldValue.arrayUnion([dict])
                ])
                onSuccess(true)
                return
            }
                                    
            //document is already there
            
            self.ref.collection("friendsRequests").document(userID).updateData([
                "peopleList" : FieldValue.arrayUnion([dict])
            ]) { (error) in
                if error == nil {
                    //success
                    onSuccess(true)
                }
            }
        }
    }
    
    func cancelConnectionRequest(user: ProfileModel, onSuccess: @escaping(Bool)->Void) {
        
        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
        let userId = user.userInfoModel.userId
        ref.collection("friendsRequests").document(userId).getDocument { (document, error) in
            guard let data = document?.data() else {
                print("Document data was empty.")
                //not possible scenario in this case
                onSuccess(false)
                return
            }
            
            for requestArray in data {
                if requestArray.key == "peopleList" {
                    //found the object
                    let array = requestArray.value as? [String] ?? [String]()
                    for realObject in array {
                        if realObject == myUserId {
                            //found the object
                            self.ref.collection("friendsRequests").document(userId).updateData(
                                ["peopleList" : FieldValue.arrayRemove([realObject])])
                            onSuccess(true)
                        }
                    }
                }
            }
        }
    }
    
    func removeConnection(user: ProfileModel, onSuccess: @escaping(Bool)->Void){
        
        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
        let userId = user.userInfoModel.userId
        
        ref.collection("friends").document(userId).getDocument { (document, error) in
            guard let data = document?.data() else {
                print("Document data was empty.")
                //not possible scenario in this case
                onSuccess(false)
                return
            }
            
            for requestArray in data {
                if requestArray.key == "peopleList" {
                    //found the object
                    let array = requestArray.value as? [String] ?? [String]()
                    for realObject in array {
                        if realObject == myUserId {
                            //found the object
                            self.ref.collection("friends").document(userId).updateData(
                                ["peopleList" : FieldValue.arrayRemove([realObject])])
                        }
                    }
                }
            }
        }
        
        ref.collection("friends").document(myUserId).getDocument { (document, error) in
            guard let data = document?.data() else {
                print("Document data was empty.")
                //not possible scenario in this case
                onSuccess(false)
                return
            }
            
            for requestArray in data {
                if requestArray.key == "peopleList" {
                    //found the object
                    let array = requestArray.value as? [String] ?? [String]()
                    for realObject in array {
                        if realObject == userId {
                            //found the object
                            self.ref.collection("friends").document(myUserId).updateData(
                                ["peopleList" : FieldValue.arrayRemove([realObject])])
                            onSuccess(true)
                        }
                    }
                }
            }
        }
    }
    
    func addFriend(user: ProfileModel, onSuccess: @escaping(Bool)->Void) {
        
        /*
         remove requests of userid first from my user object
         then add the friend objects in both users
         */
        
        //removing request from myself
        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
        ref.collection("friendsRequests").document(myUserId).getDocument { (document, error) in
            guard let data = document?.data() else {
                print("Document data was empty.")
                //not possible scenario in this case
                onSuccess(false)
                return
            }
            
            for requestArray in data {
                if requestArray.key == "peopleList" {
                    //found the object
                    let array = requestArray.value as? [String] ?? [String]()
                    for realObject in array {
                        if realObject == user.userInfoModel.userId {
                            //found the object
                            self.ref.collection("friendsRequests").document(myUserId).updateData(
                                ["peopleList" : FieldValue.arrayRemove([realObject])])
                        }
                    }
                }
            }
            
            //now add in friends list for both
            //Got objects for both users
            //
            self.ref.collection("friends").document(user.userInfoModel.userId).getDocument { (document, error) in
                
                let dict = myUserId
                if let data = document?.data() {
                    
                    print("Current data: \(data)")
                    //document is already there
                    
                    self.ref.collection("friends").document(user.userInfoModel.userId).updateData([
                        "peopleList" : FieldValue.arrayUnion([dict])
                    ]) { (error) in
                        if error == nil {
                            //success
                            print("success")
                        }
                    }
                }
                else {
                    print("Document data was empty.")
                    self.ref.collection("friends").document(user.userInfoModel.userId).setData([
                                                                                        "personalUserId" : user.userInfoModel.userId,
                        "peopleList" : FieldValue.arrayUnion([dict])
                    ])
                }
            }
            
            self.ref.collection("friends").document(myUserId).getDocument { (document, error) in
                
                let dict = user.userInfoModel.userId
                if let data = document?.data() {
                    
                    print("Current data: \(data)")
                    //document is already there
                    
                    self.ref.collection("friends").document(myUserId).updateData([
                        "peopleList" : FieldValue.arrayUnion([dict])
                    ]) { (error) in
                        if error == nil {
                            //success
                            print("success")
                            onSuccess(true)
                        }
                        else {
                            onSuccess(false)
                        }
                    }
                }
                else {
                    print("Document data was empty.")
                    self.ref.collection("friends").document(myUserId).setData([
                                                                                        "personalUserId" : myUserId,
                        "peopleList" : FieldValue.arrayUnion([dict])
                    ])
                    onSuccess(true)
                }
            }
            
            // also add in NewAddedFriends to notify other person that connection is established
            self.ref.collection("newAddedFriends").document(user.userInfoModel.userId).getDocument { (document, error) in
                
                let dict = myUserId
                if let _ = document?.data() {
                                        
                    //document is already there
                    self.ref.collection("newAddedFriends").document(user.userInfoModel.userId).updateData([
                        "peopleList" : FieldValue.arrayUnion([dict])
                    ]) { (error) in
                        if error == nil {
                            //success
                            print("success")
                        }
                    }
                }
                else {
                    self.ref.collection("newAddedFriends").document(user.userInfoModel.userId).setData([
                                                                                        "personalUserId" : user.userInfoModel.userId,
                        "peopleList" : FieldValue.arrayUnion([dict])
                    ])
                }
            }
        }
    }
    
    func reportUser(model: ReportModel, onSuccess: @escaping(Bool)->Void) {
        
        let dict = ReportModel.toDict(rModel: model)
        ref.collection("reports").addDocument(data: dict) { (error) in
            if error != nil {
                //some issue
                onSuccess(false)
            }
            else {
                //all good
                onSuccess(true)
            }
        }
        
    }
    
    func contactUs(model: ContactUsModel, onSuccess: @escaping(Bool)->Void) {
        
        let dict = ContactUsModel.toDict(rModel: model)
        ref.collection("contactUs").addDocument(data: dict) { (error) in
            if error != nil {
                //some issue
                onSuccess(false)
            }
            else {
                //all good
                onSuccess(true)
            }
        }
        
    }
    
    func addToNearbyInvisibleList(userID: String, onSuccess: @escaping(Bool)->Void) {
        
        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
        var model = NearbyVisibilityModel.init(isNearbyVisible: false, personUserID: userID, powerUserIdToUnhide: myUserId)
        var dict = NearbyVisibilityModel.toDict(dModel: model)
        
        self.addToInvisibleList(type: "usersNearbyPersonWiseVisibility", reportedID: userID, reporterID: myUserId, dict: dict) { (success) in
            
            model = NearbyVisibilityModel.init(isNearbyVisible: false, personUserID: myUserId, powerUserIdToUnhide: myUserId)
            dict = NearbyVisibilityModel.toDict(dModel: model)
            
            self.addToInvisibleList(type: "usersNearbyPersonWiseVisibility", reportedID: myUserId, reporterID: userID, dict: dict) { (success) in
                
                onSuccess(success)
            }
        }
    }
    
    private func addToInvisibleList(type: String, reportedID: String, reporterID: String, dict: [String: Any], onSuccess: @escaping(Bool)->Void) {
        
        self.ref.collection(type).document(reporterID).getDocument { (document, error) in
            
            
            if let _ = document?.data() {

                self.ref.collection(type).document(reporterID).updateData([
                    "peopleList" : FieldValue.arrayUnion([dict])
                ]) { (error) in
                    if error == nil {
                        //success
                        print("success")
                        onSuccess(true)
                    }
                    else {
                        onSuccess(false)
                    }
                }
            }
            else {
                print("Document data was empty.")
                self.ref.collection(type).document(reporterID).setData([
                                                                                    "personalUserId" : reporterID,
                    "peopleList" : FieldValue.arrayUnion([dict])
                ])
                onSuccess(true)
            }
        }
    }
    
    func addToDropbyInvisibleList(userID: String, onSuccess: @escaping(Bool)->Void) {
                        
        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
        
        var model = DropbyVisibilityModel.init(isDropbyVisible: false, personUserID: userID, powerUserIdToUnhide: myUserId)
        var dict = DropbyVisibilityModel.toDict(dModel: model)
        
        self.addToInvisibleList(type: "usersDropByPersonWiseVisibility", reportedID: userID, reporterID: myUserId, dict: dict) { (success) in
            
            model = DropbyVisibilityModel.init(isDropbyVisible: false, personUserID: myUserId, powerUserIdToUnhide: myUserId)
            dict = DropbyVisibilityModel.toDict(dModel: model)
            
            self.addToInvisibleList(type: "usersDropByPersonWiseVisibility", reportedID: myUserId, reporterID: userID, dict: dict) { (success) in
                
                onSuccess(success)
            }
            
        }
        
        
    }
    
    
    func addToBlockList(model: ProfileModel, onSuccess: @escaping(Bool)->Void) {
        
        /*
         1. check if friends then remove from friends
         2. Add to block list for both.
         
         */
        
        //1.
        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
        let friends = ProfileHandler.shared.friends
        var friendFound = false
        for friend in friends {
            if friend == model.userInfoModel.userId {
                //friend found now remove it from both
                friendFound = true
                
                self.unFriend(user: friend, forUser: myUserId) { (success) in
                    if success {
                        //all good
                        //now delete the other friend
                        
                        self.unFriend(user: myUserId, forUser: friend) { (success) in
                            if success {
                                                                
                                var blockModel = BlockListModel.init(isBlocked: true, personUserID: model.userInfoModel.userId, powerUserToUnblock: myUserId)
                                var dict = BlockListModel.toDict(dModel: blockModel)
                                
                                self.addToBlockListNow(blockedID: model.userInfoModel.userId, forUser: myUserId, dict: dict) { (success) in
                                    if success {
                                    
                                        blockModel = BlockListModel.init(isBlocked: true, personUserID: myUserId, powerUserToUnblock: myUserId)
                                        dict = BlockListModel.toDict(dModel: blockModel)
                                        
                                        self.addToBlockListNow(blockedID: myUserId, forUser: model.userInfoModel.userId, dict: dict) { (success) in
                                            onSuccess(success)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break
            }
        }
        if friendFound == false {
            
            var blockModel = BlockListModel.init(isBlocked: true, personUserID: model.userInfoModel.userId, powerUserToUnblock: myUserId)
            var dict = BlockListModel.toDict(dModel: blockModel)
            self.addToBlockListNow(blockedID: model.userInfoModel.userId, forUser: myUserId, dict: dict) { (success) in
                if success {
                    
                    blockModel = BlockListModel.init(isBlocked: true, personUserID: myUserId, powerUserToUnblock: myUserId)
                    dict = BlockListModel.toDict(dModel: blockModel)
                    self.addToBlockListNow(blockedID: myUserId, forUser: model.userInfoModel.userId, dict: dict) { (success) in
                        onSuccess(success)
                    }
                }
            }
        }
    }
    
    func addToBlockListNow(blockedID: String, forUser userId: String, dict: [String: Any], onSuccess: @escaping(Bool)->Void) {
        
        //2. add to block list
        self.ref.collection("userBlockList").document(userId).getDocument { (document, error) in
                   
            
            if let data = document?.data() {
                
                print("Current data: \(data)")
                //document is already there
                
                self.ref.collection("userBlockList").document(userId).updateData([
                    "peopleList" : FieldValue.arrayUnion([dict])
                ]) { (error) in
                    if error == nil {
                        //success
                        print("success")
                        onSuccess(true)
                    }
                    else {
                        onSuccess(false)
                    }
                }
            }
            else {
                print("Document data was empty.")
                self.ref.collection("userBlockList").document(userId).setData([
                                                                                    "personalUserId" : userId,
                    "peopleList" : FieldValue.arrayUnion([dict])
                ])
                onSuccess(true)
            }
        }
    }
    
    func unFriend(user friendID: String, forUser userId: String, onSuccess: @escaping(Bool)->Void) {
                        
        ref.collection("friends").document(userId).getDocument { (document, error) in
            guard let data = document?.data() else {
                print("Document data was empty.")
                //not possible scenario in this case
                onSuccess(false)
                return
            }
            
            for requestArray in data {
                if requestArray.key == "peopleList" {
                    //found the object
                    let array = requestArray.value as? [String]
                    for realObject in array! {
                        if realObject == friendID {
                            //found the object
                            self.ref.collection("friends").document(userId).updateData(
                                ["peopleList" : FieldValue.arrayRemove([realObject])])
                            onSuccess(true)
                            break
                        }
                    }
                }
            }
        }
    }
    
    func setDropByActivity(model: DropByActivityModel, onSuccess: @escaping(Bool)->Void) {
        
        let dict = DropByActivityModel.toDict(dModel: model)
        let myUserID = ProfileHandler.shared.profileModel.userInfoModel.userId
        ref.collection("usersDropByActivity").document(myUserID).setData(dict) { error  in
            
            if error != nil {
                onSuccess(true)
            }
            else {
                onSuccess(false)
            }
        }
    }
    
    func deleteDropbyActivity(onSuccess: @escaping(Bool)->Void) {
        
        let myUserID = ProfileHandler.shared.profileModel.userInfoModel.userId
        ref.collection("usersDropByActivity").document(myUserID).delete { (error) in
            if error != nil {
                onSuccess(true)
            }
            else {
                onSuccess(false)
            }
        }
    }
    
    func fetchAllActivities(onSuccess: @escaping([[String:Any]])->Void, onError: ErrorClosure?) {
                
        ref.collection("usersDropByActivity").getDocuments { (documents, error) in
            if let err = error, let retError = onError {
                retError(err)
            }
            else {
                
                if documents != nil {
                    
                    var arrayOfDict = [[String:Any]]()
                    
                    for obj in (documents?.documents)! {
                        print(obj.data())
                        arrayOfDict.append(obj.data())
                    }
                    onSuccess(arrayOfDict)
                }
            }
        }
    }
    
    func loadAllTables() {
        
        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
        
        self.ref
            .collection("friendsRequests")
            .document(myUserId)
            .setData([
                "personalUserId"    : myUserId,
                "peopleList"        : []
        ])
        
        self.ref
            .collection("usersDropByPersonWiseVisibility")
            .document(myUserId)
            .setData([
                "personalUserId"    : myUserId,
                "peopleList"        : []
        ])
        
        self.ref
            .collection("usersDropByPersonWiseVisibility")
            .document(myUserId)
            .setData([
                "personalUserId"    : myUserId,
                "peopleList"        : []
        ])
        
        self.ref
            .collection("userBlockList")
            .document(myUserId)
            .setData([
                "personalUserId"    : myUserId,
                "peopleList"        : []
        ])
        
        self.ref
            .collection("friends")
            .document(myUserId)
            .setData([
                "personalUserId"    : myUserId,
                "peopleList"        : []
        ])
        
        self.ref
            .collection("newAddedFriends")
            .document(myUserId)
            .setData([
                "personalUserId"    : myUserId,
                "peopleList"        : []
        ])
        
        self.ref
            .collection("notifications")
            .document(myUserId)
            .setData([
                "notificationsList"    : []
        ])
        
        self.ref
            .collection("settings")
            .document(myUserId)
            .setData(SettingsModel.toDict(settingModel: SettingsModel.init()))
    }
    
    func unBlock(user hiddenID: String, forUser userId: String, dict: [String: Any], onSuccess: @escaping(Bool)->Void) {
                        
        ref.collection("userBlockList").document(userId).getDocument { (document, error) in
            guard let data = document?.data() else {
                print("Document data was empty.")
                //not possible scenario in this case
                onSuccess(false)
                return
            }
            
            for requestArray in data {
                if requestArray.key == "peopleList" {
                    //found the object
                    self.ref.collection("userBlockList").document(userId).updateData(
                        ["peopleList" : FieldValue.arrayRemove([dict])])
                    onSuccess(true)
                }
            }
        }
    }
    
    func unHideDropby(user blockedID: String, forUser userId: String, dict: [String: Any], onSuccess: @escaping(Bool)->Void) {
                        
        ref.collection("usersDropByPersonWiseVisibility").document(userId).getDocument { (document, error) in
            guard let data = document?.data() else {
                print("Document data was empty.")
                //not possible scenario in this case
                onSuccess(false)
                return
            }
            
            for requestArray in data {
                if requestArray.key == "peopleList" {
                    //found the object
                    self.ref.collection("usersDropByPersonWiseVisibility").document(userId).updateData(
                        ["peopleList" : FieldValue.arrayRemove([dict])])
                    onSuccess(true)
                }
            }
        }
    }
    
    func getUserSettings(onSuccess: @escaping([String: Any])->Void, onError: ErrorClosure?) {
        
        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
        
        ref.collection("settings")
            .document(myUserId)
            .getDocument { (document, error) in
                
                if let err = error, let retError = onError {
                    retError(err)
                }
                else {
                    
                    if document?.data() == nil {
                        let model = SettingsModel.init()
                        self.updateSettings(setting: model)
                        onSuccess(SettingsModel.toDict(settingModel: model))
                    }
                    else {
                        onSuccess(document!.data()!)
                    }
                    
                }
            }
    }
    
    func updateSettings(setting: SettingsModel) {
        
        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
        ref.collection("settings")
            .document(myUserId)
            .setData(SettingsModel.toDict(settingModel: setting))
    }
    
    func updateLocation(latitude: Double, longitude: Double){
        
        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
        ref.collection("location")
            .document(myUserId)
            .setData(LocationModel.toDict(locModel: LocationModel.init(lat: latitude, long: longitude)))
    }
    
    func addToNotificationList(forUser userID: String, notificationModel model:NotificationsModel,  onSuccess: @escaping(Bool)->Void) {
        
        ref.collection("notifications").document(userID).getDocument { (document, error) in
            
            let dict = NotificationsModel.toDict(model: model)
            guard (document?.data()) != nil else {
                print("Document data was empty.")
                self.ref.collection("notifications").document(userID).setData([
                    "notificationsList" : FieldValue.arrayUnion([dict])
                ])
                onSuccess(true)
                return
            }
                                    
            //document is already there
            
            self.ref.collection("notifications").document(userID).updateData([
                "notificationsList" : FieldValue.arrayUnion([dict])
            ]) { (error) in
                if error == nil {
                    //success
                    onSuccess(true)
                }
            }
        }
    }
}

//MARK:- Realtime update Block
extension AuthenticationFirebase {
    
    func realtimeUpdates() -> Void {
        
        guard let userID = UserDefaults.standard.string(forKey: "UserId") else {print("No userId");return}
                
        //Set realtime update on:
        //TODO:- fire notification
        
        //1. friend request block
        ref.collection("friendsRequests").document(userID).addSnapshotListener { (documentSnapshot, error) in
            
            if let document = documentSnapshot{
                
                if let data = document.data(){
                                        
                    ProfileHandler.shared.friendRequests.removeAll()
                    for obj in data {
                        if obj.key == "peopleList" {
                            let array = obj.value as? [String] ?? [String]()
                            for realObject in array {
                                ProfileHandler.shared.friendRequests.append(realObject)
                            }
                        }
                    }
                }
            }
        }
        
        //2. friends block
        ref.collection("friends").document(userID).addSnapshotListener { (documentSnapshot, error) in
            
            if let document = documentSnapshot {
                
                if let data = document.data(){
                    
                    ProfileHandler.shared.friends.removeAll()
                    for obj in data {
                        if obj.key == "peopleList" {
                            let array = obj.value as? [String] ?? [String]()
                            for realObject in array {
                                ProfileHandler.shared.friends.append(realObject)
                            }
                        }
                    }
                }
            }
        }
        
        //3. newly added friends
        ref.collection("newAddedFriends").document(userID).addSnapshotListener { (documentSnapshot, error) in
            
            if let document = documentSnapshot {
                
                if let data = document.data(){
                                        
                    for obj in data {
                        if obj.key == "peopleList" {
                                
                            self.ref.collection("newAddedFriends").document(userID).updateData(
                                    ["peopleList" : FieldValue.delete()])
                        }
                    }
                }
            }
        }
        
        //4. nearby users
        //5. dropby users on maps
        
        //6. block list
        ref.collection("userBlockList").document(userID).addSnapshotListener { (documentSnapshot, error) in
            
            if let document = documentSnapshot {
                
                if let data = document.data(){
                    
                    ProfileHandler.shared.blockedContacts.removeAll()
                    for obj in data {
                        if obj.key == "peopleList" {
                            let array = obj.value as? [String] ?? [String]()
                            for realObject in array {
                                ProfileHandler.shared.blockedContacts.append(realObject)
                            }
                        }
                    }
                }
            }
        }
        
        //7. dropbyactivity
        ref.collection("usersDropByActivity").document(userID).addSnapshotListener { (documentSnapshot, error) in
            
            ProfileHandler.shared.dropByActivity = nil
            if let document = documentSnapshot {
                
                if let data = document.data(){
                        
                    ProfileHandler.shared.dropByActivity = DropByActivityModel.mapper(snapshot: data) ?? DropByActivityModel()
                }
            }
        }
        
        //8. AppVersion
        ref.collection("appSettings").document("iosVersion").addSnapshotListener { (documentSnapshot, error) in
                        
            if let document = documentSnapshot {
                
                if let data = document.data(){
                    NotificationCenter.default.post(name: versionNotName, object: nil, userInfo: data)
                }
            }
        }
        
        //MARK:- NEAR BY Updates
        
        ref.collection("nearByData")
            .document(userID)
            .collection("nearByUsers")
            .addSnapshotListener { (snapShot, error) in
                
                if let documentSnapshot = snapShot {
                    
                    ProfileHandler.shared.nearByAllUsers.removeAll()
                    
                    let jsonData = documentSnapshot.documents
                    for object in jsonData {
                        let userObject = ProfileModel.publicMapper(snapshot: object.data(), model: ProfileModel()) ?? ProfileModel()
                        ProfileHandler.shared.nearByAllUsers.append(userObject)
                    }
                    if let delegate = ProfileHandler.shared.userListingDelegate {
                        delegate.nearbyUserDidUpdate()
                    }
                }
            }
        
        //MARK:- NOTIFICATIONS
        ref.collection("notifications")
            .document(userID)
            .addSnapshotListener { (documentSnapshot, error) in
            
            ProfileHandler.shared.notifications.removeAll()
            
            if let document = documentSnapshot {
                
                if let data = document.data(){
                    
                    for object in data {
                        if object.key == "notificationsList" {
                            
                            let array = object.value as? [[String:Any]] ?? [[String:Any]]()
                            
                            for element in array {
                                guard let model = NotificationsModel.mapper(snapshot: element) else {continue}
                                ProfileHandler.shared.notifications.append(model)
                            }
                        }
                    }
                }
            }
        }
    }
}
