//
//  BlockList.swift
//  
//
//  Created by Junaid Mukhtar on 21/12/2020.
//

import UIKit

class BlockList: NSObject {

    var personalUserId  = String()
    var blockedContacts = [GenericUserModel]()
        
    init(personalUserId: String, friends: [GenericUserModel]) {
        
        self.personalUserId = personalUserId
        self.friendsList    = friends
    }
}

//MARK:-Mapper
extension BlockList {
    class func mapper(snapshot: [String:Any]) -> BlockList? {

        let uID     = snapshot["personalUserId"] as? String ?? ""
        let friends = snapshot["friendsList"] as? [GenericUserModel] ?? [GenericUserModel]()
        
        return NewAddedFriends.init(personalUserId: uID, friends: friends)
    }
    
    class func toDict(model: BlockList) -> [String: Any] {
        
        var dict = [String: Any]()
        
        dict["personalUserId"]  = model.personalUserId
        dict["friendsList"]     = model.friendsList
        return dict
    }
}
