//
//  FriendRequestModel.swift
//  
//
//  Created by Junaid Mukhtar on 11/12/2020.
//

import UIKit

class FriendRequestModel: NSObject {

    var friendUserId        = String()
    var friendUserName      = String()
    var isIgnored           = Bool()
    var isIgnoredUpdated    = Int()
    var profilePicURL       = String()
    
    init(friendUId: String, friendUName: String , isIgnore: Bool, isIgnoreUpdated: Int, picUrl: String) {
        
        self.friendUserId       = friendUId
        self.friendUserName     = friendUName
        self.isIgnored          = isIgnore
        self.isIgnoredUpdated   = isIgnoreUpdated
        self.profilePicURL      = picUrl
    }
    
    convenience override init(){
        self.init(friendUId: String(),
                  friendUName: String(),
                  isIgnore: Bool(),
                  isIgnoreUpdated: Int(),
                  picUrl: String())
    }
}

//MARK:-Mapper
extension FriendRequestModel {
    class func mapper(snapshot: [String:Any]) -> FriendRequestModel? {

        let uID             = snapshot["friendUserId"] as? String ?? ""
        let uName           = snapshot["friendUserName"] as? String ?? ""
        let iIgnored        = snapshot["isIgnored"] as? Bool ?? false
        let iIgnoredUpdated = snapshot["isIgnoredUpdated"] as? Int ?? -1
        let ppUrl           = snapshot["profilePicURL"] as? String ?? ""
        
        return FriendRequestModel.init(friendUId: uID, friendUName: uName, isIgnore: iIgnored, isIgnoreUpdated: iIgnoredUpdated, picUrl: ppUrl)
         
    }
    class func toDict(model: FriendRequestModel) -> [String: Any] {
        
        var dict = [String: Any]()
        
        dict["friendUserId"]        = model.friendUserId
        dict["friendUserName"]      = model.friendUserName
        dict["isIgnored"]           = model.isIgnored
        dict["isIgnoredUpdated"]    = model.isIgnoredUpdated
        dict["profilePicURL"]       = model.profilePicURL
        
        return dict
    }    
}

class GenericUserModel: NSObject, NSCoding {
//    static var supportsSecureCoding: Bool
    
    func encode(with coder: NSCoder) {
        coder.encode(self.friendUserId, forKey: "friendUserId")
        coder.encode(self.friendUserName, forKey: "friendUserName")
        coder.encode(self.profilePicURL, forKey: "profilePicURL")
    }
    
    required init?(coder: NSCoder) {
        self.friendUserId = coder.decodeObject(forKey: "friendUserId") as? String ?? ""
        self.friendUserName = coder.decodeObject(forKey: "friendUserName") as? String ?? ""
        self.profilePicURL = coder.decodeObject(forKey: "profilePicURL") as? String ?? ""
    }
    

    var friendUserId        = String()
    var friendUserName      = String()
    var profilePicURL       = String()
    
    init(friendUId: String, friendUName: String, picUrl: String) {
        
        self.friendUserId       = friendUId
        self.friendUserName     = friendUName
        self.profilePicURL      = picUrl
    }
    
    convenience override init(){
        self.init(friendUId: String(),
                  friendUName: String(),
                  picUrl: String())
    }
}

//MARK:-Mapper
extension GenericUserModel {
    class func mapper(snapshot: [String:Any]) -> GenericUserModel? {

        let uID             = snapshot["id"] as? String ?? ""
        let uName           = snapshot["name"] as? String ?? ""
        let ppUrl           = snapshot["profilePicUrl"] as? String ?? ""
        
        return GenericUserModel.init(friendUId: uID, friendUName: uName, picUrl: ppUrl)
    }
    
    class func toDict(model: GenericUserModel) -> [String: Any] {
        
        var dict = [String: Any]()
        
        dict["id"]        = model.friendUserId
        dict["name"]      = model.friendUserName
        dict["profilePicUrl"]       = model.profilePicURL
        return dict
    }
}

class NewAddedFriends: NSObject {
    var personalUserId  = String()
    var friendsList     = [String]()
    
    init(personalUserId: String, friends: [String]) {
        
        self.personalUserId = personalUserId
        self.friendsList    = friends
    }
}

//MARK:-Mapper
extension NewAddedFriends {
    class func mapper(snapshot: [String:Any]) -> NewAddedFriends? {

        let uID     = snapshot["personalUserId"] as? String ?? ""
        let friends = snapshot["peopleList"] as? [String] ?? [String]()
        
        return NewAddedFriends.init(personalUserId: uID, friends: friends)
    }
    
    class func toDict(model: NewAddedFriends) -> [String: Any] {
        
        var dict = [String: Any]()
        
        dict["personalUserId"]  = model.personalUserId
        dict["peopleList"]     = model.friendsList
        return dict
    }
}
