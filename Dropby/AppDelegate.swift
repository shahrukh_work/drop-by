//
//  AppDelegate.swift
//  Dropby
//
//  Created by Junaid on 10/11/2020.
//

import UIKit
import Firebase
import GoogleSignIn
import FirebaseAuth
import UserNotifications
import FirebaseMessaging
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces


@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var ref: Firestore?
    
    var window: UIWindow?

    class func sharedInstance() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        // Override point for customization after application launch.
        inilization(application: application)
        //GIDSignIn.sharedInstance.clientID = FirebaseApp.app()?.options.clientID
        
        GMSServices.provideAPIKey("AIzaSyDS5aT45kbELdIKcsbO8THRE57ZvmHW-dY")
        GMSPlacesClient.provideAPIKey("AIzaSyDS5aT45kbELdIKcsbO8THRE57ZvmHW-dY")
        
        Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        ref = Firestore.firestore()
//        addNotificationObservers()
        
        AppHandler.shared.decideAppState()
        return true
    }
    @objc func runTimedCode(){
        SocketHelper.shared.connectSocket { (success) in
            if success{
                SocketHelper.shared.emit(params: ProfileHandler.shared.profileModel.userInfoModel.userId)
            }
        }
    }
    func addNotificationObservers() {
        
            NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willTerminateNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(appBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        }
        @objc func appMovedToBackground() {
            let userID = ProfileHandler.shared.profileModel.userInfoModel.userId
            guard ref != nil else {
                return
            }
            ref!.collection("usersPublic").document(userID).updateData(["onlineModel.IsOnline": false])
            UserDefaults.standard.setValue(false, forKey: "isOnline")
            NotificationCenter.default.post(name: Notification.Name("isOnline"), object: nil)
            checkingForOnline()
        }
        @objc func appBecomeActive() {
            let userID = ProfileHandler.shared.profileModel.userInfoModel.userId
            guard ref != nil else {
                return
            }
            
            ref!.collection("usersPublic").document(userID).updateData(["onlineModel.IsOnline": true])
            UserDefaults.standard.setValue(true, forKey: "isOnline")
            NotificationCenter.default.post(name: Notification.Name("isOnline"), object: nil)
            
            
            checkingForOnline()

        }
    func checkingForOnline(){
        if UserDefaults.standard.bool(forKey: "isOnline") {
            print("true")
        }
        else{
            print("false")
        }
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func application(_ application: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
      if Auth.auth().canHandle(url) {
        return true
      }
        return false
      // URL not auth related, developer should handle it.
    }

}
extension AppDelegate : Navigator{
    func inilization(application: UIApplication){
        

        // 2
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(MessageDetailVC.self)
        //4 notifacrion
        FirebaseApp.configure()
        settingPushNotification(application: application)
    }
    
    private func settingPushNotification(application: UIApplication){
        
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        
        Messaging.messaging().delegate = self
        
        //Solicit permission from the user to receive notifications
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, error) in
            guard error == nil else{
                print(error!.localizedDescription)
                return
            }
        }
        
        //get application instance ID
//        InstanceID.instanceID().instanceID { (result, error) in
//            if let error = error {
//                print("Error fetching remote instance ID: \(error)")
//            } else if let result = result {
//                print("Remote instance ID token: \(result.token)")
//            }
//        }
//
//
        
        application.registerForRemoteNotifications()
    }
}
@available(iOS 10, *)
// MARK:- PushNotifications Deleget
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //completionHandler(.alert)
        
        
        let userInfo = notification.request.content.userInfo
        if UIApplication.shared.applicationState != .active {
//            handelNotificationRequest(userInfo : userInfo)
        }
//        print(userInfo)
        
        completionHandler([.sound , .alert , .badge])
        
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        
//        if Auth.auth().canHandleNotification(notification) {
//           completionHandler(.noData)
//           return
//         }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
            
            let notificationData = response.notification.request.content.userInfo
//            guard let data =  notificationData["data"] as? [String:String] else {
//            return
//                }
//        let chatId = data["chatId"]
//        let id = data["id"]
//        let name = data["name"]
//        let profilePic = data["profilePicUrl"]
            NotificationCenter.default.post(name: Notification.Name("message"), object: nil,userInfo: notificationData)
//            let storyboard = UIStoryboard(name: "Message", bundle: nil)
//            // instantiate the view controller we want to show from storyboard
//            // root view controller is tab bar controller
//            // the selected tab is a navigation controller
//            // then we push the new view controller to it
//            if  let conversationVC = storyboard.instantiateViewController(withIdentifier: "MessagesVC") as? MessagesVC,
//                let tabBarController = self.window?.rootViewController as? UITabBarController,
//                let navController = tabBarController.selectedViewController as? UINavigationController {
//                    // we can modify variable of the new view controller using notification data
//                    // (eg: title of notification)
//                    //conversationVC.senderDisplayName = response.notification.request.content.title
//                    // you can access custom data of the push notification by using userInfo property
//                    // response.notification.request.content.userInfo
//                    navController.pushViewController(conversationVC, animated: true)
//            }
            
            completionHandler()
            
        }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print("Failed to register for remote notifications with error: \(error)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      // Pass device token to auth
      Auth.auth().setAPNSToken(deviceToken, type: .unknown)

      // Further handling of the device token if needed by the app
      // ...
    }
}
extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(String(describing: fcmToken))")
        UserDefaults.standard.set(fcmToken, forKey: "fcmToken")
        
    }

    
}
