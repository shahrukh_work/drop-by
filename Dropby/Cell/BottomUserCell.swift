//
//  BottomUserCell.swift
//  Dropby
//
//  Created by Junaid on 30/11/2020.
//

import UIKit
import MBProgressHUD
import CoreLocation

protocol DropbyBottomCellProtocol {
    func messageUser(user: ProfileModel)
    func hideUser(user: ProfileModel)
}

class BottomUserCell: UITableViewCell {

    @IBOutlet weak var iconActivity: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var invisibleButton: UIButton!
    @IBOutlet weak var messageButton: UIButton!
    var delegage: DropbyBottomCellProtocol!
    
    var profileModel : ProfileModel!{
        didSet{
            
            let name = (profileModel.userInfoModel.firstName ) + " " + (profileModel.userInfoModel.lastName)
            lblName.text = name
            iconImageView.sd_setImage(with: URL(string: profileModel.userInfoModel.profilePicURL ), placeholderImage: UIImage(named: "avatar_1"))
            iconActivity.image = UIImage(named: profileModel.dropByActivity)
            let distance = LocationHandler.shared.calculateDistance(to: CLLocation(latitude: profileModel.geoLocationModel.coordinates.latitude, longitude: profileModel.geoLocationModel.coordinates.longitude))
            lblDistance.text = String(format: "%.2f", (distance/1000 * 0.621)) + " miles"
            lblDescription.text = profileModel.dropByActivityModel.notes
        }
    }
    @IBAction func markUserInvisible(_ sender: Any) {
        
        if delegage != nil {
            delegage.hideUser(user: profileModel)
        }
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        
        if delegage != nil {
            delegage.messageUser(user: profileModel)
        }
    }
}
