//
//  HomeCell.swift
//  Dropby
//
//  Created by Junaid on 19/11/2020.
//

import UIKit
import TagListView

class HomeCell: UITableViewCell {

    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tagView: TagListView!
    @IBOutlet weak var universityLabel: UILabel!
    @IBOutlet weak var basicInfoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var profileModel : ProfileModel!{

        didSet{
            tagView.removeAllTags()
            let name = (profileModel.userInfoModel.firstName ) + " " + (profileModel.userInfoModel.lastName)
            nameLabel.text = name
            profilePicture.sd_setImage(with: URL(string: profileModel.userInfoModel.profilePicURL ), placeholderImage: UIImage(named: "avatar_1"), options: .refreshCached, context: nil)
            let tags = profileModel.tagsModel.tagsList
            if tags.count > 4 {
                tagView.addTags(Array(tags[0..<4]))
            }
            else {
                tagView.addTags(tags)
            }
            
            basicInfoLabel.text = profileModel.userInfoModel.basicInfo
            universityLabel.text = profileModel.userInfoModel.instituteName
            
        }
    }

}
