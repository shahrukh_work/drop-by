//
//  ContactCell.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 06/01/2021.
//

import UIKit
import Contacts

class ContactCell: UITableViewCell {

    @IBOutlet weak var lblContactName: UILabel!
    @IBOutlet weak var contactStatusView: UIView!
    @IBOutlet weak var imgContact: UIImageView!
    @IBOutlet weak var onlineStatus: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        done
        NotificationCenter.default.addObserver(self, selector: #selector(self.onlineView(notifications:)), name: Notification.Name("isOnline"), object: nil)

    }
    @objc func onlineView(notifications : Notification){
        if UserDefaults.standard.bool(forKey: "isOnline"){
            self.onlineStatus.backgroundColor = .green
        }
        else{
            self.onlineStatus.backgroundColor = .gray
        }
    }
    
    var contact : GenericUserModel!{
        didSet{
            lblContactName.text = contact.friendUserName
            imgContact.sd_setImage(with: URL(string: contact.profilePicURL ), placeholderImage: UIImage(named: "avatar_1"))
        }
    }
    
}
