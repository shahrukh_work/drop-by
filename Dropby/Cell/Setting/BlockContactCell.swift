//
//  BlockContactCell.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 2/7/21.
//

import UIKit
import MBProgressHUD

class BlockContactCell: UITableViewCell {

    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var unblockBtn: UIButton!
    var callback: (()->Void)?
    var profileModel : GenericUserModel!{
        didSet{
                        
            userNameLbl.text = profileModel.friendUserName
            userImgView.sd_setImage(with: URL(string: profileModel.profilePicURL ), placeholderImage: UIImage(named: "avatar_1"))            
        }
    }
    
    @IBAction func didTapUnBlock(_ sender: Any) {
        
        let vc = GenericPopup.instantiate(fromAppStoryboard: .PopUp)
        vc.modalPresentationStyle = .overFullScreen
        vc.titleText = "Unblock User"
        vc.descriptionText = "Do you want to unblock "+self.profileModel.friendUserName+"?"
        vc.cancelButtonText = "CANCEL"
        vc.okButtonText = "YES"
        
        if self.parentContainerViewController() != nil {
            self.parentContainerViewController()!.present(vc, animated: true) {
                                
                vc.btnActionCallback = { action in
                    
                    if action == .ok {
                        MBProgressHUD.showAdded(to: self.parentContainerViewController()!.view, animated: true)
                        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
                        let personUserId = self.profileModel!.friendUserId
                        
                        var model = BlockListModel.init(isBlocked: true, personUserID: personUserId, powerUserToUnblock: myUserId)
                        var dict = BlockListModel.toDict(dModel: model)
                        
                        ProfileHandler.shared.authInteractor.unBlock(user: personUserId, forUser: myUserId, dict: dict) { (success) in
                            
                            model = BlockListModel.init(isBlocked: true, personUserID: myUserId, powerUserToUnblock: personUserId)
                            dict = BlockListModel.toDict(dModel: model)
                            
                            ProfileHandler.shared.authInteractor.unBlock(user: myUserId, forUser: personUserId, dict: dict) { (success) in
                                
                                MBProgressHUD.hide(for: self.parentContainerViewController()!.view, animated: true)
                                if let call = self.callback {
                                    call()
                                }
                            }
                        }
                    }
                }
                UIView.animate(withDuration: 0.1) {
                    
                    vc.backgroundView.alpha = 0.5
                }
            }
        }
    }    
}
