//
//  LocationVisibilityCell.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 2/7/21.
//

import UIKit
import MBProgressHUD

class LocationVisibilityCell: UITableViewCell {

    @IBOutlet weak var makeVisibleBtn: UIButton!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    var callback: (()->Void)?
    var profileModel : GenericUserModel!{
        didSet{
                        
            userNameLbl.text = profileModel.friendUserName
            userImgView.sd_setImage(with: URL(string: profileModel.profilePicURL ), placeholderImage: UIImage(named: "avatar_1"))
        }
    }
    
    @IBAction func makeVisibleTapped(_ sender: Any) {
        
        let vc = GenericPopup.instantiate(fromAppStoryboard: .PopUp)
        vc.modalPresentationStyle = .overFullScreen
        vc.titleText = "Unhide User"
        vc.descriptionText = "Do you want to unhide "+self.profileModel.friendUserName+"?"
        vc.cancelButtonText = "CANCEL"
        vc.okButtonText = "YES"
        
        if self.parentContainerViewController() != nil {
            self.parentContainerViewController()!.present(vc, animated: true) {
                                
                vc.btnActionCallback = { action in
                    
                    if action == .ok {
                        MBProgressHUD.showAdded(to: self.parentContainerViewController()!.view, animated: true)
                        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
                        let personUserId = self.profileModel!.friendUserId
                        
                        var model = DropbyVisibilityModel.init(isDropbyVisible: false, personUserID: personUserId, powerUserIdToUnhide: myUserId)
                        var dict = DropbyVisibilityModel.toDict(dModel: model)
                        
                        ProfileHandler.shared.authInteractor.unHideDropby(user: personUserId, forUser: myUserId, dict: dict) { (success) in
                            
                            model = DropbyVisibilityModel.init(isDropbyVisible: false, personUserID: myUserId, powerUserIdToUnhide: myUserId)
                            dict = DropbyVisibilityModel.toDict(dModel: model)
                            
                            ProfileHandler.shared.authInteractor.unHideDropby(user: myUserId, forUser: personUserId, dict: dict) { (success) in
                                
                                MBProgressHUD.hide(for: self.parentContainerViewController()!.view, animated: true)
                                if let call = self.callback {
                                    call()
                                }
                            }
                        }
                    }
                }
                UIView.animate(withDuration: 0.1) {
                    
                    vc.backgroundView.alpha = 0.5
                }
            }
        }
    }
}
