//
//  NotificationCell.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 2/7/21.
//

import UIKit

class NotificationSettingCell: UITableViewCell {

    var callback: ((Bool)->Void)?
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var switchBtn: UISwitch!
    
    @IBAction func valueChanged(_ sender: Any) {
        
        if let button = sender as? UISwitch {
                        
            callback!(button.isOn)
        }
        
    }
}
