//
//  ShareLocationCell.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 2/7/21.
//

import UIKit
import MBProgressHUD

class ShareLocationCell: UITableViewCell {

    @IBOutlet weak var btnSelection: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    var visibilityStatus = VisibilityStatus.Everyone
    
    
    @IBAction func didSelectVisibility(_ sender: Any) {
        
        if let controller = self.parentContainerViewController() as? LocationSettingVC {
            let impactMed = UIImpactFeedbackGenerator(style: .light)
            impactMed.impactOccurred()
            
            MBProgressHUD.showAdded(to: controller.view, animated: true)
            controller.selectedVisibility = visibilityStatus.rawValue
            ProfileHandler.shared.setVisibility(visibility: controller.selectedVisibility) { (success) in
                MBProgressHUD.hide(for: controller.view, animated: true)
                controller.tableview.reloadData()
            }
        }
    }
    
}
