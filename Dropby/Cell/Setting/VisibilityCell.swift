//
//  VisibilityCell.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 2/18/21.
//

import UIKit

class VisibilityCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
}
