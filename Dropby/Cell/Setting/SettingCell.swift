//
//  SettingCell.swift
//  Dropby
//
//  Created by Junaid on 08/12/2020.
//

import UIKit

class SettingCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var forwardArrowImageView: UIImageView!
    
    
    var model : SettingModel!{
        didSet{
            icon.image = model.icon
            titleLbl.text = model.name
        }
    }
    
}
