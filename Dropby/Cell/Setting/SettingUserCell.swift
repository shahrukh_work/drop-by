//
//  ProfileHeaderCell.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 04/01/2021.
//

import UIKit

class SettingUserCell: UITableViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    
    var user : ProfileModel? {
        didSet{
            
            let name = (user?.userInfoModel.firstName
                ?? "")
            lblName.text = name
            lblDescription.text = user?.userInfoModel.instituteName ?? ""               
            imgUser.sd_setImage(with: URL(string: user!.userInfoModel.profilePicURL), placeholderImage: nil, options: .refreshCached, context: nil)
        }
    }
}

