//
//  PinnedConatctCell.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 08/01/2021.
//

import UIKit

class PinnedConatctCell: UICollectionViewCell {
    
    @IBOutlet weak var imgContact: UIImageView!
    @IBOutlet weak var onlineStatusView: UIView!
    @IBOutlet weak var lblName: UILabel!
    var userID: String? {
        didSet {
            ProfileHandler.shared.authInteractor.fetchUser(withUserId: userID!) { (profile) in
                if profile != nil {
                    self.pinnedUser = profile
                    self.loadView()
                }
            }
        }
    }
    var pinnedUser: ProfileModel?
    var wifiReachability:Reachability?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imgContact.image = #imageLiteral(resourceName: "add_contact")
        self.lblName.text = ""
    }
    func loadView() {
        self.lblName.text = self.pinnedUser!.userInfoModel.firstName
        self.imgContact.sd_setImage(with: URL(string: self.pinnedUser!.userInfoModel.profilePicURL)) { (imaage, error, cacheType, url) in
        }
        startMonitoringReachability()
    }
    func startMonitoringReachability()
        {
            NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
            if ((wifiReachability == nil)) {
                wifiReachability = Reachability.forInternetConnection()
            }
            wifiReachability?.startNotifier()
            reachabilityChanged()
        }
        @objc func reachabilityChanged()
        {
            if wifiReachability?.currentReachabilityStatus() == .NotReachable {
                self.onlineStatusView.backgroundColor = .gray
            }
            else{
                self.onlineStatusView.backgroundColor = .green
            }
        }
}
