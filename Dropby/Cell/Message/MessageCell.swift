//
//  MessageCell.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 08/01/2021.
//

import UIKit
import FirebaseFirestore

class MessageCell: UITableViewCell {

    @IBOutlet weak var unreadCountLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var onlineStatusView: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    var wifiReachability:Reachability?
    var ref: Firestore?
    var messageObject: MessageListModel? {
        didSet {
            loadMessagePreview()
        }
    }
    var userID: String? {
        didSet {
            if userID != nil {
                ProfileHandler.shared.authInteractor.fetchUser(withUserId: userID!) { (profile) in
                    if profile != nil {
                        self.chatUser = profile
                        self.loadView()
                    }
                }
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        ref = Firestore.firestore()

    }
    var chatUser: ProfileModel?
    
    func loadView() {
        if self.chatUser == nil {
            return
        }
        startMonitoringReachability()
        self.lblName.text = self.chatUser!.userInfoModel.firstName+" "+self.chatUser!.userInfoModel.lastName
        self.imgUser.sd_setImage(with: URL(string: self.chatUser!.userInfoModel.profilePicURL ), placeholderImage: UIImage(named: "avatar_1"), options: .refreshCached, context: nil)
        let date = Timestamp.dateValue(self.messageObject!.createdAt)()
        let time1 = DateFormatter.localizedString(from: date, dateStyle: .none, timeStyle: .short)
        self.timeLbl.text = time1
    }
    func startMonitoringReachability()
        {
//        NotificationCenter.default.addObserver(self, selector: #selector(self.onlineView(notifications:)), name: Notification.Name("isOnline"), object: nil)

    
            NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
            if ((wifiReachability == nil)) {
                wifiReachability = Reachability.forInternetConnection()
            }
            wifiReachability?.startNotifier()
            reachabilityChanged()
        }
        @objc func reachabilityChanged()
        {
            if wifiReachability?.currentReachabilityStatus() == .NotReachable {
                self.onlineStatusView.backgroundColor = .gray
            }
            else{
                self.onlineStatusView.backgroundColor = .green
            }
        }
//    @objc func onlineView(notifications : Notification){
//        if UserDefaults.standard.bool(forKey: "isOnline"){
//            self.onlineStatusView.backgroundColor = .green
//        }
//        else{
//            self.onlineStatusView.backgroundColor = .gray
//        }
//    }

    func loadMessagePreview() {
        
        let myUserId = ProfileHandler.shared.profileModel.userInfoModel.userId
        
        if self.messageObject!.participants.first != myUserId {
            self.userID = self.messageObject!.participants.first
        }
        else {
            self.userID = self.messageObject!.participants.last
        }
        let lastMessage = self.messageObject!.lastMessage
        lblDetails.text = lastMessage.body
        if let participentId = messageObject?.participants[1]{
            
            ref!.collection("usersPublic").document(participentId).addSnapshotListener { doc, err in
                guard let document = doc else {
                        print("Error fetching document: \(err!)")
                        return
                      }
                      guard let data = document.data() else {
                        
                        return
                      }
                guard let value = data["onlineModel"] as? [String:Any] else{
                    return
                }
                let receivedOnlineStatus = value["IsOnline"] as? Int
                if receivedOnlineStatus == 1{
                    self.onlineStatusView.backgroundColor = .green
                    print(receivedOnlineStatus ?? 0)
                }
                else{
                    if receivedOnlineStatus == 1{
                        self.onlineStatusView.backgroundColor = .green
                        print(receivedOnlineStatus ?? 0)
                    }
                    else{
                        self.onlineStatusView.backgroundColor = .gray
                        print(receivedOnlineStatus ?? 0)
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
    


