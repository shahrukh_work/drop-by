//
//  NotificationCell.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 1/25/21.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var notificationImageView: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
}
