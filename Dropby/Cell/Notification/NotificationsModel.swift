//
//  NotificationsModel.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 05/03/2021.
//

import UIKit
import FirebaseFirestore

class NotificationsModel: NSObject {
    var body            = String()
    var time            = Timestamp.init()
    var basicInfoModel  = GenericUserModel()
    
    init(body: String, time: Timestamp, model: GenericUserModel) {
        
        self.body           = body
        self.time           = time
        self.basicInfoModel = model
    }
    
    convenience override init(){
        self.init(body: String(), time: Timestamp.init(), model: GenericUserModel())
    }
}

extension NotificationsModel {
    class func mapper(snapshot: [String:Any]) -> NotificationsModel? {
                        
        let model   =   GenericUserModel.mapper(snapshot: snapshot["basicInfoModel"] as! [String : Any]) ?? GenericUserModel()
        let time    =   snapshot["time"] as? Timestamp ?? Timestamp.init()
        let body    =   snapshot["body"] as? String ?? ""
        return NotificationsModel.init(body: body, time: time, model: model)
    }
    
    class func toDict(model: NotificationsModel) -> [String: Any] {
        
        var dict = [String: Any]()
        
        dict["basicInfoModel"]        = GenericUserModel.toDict(model: model.basicInfoModel)
        dict["time"]        = model.time
        dict["body"]        = model.body
        return dict
    }
}

