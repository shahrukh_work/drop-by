//
//  ProfileLabelsCell.swift
//  Dropby
//
//  Created by Junaid on 01/12/2020.
//

import UIKit
import TagListView

class ProfileLabelsCell: UITableViewCell {

     
    @IBOutlet weak var tagsList: TagListView!
    
}
