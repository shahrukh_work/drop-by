//
//  HeaderCell.swift
//  Dropby
//
//  Created by Junaid on 01/12/2020.
//

import UIKit
import MBProgressHUD

protocol PresesntVCCellProtocol {
    func presentDidSelect(forUser user: ProfileModel)
    func messageUser(user: ProfileModel)
}

class ProfileHeaderCell: UITableViewCell {
    
    @IBOutlet weak var ImgUserIcon: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblDetail1: UILabel!
     
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var btnAddFriend: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    var delegage: PresesntVCCellProtocol!
    
    var user : ProfileModel? {
        didSet{
            
            let name = (user?.userInfoModel.firstName
                ?? "") + " " + (user?.userInfoModel.lastName ?? "")
            lblUserName.text = name
            lblDetail1.text = user!.userInfoModel.instituteName
            
            ImgUserIcon.sd_setImage(with: URL(string: user!.userInfoModel.profilePicURL)) { (image, error, SDImageCacheTypeDisk, url) in
                 
            }
            updateButtonsStatus()
        }
    }
    
    func updateButtonsStatus() {
        //check if he is friend or not
        MBProgressHUD.showAdded(to: self, animated: true)
        ProfileHandler.shared.checkUserFriendStatus(userId: (user?.userInfoModel.userId)!) { (status) in
            
            MBProgressHUD.hide(for: self, animated: true)
            if status != nil {
                if status == .notFriends{
                    
                    self.btnMessage.isHidden = true
                    self.btnAddFriend.isHidden = false
                    self.btnAddFriend.isEnabled = true
                    self.btnAddFriend.isSelected = false
                }
                else if status == .friends {
                    self.btnMessage.isHidden = false
                    self.btnAddFriend.isHidden = true
                    self.btnAddFriend.isEnabled = true
                    self.btnAddFriend.isSelected = false
                }
                else if status == .friendRequested {
                    //change to cancel
                    self.btnMessage.isHidden = true
                    self.btnAddFriend.isHidden = false
                    self.btnAddFriend.isEnabled = false
                    self.btnAddFriend.isSelected = true
                }
            }
        }
    }
    
    @IBAction func actionTapped(_ sender: Any) {
        
        if let usr = user {
            if let btn = sender as? UIButton {
                if btn.tag == 1 {
                    //send message
                    var isBlocked = false
                    for blockedContact in ProfileHandler.shared.blockedContacts {
                        if blockedContact == usr.userInfoModel.userId {
                            
                            //cannot add
                            self.delegage.presentDidSelect(forUser: usr)
                            isBlocked = true
                            break
                        }
                    }
                    if !isBlocked {
                        self.delegage.messageUser(user: usr)
                    }
                    else {
                        
                    }
                }
                else if btn.tag == 2 {
                    //send friend request
                    var isBlocked = false
                    for blockedContact in ProfileHandler.shared.blockedContacts {
                        if blockedContact == usr.userInfoModel.userId {
                            
                            //cannot add
                            self.delegage.presentDidSelect(forUser: usr)
                            isBlocked = true
                            break
                        }
                    }
                    if isBlocked == false {
                        
                        MBProgressHUD.showAdded(to: self, animated: true)
                        ProfileHandler.shared.checkUserFriendStatus(userId: user!.userInfoModel.userId) { (status) in
                            MBProgressHUD.hide(for: self, animated: true)
                            if status != nil {
                                
                                if status == .friends {
                                    //cancel request
                                }
                                else if status == .notFriends {
                                    //add friend
                                    ProfileHandler.shared.sendFriendRequest(user: usr) { (success) in
                                        self.updateButtonsStatus()
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    //more menu
                    let userModel = self.user!
                    let vc = MoreActionSheet.instantiate(fromAppStoryboard: .PopUp)
                    vc.userModel = userModel
                    vc.callBack = { type in
                        
                        DispatchQueue.main.async {
                            vc.closeMenu()
                        }
                        if self.parentContainerViewController() != nil {
                            
                            if type == .report {
                                
                                let storyBoard = UIStoryboard(name: "Feedback", bundle: nil)
                                guard let reportVC = storyBoard.instantiateViewController(identifier: "ReportVC") as? ReportVC else{return}
                                reportVC.againstUserId = userModel.userInfoModel.userId
                                if self.parentContainerViewController() != nil {
                                    self.parentContainerViewController()!.show(reportVC, sender: self)
                                }
                            }
                            else if type == .block {
                                
                                MBProgressHUD.showAdded(to: self.parentContainerViewController()!.view, animated: true)
                                //first check with API if iam blocked?
                                
                                ProfileHandler.shared.authInteractor.addToBlockList(model: userModel) { (success) in
                                    
                                    DispatchQueue.main.async {
                                        
                                        MBProgressHUD.hide(for: self.parentContainerViewController()!.view, animated: true)
                                        //Call popup
                                        self.afterSteps(title: "Contact Blocked", text: " was successfully blocked. You can no longer interact with this user", userName: userModel.userInfoModel.firstName, viewController: self.parentContainerViewController()!)
                                    }
                                }
                            }
                            else if type == .invisible {
                                
                                MBProgressHUD.showAdded(to: self.parentContainerViewController()!.view, animated: true)
                                
                                ProfileHandler.shared.authInteractor.addToNearbyInvisibleList(userID: userModel.userInfoModel.userId) { (success) in
                                    DispatchQueue.main.async {
                                        
                                        MBProgressHUD.hide(for: self.parentContainerViewController()!.view, animated: true)
                                        self.afterSteps(title: "Invisible", text: " was successfully marked invisible. You can no longer interact with this user", userName: userModel.userInfoModel.firstName, viewController: self.parentContainerViewController()!)
                                    }
                                }
                            }
                            else if type == .friendUnFriend {
                                self.updateButtonsStatus()
                            }
                        }
                    }
                    vc.modalPresentationStyle = .overFullScreen
                    self.parentContainerViewController()!.present(vc, animated: true) {
                        UIView.animate(withDuration: 0.1) {
                            vc.backgroundView.alpha = 0.5
                        }
                    }
                }
            }
        }
    }
    
    func afterSteps(title: String, text: String, userName: String, viewController: UIViewController) {
                
        let vc = GenericPopup.instantiate(fromAppStoryboard: .PopUp)
        vc.modalPresentationStyle = .overFullScreen
        vc.titleText = title
        vc.descriptionText = userName+text
        vc.shouldHideOk = true
        vc.cancelButtonText = "OK"
        
        viewController.present(vc, animated: true) {
                        
            vc.btnActionCallback = { action in
                
                DispatchQueue.main.async {                    
                    viewController.navigationController?.popViewController(animated: true)
                }
                
            }
            UIView.animate(withDuration: 0.1) {
                
                vc.backgroundView.alpha = 0.5
            }
        }
    }
    
}
