//
//  ProfileNetworkCell.swift
//  Dropby
//
//  Created by Junaid on 01/12/2020.
//

import UIKit
import MBProgressHUD

class ProfileNetworkCell: UITableViewCell {

    @IBOutlet weak var viewAllBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var commonFriends = [GenericUserModel]()
    var userId = String()
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }
    
    func fetchMutualContacts() {
        MBProgressHUD.showAdded(to: self, animated: true)
        ProfileHandler.shared.getCommonFriendsWith(userID: userId) { (common) in
            
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self, animated: true)
                
                if common != nil {
                    
                    self.commonFriends = common!
                    if self.commonFriends.count < 5 {
                        self.viewAllBtn.isHidden = true
                    }
                    self.collectionView.reloadData()
                }
            }
        }
    }
}

extension ProfileNetworkCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return commonFriends.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell : DropByActivityCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DropByActivityCell", for: indexPath) as? DropByActivityCell else{return UICollectionViewCell()}
        let friend = commonFriends[indexPath.item]
        cell.iconImageView.sd_setImage(with: URL(string: friend.profilePicURL)) { (image, error, type, url) in
            
        }
        cell.nameLbl.text = friend.friendUserName
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = collectionView.bounds.size.height
        return CGSize(width: height, height: height)
    }
    
}
