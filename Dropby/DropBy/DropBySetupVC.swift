//
//  DropBySetupVC.swift
//  Dropby
//
//  Created by Junaid on 09/12/2020.
//

import UIKit
import FirebaseFirestore
import MBProgressHUD

class DropBySetupVC: UIViewController {
    
    //SetupViews
    @IBOutlet weak var setupStView: UIStackView!
    @IBOutlet weak var currentTimeLbl: UILabel!
    @IBOutlet weak var endTimeLbl: UILabel!
    @IBOutlet weak var noteTxtView: UITextView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var slider: UISlider!
    var startDate: Date?
    var endDate: Date?
    var timer = Timer()
    var lastSliderValue: Int = 0
    ///Setup Data
    
    let activitesNameArray = [DropByActivity.coffee.rawValue , DropByActivity.cycling.rawValue , DropByActivity.running.rawValue , DropByActivity.study.rawValue, DropByActivity.food.rawValue, DropByActivity.drinks.rawValue, DropByActivity.movie.rawValue, DropByActivity.swimming.rawValue, DropByActivity.exercise.rawValue, DropByActivity.chill.rawValue]
    
    var selectedActivity = DropByActivity.coffee.rawValue
    
    var showFeedBackCallback: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.reloadData()
        
        self.slider.setThumbImage(UIImage(named: "scale_pivot"), for: .normal)
        
        
        
        if let activity = ProfileHandler.shared.dropByActivity {
            //if time passed then remove activity
            let endTime = Date.init(timeInterval: 0, since: Timestamp.dateValue(activity.endTime)())
            MBProgressHUD.showAdded(to: self.view, animated: true)
            if endTime > Date(){
                
                let _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.navigateToActivity) , userInfo: nil, repeats: false)
            }
            else {
                //expire acivity
                ProfileHandler.shared.authInteractor.deleteDropbyActivity { (success) in
                    self.startTimer()
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }
        }
        else {
            startTimer()
        }
    }
    
    func startTimer() -> Void {
        startDate = Date()
        endDate = Date()
        endDate = endDate?.addingTimeInterval(TimeInterval(3600))
        currentTimeLbl.text = DateFormatter.localizedString(from: startDate!, dateStyle: .none, timeStyle: .short)
        endTimeLbl.text = DateFormatter.localizedString(from: endDate!, dateStyle: .none, timeStyle: .short)
        timer = Timer.scheduledTimer(timeInterval: 15.0, target: self, selector:#selector(self.tick) , userInfo: nil, repeats: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
                
        if let activity = ProfileHandler.shared.dropByActivity {
            //if time passed then remove activity
            if let dropbyVC = self.parent as? DropByVC {
                if dropbyVC.isEditMode {
                                        
                    let endTime = Date.init(timeInterval: 0, since: Timestamp.dateValue(activity.endTime)())
                    startDate = Date()
                    endDate = endTime
                    currentTimeLbl.text = DateFormatter.localizedString(from: startDate!, dateStyle: .none, timeStyle: .short)
                    endTimeLbl.text = DateFormatter.localizedString(from: endDate!, dateStyle: .none, timeStyle: .short)
                    selectedActivity = activity.activity
                    noteTxtView.text = activity.notes
                    collectionView.reloadData()
                }
                else {
                    let endTime = Date.init(timeInterval: 0, since: Timestamp.dateValue(activity.endTime)())
                    
                    if endTime > Date(){
                        
                        self.navigateToActivity()
                        timer.invalidate()
                    }
                    else {
                        //expire acivity
                        MBProgressHUD.showAdded(to: self.view, animated: true)
                        ProfileHandler.shared.authInteractor.deleteDropbyActivity { (success) in
                            self.startTimer()
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                    }
                }
            }
        }
        else {
            startTimer()
        }
    }
    
    @objc func navigateToActivity() {
        MBProgressHUD.hide(for: self.view, animated: true)
        self.showFeedBackCallback?()
    }
    
    @IBAction func saveSetup(_ sender: Any) {
        if startDate == nil {
            startTimer()
            return
        }
        //save activity on firebase and then change the screen
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let activityModel = DropByActivityModel.init(notes: noteTxtView.text, activity: selectedActivity, startTime: Timestamp.init(date: startDate!), endTime: Timestamp.init(date: endDate!), userId: ProfileHandler.shared.profileModel.userInfoModel.userId)
        ProfileHandler.shared.authInteractor.setDropByActivity(model: activityModel) { (success) in
            MBProgressHUD.hide(for: self.view, animated: true)
            self.showFeedBackCallback?()
            NotificationCenter.default.post(name: startActivity, object: nil)
        }
    }
    
    @objc func tick() {
        startDate = Date()
        currentTimeLbl.text = DateFormatter.localizedString(from: Date(), dateStyle: .none, timeStyle: .short)
        endDate = endDate?.addingTimeInterval(15.0)
        endTimeLbl.text = DateFormatter.localizedString(from: endDate!, dateStyle: .none, timeStyle: .short)
    }
//    @IBAction func sliderValueDidChange(_ sender: Any) {
//        if startDate == nil {
//            startTimer()
//            return
//        }
//
//        endDate = startDate!.addingTimeInterval(TimeInterval(Int(slider.value*60)))
//        endTimeLbl.text = DateFormatter.localizedString(from: endDate!, dateStyle: .none, timeStyle: .short)
//    }
    @IBAction func sliderValueDidChange(_ sender: Any) {
        if startDate == nil {
            startTimer()
            return
        }
        
        var sliderValue = Int(slider.value)
        sliderValue = sliderValue - lastSliderValue
        let partialEndDate = endDate!
        endDate = endDate!.addingTimeInterval(TimeInterval(sliderValue * 60))
        let difference = endDate! - startDate!
        if difference < 2 {
            endDate = partialEndDate
            return
        }
        endTimeLbl.text = DateFormatter.localizedString(from: endDate!, dateStyle: .none, timeStyle: .short)
        lastSliderValue = Int(slider.value)
    }
    @IBAction func sliderClicked(_ sender: Any) {
        lastSliderValue = Int(slider.value)
    }
    @IBAction func increaseActivityHourPressed(_ sender: UIButton) {
        if startDate == nil {
            startTimer()
            return
        }
        if let endTime = endDate, let startTime = startDate {
            let difference = endTime - startTime
//            if difference < 0 {
//                endDate = startDate
//            }
            if difference > 32400{  // 9 hours i.e 3600sec * 9
                return
            }
            else{
                endDate = endDate!.addingTimeInterval(TimeInterval(3600))
                endTimeLbl.text = DateFormatter.localizedString(from: endDate!, dateStyle: .none, timeStyle: .short)
            }
        }
        lastSliderValue = Int(slider.value)
    }
    @IBAction func reduceActivityHourPressed(_ sender: UIButton) {
        if startDate == nil {
            startTimer()
            return
        }
        let partialEndDate = endDate!.addingTimeInterval(TimeInterval(-3600))
        if let _ = endDate, let startTime = startDate {
            let difference = partialEndDate - startTime
            if difference < 2 {
                return
            }
            else{
                endDate = partialEndDate
                endTimeLbl.text = DateFormatter.localizedString(from: endDate!, dateStyle: .none, timeStyle: .short)
            }
        }
    }
    
}

//MARK:- CollectioView
extension DropBySetupVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return activitesNameArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell : DropByActivityCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DropByActivityCell", for: indexPath) as? DropByActivityCell else{return UICollectionViewCell()}
        let activity = activitesNameArray[indexPath.row]
        
        cell.cornerRadius = 0
        cell.borderWidth = 0
        cell.borderColor = UIColor.clear
        
        if selectedActivity == activity {
            cell.cornerRadius = 10
            cell.borderWidth = 3
            cell.borderColor = UIColor.init(named: "BlueType")
        }
        cell.iconImageView.image = UIImage(named: activity)
        cell.nameLbl.text = activity.capitalizingFirstLetter()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = collectionView.bounds.size.height
        return CGSize(width: height*0.9, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        guard let _ : DropByActivityCell = collectionView.cellForItem(at: indexPath) as? DropByActivityCell else {return}
        selectedActivity = activitesNameArray[indexPath.row]
        self.collectionView.reloadData()
    }
    
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
    
    
    
}
