//
//  DropbyVisibilityModel.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 10/02/2021.
//

import UIKit

class DropbyVisibilityModel: NSObject {

    var isDropbyVisible = true
    var personUserID = ""
    var powerUserIdToUnhide = ""
    
    init(isDropbyVisible: Bool, personUserID: String, powerUserIdToUnhide: String) {
        self.isDropbyVisible        = isDropbyVisible
        self.personUserID           = personUserID
        self.powerUserIdToUnhide    = powerUserIdToUnhide
        
    }
    
    override convenience init() {
        self.init(isDropbyVisible: true, personUserID: String(), powerUserIdToUnhide: String())
    }
}

//MARK:- MAPPER
extension DropbyVisibilityModel {
    
    class func mapper(snapshot: [String:Any]) -> DropbyVisibilityModel? {
        
        let isDropbyVisible     = snapshot["isDropbyVisible"] as? Bool ?? true
        let personUserID        = snapshot["personUserId"] as? String ?? String()
        let powerUserIdToUnhide = snapshot["powerUserIdToUnhide"] as? String ?? String()
        return DropbyVisibilityModel.init(isDropbyVisible: isDropbyVisible, personUserID: personUserID, powerUserIdToUnhide: powerUserIdToUnhide)
    }
    
    class func toDict(dModel: DropbyVisibilityModel) -> [String: Any] {
        
        var dict = [String: Any]()
        
        dict["isDropbyVisible"]     = dModel.isDropbyVisible
        dict["personUserId"]        = dModel.personUserID
        dict["powerUserIdToUnhide"] = dModel.powerUserIdToUnhide
        return dict
    }
}

