//
//  DropByActivityVC.swift
//  
//
//  Created by Junaid Mukhtar on 07/12/2020.
//

import UIKit
import MBProgressHUD
import FirebaseFirestore
import CoreLocation

class DropByActivityVC: UIViewController {

    @IBOutlet weak var emptyListLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var timeLeftLbl: UILabel!
    @IBOutlet weak var currentActivity: DesignableButton!
    var timer:Timer?
    var disableActivity: (() -> Void)?
    var dropByusers = [ProfileModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.launchActivity), name: startActivity, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        ProfileHandler.shared.userListingDelegate = self
        launchActivity()
    }
    
    @objc func launchActivity(){
        
        self.refreshDropByList()
        if let activity = ProfileHandler.shared.dropByActivity {
            
            self.timeLeftLbl.text = getTimeString()
            if self.timer == nil {
                self.timer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector:#selector(self.tick) , userInfo: nil, repeats: true)
            }
            currentActivity.setImage(UIImage(named: activity.activity), for: UIControl.State())
        }
    }
    
    
    @objc func tick() {
        
        if ProfileHandler.shared.dropByActivity == nil || self.timer == nil {
            
            return
        }
        self.timeLeftLbl.text = getTimeString()
    }
    
    fileprivate func getTimeString() -> String {
        
        let endTime = Date.init(timeInterval: 0, since: Timestamp.dateValue(ProfileHandler.shared.dropByActivity!.endTime)())
        
        let time1 = DateFormatter.localizedString(from: Date(), dateStyle: .none, timeStyle: .short)
        
        let time2 = DateFormatter.localizedString(from: endTime, dateStyle: .none, timeStyle: .short)

        let formatter = DateFormatter()
        formatter.dateFormat = "h:mma"

        let date1 = formatter.date(from: time1)!
        let date2 = formatter.date(from: time2)!

        let elapsedTime = date2.timeIntervalSince(date1)

        // convert from seconds to hours, rounding down to the nearest hour
        let hours = floor(elapsedTime / 60 / 60)

        // we have to subtract the number of seconds in hours from minutes to get
        // the remaining minutes, rounding down to the nearest minute (in case you
        // want to get seconds down the road)
        let minutes = floor((elapsedTime - (hours * 60 * 60)) / 60)
        
        return String(Int(hours)) + " Hr(s) " + String(Int(minutes)) + " Mins"
    }
    
    @IBAction func editPressed(_ sender: Any) {
//        activityEditMode?()
    }
    
    @IBAction func disableTapped(_ sender: Any) {
        disableActivity?()
        goBack()
    }
    
    func goBack() {
        
        self.parent?.dismiss(animated: true, completion: nil)        
    }
    
    func stopTimerTest() {
        if timer != nil {
            timer!.invalidate()
            timer = nil
        }
    }
}
//MARK:- UITableViewDataSource
extension DropByActivityVC : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dropByusers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let userModel = dropByusers[indexPath.row]
        let profileVC = ProfileVC.instantiate(fromAppStoryboard: .DropBy)
        profileVC.user = userModel
        self.show(profileVC, sender: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BottomUserCell", for: indexPath) as? BottomUserCell else{return UITableViewCell()}
        let userModel = dropByusers[indexPath.row]
        cell.profileModel = userModel
        cell.selectionStyle = .none
        cell.iconActivity.image = UIImage(named: userModel.dropByActivity)
//        cell.lblDescription.text = userModel.notes
        
        return cell
        
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        
        
        
        let messageAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            /// add vc here
            success(true)
        })
        
        
        messageAction.image = UIImage(named:"message")
        messageAction.backgroundColor = #colorLiteral(red: 0.1858788133, green: 0.7518694997, blue: 0.3267681003, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [  messageAction])
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let editAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            /// add vc here
            let userModel = self.dropByusers[indexPath.row]
            let vc = MoreActionSheet.instantiate(fromAppStoryboard: .PopUp)
            vc.userModel = userModel
            vc.callBack = { type in 
                
                if type == .report {
                    let storyBoard = UIStoryboard(name: "Feedback", bundle: nil)
                    guard let reportVC = storyBoard.instantiateViewController(identifier: "ReportVC") as? ReportVC else{return}
                                        
                    reportVC.againstUserId = userModel.userInfoModel.userId
                    self.navigationController?.pushViewController(reportVC, animated: true)
                }
                else if type == .block {
                    
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    ProfileHandler.shared.authInteractor.addToBlockList(model: userModel) { (success) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                }                
            }
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true) {
                UIView.animate(withDuration: 0.1) {
                    vc.backgroundView.alpha = 0.5
                }
                
            }
            success(true)
        })
        
        editAction.image = UIImage(named:"more")
        editAction.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [  editAction])
    }
    
    func refreshDropByList() {
        
        self.dropByusers.removeAll()
        self.dropByusers = ProfileHandler.shared.dropbyUsers
        DispatchQueue.main.async {
            
            if self.dropByusers.isEmpty {
                self.tableView.isHidden = true
                self.emptyListLabel.isHidden = false
            }
            else {
                self.tableView.isHidden = false
                self.emptyListLabel.isHidden = true
            }
            self.tableView.reloadData()
        }
    }
}

extension DropByActivityVC : UserListingRefreshProtocol {
    func dropbyUsersDidUpdate() {
        
        self.refreshDropByList()
    }
    
    func nearbyUserDidUpdate() {
        
    }
    
    func locationUpdated(location: CLLocation) {
    
    }
}
