//
//  DropByVC.swift
//  Dropby
//
//  Created by Junaid on 03/12/2020.
//

import UIKit
import MBProgressHUD

class DropByVC: SuperViewController {
    
    @IBOutlet weak var activityContainerView: UIView!
    @IBOutlet weak var setupContainerView: UIView!
    var isEditMode = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        activityContainerView.isHidden=true
        activityContainerView.alpha = 0
        
        setupContainerView.isHidden = false
        setupContainerView.alpha = 1
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let dvc : DropBySetupVC = segue.destination as? DropBySetupVC{
            if segue.identifier == "DropBySetup"{
                dvc.showFeedBackCallback = { [weak self] in
                    
                    if let stronSelf = self {
                        
                        stronSelf.isEditMode = false
                        stronSelf.activityContainerView.isHidden=false
                        stronSelf.activityContainerView.alpha = 1
                        
                        stronSelf.setupContainerView.isHidden = true
                        stronSelf.setupContainerView.alpha = 0
                    }
                }
            }
        }
        
        if let dvc : DropByActivityVC = segue.destination as? DropByActivityVC{
            if segue.identifier == "DropByActivities"{
                dvc.disableActivity = { [weak self] in
                    
                    if let stronSelf = self {
                        MBProgressHUD.showAdded(to: stronSelf.view, animated: true)
                        ProfileHandler.shared.authInteractor.deleteDropbyActivity { (success) in
                            
                            ProfileHandler.shared.dropByActivity = nil
                            MBProgressHUD.hide(for: stronSelf.view, animated: true)
                            
                            stronSelf.activityContainerView.isHidden=true
                            stronSelf.activityContainerView.alpha = 0
                            
                            stronSelf.setupContainerView.isHidden = false
                            stronSelf.setupContainerView.alpha = 1
                        }
                    }
                }
            }
        }
    }
    
    
    ///DropByActivities
    //DropBySetup
    
    
}
