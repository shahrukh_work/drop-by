//
//  DropByVC.swift
//  Dropby
//
//  Created by Junaid on 26/11/2020.
//

import UIKit
import FirebaseFirestore
var ref: Firestore?

var globalDropByMainVC:DropByMainVC?
class DropByMainVC: SuperViewController {
    

    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var dropByContainerView: UIView!
    @IBOutlet weak var notificationBarButton: UIBarButtonItem!
        var shouldShowNotificationView = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        globalDropByMainVC = self
        ref = Firestore.firestore()
        ref?.collection("usersDropByActivity").addSnapshotListener { snap, error in
            NotificationCenter.default.post(name: Notification.Name("updated"), object: nil)
            APIManager.shared.fetchDropbyUsers { success in
                
                if success{
                DispatchQueue.main.async {
//                    globalDropByMapView?.timer?.invalidate()
                    globalDropByMapView?.dropbyUsersDidUpdate()
                    }
                }
            }
        }
        
        self.setup()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        APIManager.shared.fetchDropbyUsers { success in
//
//            if success{
//            DispatchQueue.main.async {
//                globalDropByMapView?.timer?.invalidate()
//                globalDropByMapView?.timer = nil
//                globalDropByMapView?.dropbyUsersDidUpdate()
//                }
//            }
//        }
        shouldShowNotificationView = false
                if ref == nil {
                    ref = Firestore.firestore()
                }
                ref?.collection("notifications").document(ProfileHandler.shared.profileModel.userInfoModel.userId).addSnapshotListener { doc, error in
                    if self.shouldShowNotificationView == false{
                        self.shouldShowNotificationView = true
                        let shouldShowBadge = UserDefaults.standard.bool(forKey: "notification")
                        if shouldShowBadge {
                            self.notificationBarButton.setBadge()
                        }
                        else{
                            self.notificationBarButton.removeBadge()
                        }
                    }
                    else{
                        self.notificationBarButton.setBadge()
                        UserDefaults.standard.set(true, forKey: "notification")
                    }
                }
    }
    private func setup(){
        
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.systemBlue], for: .selected)
        hideMapView()

    }
    @IBAction func didTapSideMenu(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Settings", bundle: nil)
        guard let vc = storyBoard.instantiateViewController(identifier: "SettingVC") as? SettingVC else{return}
        vc.hidesBottomBarWhenPushed = true
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func segmentDidChanges(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            hideMapView()
            break;
            
        case 1:
            
            self.showMapView()
            break;
            
        default:
            break;
        }
    }
    
    private func showMapView(){
        mapContainerView.alpha = 0
        dropByContainerView.alpha = 1
        
    }
    
    private func hideMapView(){
        mapContainerView.alpha = 1
        dropByContainerView.alpha = 0
    }
    
    
}
