//
//  DropByMapVC.swift
//  Dropby
//
//  Created by Junaid on 26/11/2020.
//

import UIKit
import GoogleMaps
import GoogleMapsUtils
import MBProgressHUD
import SDWebImage
import UIDrawer
import FirebaseFirestore
var globalDropByMapView : DropByMapVC?
class DropByMapVC: UIViewController, GMUClusterRendererDelegate {
    var timer: Timer?
    //MAP
    var mapView : GMSMapView?
    private var clusterManager: GMUClusterManager?
    var markerList : [GMSMarker] = []
    //    var infoWin = InfoWindow()
    fileprivate var locationMarker : GMSMarker? = GMSMarker()
    //Location
    var location : CLLocation?
    var infoWin = InfoWindow()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        globalDropByMapView = self
        self.initViews()
        self.setupCluster()
        //        self.setupCluster()
        //        pullUpControl.dataSource = self
        //        self.infoWin = loadNiB()
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.addNotificationObservers()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updated(notifications:)), name: Notification.Name("updated"), object: nil)
        
        
    }
    @objc func updated(notifications : Notification){
        
    }
    
    func showBottomSheet(marker: GMSMarker) {
        let viewController = DropByBottomVC.instantiate(fromAppStoryboard: .DropBy)
        viewController.showAllUsers = true
        if let userID = marker.userData as? String {
            var found = false
            if globalDropByBottomVC?.tableView != nil{
                if ProfileHandler.shared.dropbyUsers.count == 1{
                    
                    globalDropByBottomVC!.tableView.isScrollEnabled = false
                }
                else{
                    globalDropByBottomVC!.tableView.isScrollEnabled = true
                }
                
            }
            for user in ProfileHandler.shared.dropbyUsers {
                if user.userInfoModel.userId == userID {
                    
                    found = true
                    viewController.profileModel = user
                }
            }
            if found == false {
                viewController.profileModel = ProfileHandler.shared.profileModel
            }
        }
        
        viewController.callBack = {message, user in
            
            viewController.dismiss(animated: true) {
                if message {
                    
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    //check if already a chat thread exists
                    MessagingHandler.shared.fetchChatDetailFor(userWithId: user.userInfoModel.userId) { (chatModel) in
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                        let detailMessageVc = MessageDetailVC.instantiate(fromAppStoryboard: .Message)
                        //            if self.parent.navigationController != nil {
                        //                    self.navigationController?.pushViewController(detailMessageVc, animated: true)
                        //                self.pushViewController(detailMessageVc, animated: true)
                        self.show(detailMessageVc, sender: true)
                        if chatModel != nil {
                            
                            detailMessageVc.chatListModel = chatModel!
                            detailMessageVc.chatID = chatModel!.chatID
                            detailMessageVc.userObject = user
                        }
                        else {
                            //create new chat
                            detailMessageVc.userObject = user
                        }
                        //            }
                    }
                }
                else {
                    let vc = GenericPopup.instantiate(fromAppStoryboard: .PopUp)
                    vc.modalPresentationStyle = .overFullScreen
                    vc.titleText = "Mark Invisible"
                    vc.descriptionText = "Would you like to mark "+user.userInfoModel.firstName+" invisible?"
                    vc.cancelButtonText = "CANCEL"
                    vc.okButtonText = "YES"
                    
                    self.present(vc, animated: true) {
                        
                        vc.btnActionCallback = { action in
                            
                            if action == .ok {
                                MBProgressHUD.showAdded(to: self.view, animated: true)
                                ProfileHandler.shared.authInteractor.addToDropbyInvisibleList(userID: user.userInfoModel.userId) { (success) in
                                    //refresh map
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    ProfileHandler.shared.shouldRefreshListing()
                                }
                                globalDropByMapView?.mapView?.clear()
                            }
                        }
                        UIView.animate(withDuration: 0.1) {
                            
                            vc.backgroundView.alpha = 0.5
                        }
                    }
                }
            }
        }
        viewController.modalPresentationStyle = .custom
        viewController.transitioningDelegate = self
        self.navigationController!.present(viewController, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        //        removeBottomView()
        //        self.dropbyUsersDidUpdate()
        self.location = ProfileHandler.shared.location
        ProfileHandler.shared.userListingDelegate = self
        LocationHandler.shared.locationUpdateDelegate = self        
        //        self.showUserLocationOnMap()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //        LocationHandler.shared.stopLocationUpdate()
    }
    
    
    private func saveLocationTOFireBase(){
        ///Adding into Profile model
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        ProfileHandler.shared.updateModelWithLocationModel(latitude: self.location!.coordinate.latitude, longitude: self.location!.coordinate.longitude) { (success) in
            
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
}

extension DropByMapVC{
    
    private func initViews(){
        self.addMap()
        
    }
}

//MARK:- Google Map
extension DropByMapVC{
    
    
    private func addMap(){
        self.dropbyUsersDidUpdate()
        if mapView == nil{
            let camera = GMSCameraPosition.camera(withLatitude: 0, longitude: 0 , zoom: 12)
            mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), camera: camera)
            mapView?.delegate = self
            mapView?.isMyLocationEnabled = true
            do {
                // Set the map style by passing the URL of the local file.
                if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                    mapView?.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                } else {
                    NSLog("Unable to find style.json")
                }
            } catch {
                NSLog("One or more of the map styles failed to load. \(error)")
            }
            
            self.view.addSubview(mapView!)
        }
    }
    
    private func showUserLocationOnMap(){
        
        DispatchQueue.main.async {
            self.addMarkersToCluster()
        }
    }
    
    private func getBase(number: Double) -> Double {
        return round(number * 1000)/1000
    }
    private func randomCoordinate() -> Double {
        return Double(arc4random_uniform(140)) * 0.0001
    }
    
    
    private func getNearByUserMarker( user : ProfileModel)-> GMSMarker{
        
        var position = CLLocationCoordinate2D()
        let iconPath = user.dropByActivity
        position = CLLocationCoordinate2D(latitude: user.geoLocationModel.coordinates.latitude, longitude: user.geoLocationModel.coordinates.longitude)
        
        let marker = GMSMarker(position: position)
        marker.icon = UIImage(named: iconPath)
        marker.userData = user.userInfoModel.userId
        return marker
        
    }
    
    private func getTestMarker(location: CLLocation)-> GMSMarker{
        
        var position = CLLocationCoordinate2D()
        position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        let marker = GMSMarker(position: position)
        marker.icon = UIImage(named: "avatar_1")
        return marker
    }
    
}


//MARK:- Marker Clustring
extension DropByMapVC : GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let zoom = mapView.camera.zoom
        print("map zoom is ",String(zoom))
    }
    
    private func setupCluster(){
        if clusterManager != nil {
            return
        }
        guard let map = mapView else{return}
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: map,
                                                 clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        renderer.minimumClusterSize = 2
        clusterManager = GMUClusterManager(map: map, algorithm: algorithm,
                                           renderer: renderer)
        
        clusterManager?.setMapDelegate(self)
        
    }
    
    private func addMarkersToCluster(){
        self.markerList.removeAll()
        clusterManager?.clearItems()
        let bounds = GMSCoordinateBounds()
        bounds.includingCoordinate(CLLocationCoordinate2D(latitude: self.location!.coordinate.latitude, longitude: self.location!.coordinate.longitude))
        let path = GMSMutablePath()
        guard let manager = clusterManager else{return}
        for user in ProfileHandler.shared.dropbyUsers{
            
            let marker = self.getNearByUserMarker(user: user)
            marker.icon = UIImage(named: user.dropByActivity)
            marker.tracksInfoWindowChanges = true
            markerList.append(marker)
            path.add(CLLocationCoordinate2DMake(marker.position.latitude, marker.position.longitude))
        }
        //        let position1 = CLLocationCoordinate2D(latitude: 47.60, longitude: -122.33)
        //        let marker1 = GMSMarker(position: position1)
        //        marker1.icon = #imageLiteral(resourceName: "food")
        //        let position2 = CLLocationCoordinate2D(latitude: 47.60, longitude: -122.46)
        //        let marker2 = GMSMarker(position: position2)
        //        marker2.icon = #imageLiteral(resourceName: "running")
        //        let position3 = CLLocationCoordinate2D(latitude: 47.30, longitude: -122.46)
        //        let marker3 = GMSMarker(position: position3)
        //        marker3.icon = #imageLiteral(resourceName: "chill")
        //        let position4 = CLLocationCoordinate2D(latitude: 47.20, longitude: -122.23)
        //        let marker4 = GMSMarker(position: position4)
        //        marker4.icon = #imageLiteral(resourceName: "swimming")
        //        markerList.append(marker1)
        //        markerList.append(marker2)
        //        markerList.append(marker3)
        //        markerList.append(marker4)
        let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
        mapView?.animate(with: update)
        if markerList.count == 0{
            
            
            let camera = GMSCameraPosition.camera(withLatitude: (self.location!.coordinate.latitude), longitude: self.location!.coordinate.longitude, zoom: 15.0)
            self.mapView!.animate(to: camera)
        }
        else {
            let bounds = GMSCoordinateBounds(path: path)
            self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
        }
        manager.add(markerList)
        manager.cluster()
        
    }
    
    
    internal func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        mapView.animate(toLocation: marker.position)
        // check if a cluster icon was tapped
        if marker.userData is GMUCluster {
            // zoom in on tapped cluster
            mapView.animate(toZoom: mapView.camera.zoom + 1)
            NSLog("Did tap cluster")
            print(marker.icon as Any)
            
            return true
        }
        locationMarker = marker
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        showBottomSheet(marker: marker)
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        DispatchQueue.main.async {
            self.infoWin.timer?.invalidate()
            self.infoWin.removeFromSuperview()
        }
        
    }
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        let userID = marker.userData as? String ?? ""
        
        for user in ProfileHandler.shared.dropbyUsers{
            
            if user.userInfoModel.userId == userID {
                //found it
                infoWin.removeFromSuperview()
                infoWin = UINib(nibName: "InfoWindow", bundle: nil).instantiate(withOwner: self, options: nil).first as! InfoWindow
                infoWin.notesLabel.text = user.dropByActivityModel.notes
                infoWin.imageV.sd_setImage(with: URL(string: user.userInfoModel.profilePicURL), placeholderImage: UIImage(named: "avatar_1"))
                self.timer?.invalidate()
                DispatchQueue.main.async {
                    
                    self.timer =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.tick(sender:)) , userInfo: user.dropByActivityModel.endTime, repeats: true)
                    
                    
                }
                return infoWin
            }
            
        }
        return nil
    }
    
    @objc func tick(sender: Timer) {
        DispatchQueue.main.async {
            let time = sender.userInfo as? Timestamp
            
            let endTime = Date.init(timeInterval: 0, since: time!.dateValue())
            let currentTime = Date()
            
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "HH:mm:ss"
            
            let elapsedTime = Int(endTime.timeIntervalSince(currentTime))
            
            self.infoWin.timeLabel.text = String(elapsedTime.secondsToTime())
        }
    }
    
}

extension DropByMapVC : UserListingRefreshProtocol {
    func dropbyUsersDidUpdate() {
        
        self.showUserLocationOnMap()
    }
    
    func nearbyUserDidUpdate() {
        
    }
    
    func locationUpdated(location: CLLocation) {
        self.location = location
    }
}

extension DropByMapVC : LocationProtocol {
    func updateUserLocation(location: CLLocation) {
        self.location = location
        if self.location != nil {
            ProfileHandler.shared.updateUserLocation(location: self.location!)
        }
        
    }
    
    func shouldRefreshListing() {
        
    }
}

extension DropByMapVC : UIViewControllerTransitioningDelegate  {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presentationController = DrawerPresentationController(presentedViewController: presented, presenting: presenting)
        presentationController.blurEffectStyle = .systemUltraThinMaterial
        presentationController.cornerRadius = 20
        presentationController.roundedCorners = [.topLeft, .topRight]
        presentationController.topGap = 300
        presentationController.bounce = true
        
        return presentationController
    }
}
