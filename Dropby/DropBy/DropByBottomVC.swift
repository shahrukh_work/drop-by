//
//  DropByBottomVC.swift
//  Dropby
//
//  Created by Junaid on 29/11/2020.
//

import UIKit
import MBProgressHUD
var globalDropByBottomVC:DropByBottomVC?
class DropByBottomVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var handleArea: UIView!
    var numberOfRows = 1
    var height: CGFloat?
    
    var showAllUsers = true
    var profileModel : ProfileModel?
    var screenHeight : CGFloat = 0
    var callBack : ((Bool, ProfileModel)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        globalDropByBottomVC = self
        
//        tableView.tableFooterView = UIView()
        
//        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(DropByBottomVC.panGesture))
//            view.addGestureRecognizer(gesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.tableView.reloadData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

//        UIView.animate(withDuration: 0.3) { [weak self] in
//            let frame = self?.view.frame
//            let yComponent = UIScreen.main.bounds.height - 250
//            self?.view.frame = CGRect(x: 0, y: yComponent, width: frame!.width, height: frame!.height)
//        }
    }
    
//    @objc func panGesture(recognizer: UIPanGestureRecognizer) {
//
//        showAllUsers = true
//        self.tableView.reloadData()
//
//        let translation = recognizer.translation(in: self.view)
//        let y = self.view.frame.minY + translation.y
//        self.view.frame = CGRect(x: 0, y: y, width: view.frame.width, height: screenHeight-y)
//        print(y)
//
//        recognizer.setTranslation(CGPoint.zero, in: self.view)
//        if y >= screenHeight {
//            self.removeFromParent()
//            dismiss(animated: true, completion: nil)
//        }
//    }
    
    
    
    @IBAction func dismissButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}


extension DropByBottomVC : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if showAllUsers{
            return ProfileHandler.shared.dropbyUsers.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BottomUserCell", for: indexPath) as? BottomUserCell else{return UITableViewCell()}
        
        if !showAllUsers && profileModel != nil{
            cell.profileModel = profileModel
        }else{
            cell.profileModel = ProfileHandler.shared.dropbyUsers[indexPath.row]
        }
        cell.selectionStyle = .none
        cell.delegage = self
        return cell
        
    }

}
extension DropByBottomVC : DropbyBottomCellProtocol {
    func messageUser(user: ProfileModel) {
        
        if callBack != nil {
            callBack!(true, user)
        }
    }
    
    func hideUser(user: ProfileModel) {
        if callBack != nil {
            callBack!(false, user)
        }
    }
}
