//
//  DropByActivityModel.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 23/12/2020.
//

import UIKit
import FirebaseFirestore

class DropByActivityModel: NSObject {

    var notes           = String()
    var activity        = String()
    var startTime       = Timestamp.init()
    var endTime         = Timestamp.init()
    var personalUserId  = String()
    
    init(notes: String, activity: String, startTime: Timestamp, endTime: Timestamp, userId: String) {
        self.startTime      = startTime
        self.endTime        = endTime
        self.notes          = notes
        self.activity       = activity
        self.personalUserId = userId
    }
    
    override convenience init() {
        self.init(notes: String(), activity: String(), startTime: Timestamp.init(), endTime: Timestamp.init(), userId: String())
    }
}

//MARK:- MAPPER
extension DropByActivityModel {
    
    class func mapper(snapshot: [String:Any]) -> DropByActivityModel? {
        
        let acti    = snapshot["activity"] as? String ?? String()
        var startT  = snapshot["startTime"] as? Timestamp
        var endT    = snapshot["endTime"] as? Timestamp
    
        let notes   = snapshot["notes"] as? String ?? String()
        let userID  = snapshot["personalUserId"] as? String ?? String()
        
        if startT == nil && endT == nil {
            //means data from server
            if let startArray = snapshot["startTime"] as? [String:Any] {
                let seconds = startArray["_seconds"] as? Int64 ?? 0
                let nanoseconds = startArray["_nanoseconds"] as? Int32 ?? 0
                startT = Timestamp.init(seconds: seconds, nanoseconds: nanoseconds)
            }
            
            if let startArray = snapshot["endTime"] as? [String:Any] {
                let seconds = startArray["_seconds"] as? Int64 ?? 0
                let nanoseconds = startArray["_nanoseconds"] as? Int32 ?? 0
                endT = Timestamp.init(seconds: seconds, nanoseconds: nanoseconds)
            }
        }
        
        
        return DropByActivityModel.init(notes: notes, activity: acti, startTime: startT!, endTime: endT!, userId: userID)
    }
    
    class func toDict(dModel: DropByActivityModel) -> [String: Any] {
        
        var dict = [String: Any]()
        
        dict["startTime"]       = dModel.startTime
        dict["endTime"]         = dModel.endTime
        dict["activity"]        = dModel.activity
        dict["notes"]           = dModel.notes
        dict["personalUserId"]  = dModel.personalUserId
        return dict
    }
}
