//
//  MessageSenderTableViewCell.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 28/01/2021.
//

import UIKit
import FirebaseFirestore

class MessageSenderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var bodyLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    var userObject: ProfileModel? {
        didSet {
            updateProfileViews()
        }
    }
    @IBOutlet weak var containerView: UIImageView!
    
    var messageObject: MessageDetail? {
        didSet{
            updateMessageObject()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateMessageObject() {
        
        
        self.bodyLbl.text = self.messageObject!.body
        let date = Timestamp.dateValue(self.messageObject!.createdAt)()
        let time1 = DateFormatter.localizedString(from: date, dateStyle: .none, timeStyle: .short)
        self.timeLbl.text = time1
    }
    
    func updateProfileViews() {
        self.titleLbl.text = self.userObject!.userInfoModel.firstName+" "+self.userObject!.userInfoModel.lastName        
    }
}
