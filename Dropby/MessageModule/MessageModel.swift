//
//  MessageModel.swift
//  Dropby
//
//  Created by Junaid Mukhtar on 27/01/2021.
//

import UIKit
import FirebaseFirestore

class MessageListModel: NSObject {
    
    var messages        = [MessageDetail]()
    var createdAt       = Timestamp.init()
    var participants    = [String]()
    var chatID          = String()
    var lastMessage     = MessageDetail()
    var unreadCount     = [String:Int]()
    
    init(createdAt: Timestamp, participants: [String], chatid: String, lastMsg: MessageDetail, unread: [String:Int]) {
        
        self.createdAt      = createdAt
        self.participants   = participants
        self.chatID         = chatid
        self.lastMessage    = lastMsg
        self.unreadCount    = unread
    }
    
    convenience override init(){
        self.init(createdAt: Timestamp.init(), participants: [String](), chatid: String(), lastMsg: MessageDetail(), unread: [String:Int]())
    }
}

extension MessageListModel {
    class func mapper(snapshot: [String:Any]) -> MessageListModel? {
        
        let createdAt       =   snapshot["createdAt"] as? Timestamp ?? Timestamp.init()
        let unreadCounts     =   snapshot["unreadCounts"] as? [String:Int]
        var participants    =   [String]()
        let participantObjects = snapshot["participants"] as? [String:Bool]
        var unreads = [String:Int]()
        
        if unreadCounts != nil {
            
            for count in unreadCounts! {
                
//                var obj = [String:Int]()
                unreads[count.key] = count.value
            }
        }
        
        if participantObjects != nil {
            for obj in participantObjects! {
                participants.append(obj.key)
            }
        }

        let lastMessage     =   MessageDetail.mapper(snapshot: snapshot["lastMessage"] as? [String : Any] ?? [String:Any]()) ?? MessageDetail()
        return MessageListModel.init(createdAt: createdAt, participants: participants, chatid: "", lastMsg: lastMessage, unread: unreads)
    }
    
    class func toDict(listModel: MessageListModel) -> [String: Any] {
        
        var dict = [String: Any]()
        dict["createdAt"]           = listModel.createdAt
        let participantsMap         = [listModel.participants.first: true,
                               listModel.participants.last: true]
        dict["participants"]        = participantsMap
        dict["lastMessage"]         = MessageDetail.toDict(messageModel: listModel.lastMessage)
        dict["participantsList"]    = [listModel.participants.first, listModel.participants.last]
//        var unreadsdict = [[String:Int]]()
//        for obj in listModel.unreadCount {
//
//            unreadsdict.append(obj)
//        }
        dict["unreadCounts"]         = listModel.unreadCount
        return dict
    }
}

class MessageDetail: NSObject {
    var messageID       = String()
    var body            = String()
    var createdAt       = Timestamp.init()
    var sender          = String()
    
    init(body: String, createdAt: Timestamp, sender: String) {
        
        self.body       = body
        self.createdAt  = createdAt
        self.sender     = sender
    }
    
    convenience override init(){
        self.init(body: "", createdAt: Timestamp.init(), sender: String())
    }
}

extension MessageDetail {
    class func mapper(snapshot: [String:Any]) -> MessageDetail? {
                
        
        let body        =   snapshot["body"] as? String ?? String()
        let createdAt   =   snapshot["createdAt"] as? Timestamp ?? Timestamp.init()
        let sender      =   snapshot["sender"] as? String ?? ""
        return MessageDetail.init(body: body, createdAt: createdAt, sender: sender)
    }
    
    class func toDict(messageModel: MessageDetail) -> [String: Any] {
        
        var dict = [String: Any]()
        
        dict["body"]        = messageModel.body
        dict["createdAt"]   = messageModel.createdAt
        dict["sender"]      = messageModel.sender
        return dict
//            [messageModel.messageID: dict]
    }
}
