# Drop-By

 ## Dropby Swift iOS app  getting started.

**Built using XCode 10.5 (Swift 5)**

# How to run the Dropby?

### Clone this [repo](https://gitlab.com/shahrukh_work/drop-by.git)

## How to Build?

1. Clone this repo
2. Open shell window and navigate to project folder
3. Run pod install
4. Open Dropby.xcworkspace and run the project on selected device or simulator
5. Add bridging header - see file Particle-Bridging-Header.h for reference.
6. Go to project settings->build settings->Objective-C bridging header-> DropBy-Bridging-Header.h (or wherever file is located).


### Error installing pods?

- if cocoapods installed
    - pod repo update
- esle 
    - Run this command in terminal **sudo gem install cocoapods**
    

## Heirarcy of DropBy

### Storyboards

    1. UserAuthentication
    2. Home
    3. Main
    4. Contacts
    5. DropBy
    6. Settings
    7. SettingDetail
    8. Message
    9. Popup
    10. Feedback
    11. Info Window
        

### Screens inside UserAuthentication StoryBoard

    1. LoginViewController
    2. OTPViewController
    3. EmailLinkingViewController
    4. EmailVerificattionVC
    5. DetailEditProfile
    6. MyTagsViewController
    
### Screens inside Home StoryBoard

    1. HomeeVC (NearBy Scree)
    2. DropByVC
    3. ContactVC
    4. MessageVC
    
### Screens inside Contacts StoryBoard
    
    1. Contacts
    
### Screens inside Message StoryBoard
    
    1. MessageVC
    2. MessaageDetail
    
### Screens inside Dropby StoryBoard

    1. Setting DetailVC
    2. SettingVC
    3. Change Phone
    4. Location SettingVC
    5. TermAndCondtionVC


# Third Parties

## Firebase 

1. Firebase is used to authenticate the Phonenumber
2. Firebase FireStore is used to store data and fetch data.
3. Firebase Cloud functions are also used to for some functionality
    - Contact Sync
    - NearBy
4. Chat is also implemented using firebase FireStore
5. Firebase Push Notifications


## GoogleMaps

1. Cluster mananger is used to show the marker and Clusters of Icons.
2. We are showing 


## LinkedIn

1. Email Authenticattion is used to for valid 


# Database
1. Database is FireStore
2. Firebase storage is used to store images.
3. Fireebase Snapshots are used to fetch data from firebase FireStore.


## Code to user snapshot
`ref.collection("nearByData").document(userID).collection("nearByUsers").addSnapshotListener { (snapShot, error) in }`

# Functionality

### ---- **Home Screen Functionality** -----

1. List of users are appearing on the screen that are within the radius of 9 km.\\
2. You can open the datail of that user
3. You can accept and send the friend request
4. You can mesage a friend.
5. Filters are available (All, Friends, Other)
6. Visibility Filter (Only friends, Everyone, Offline, Friends of Friends)


### ---- **DropBy Screen Functionality** -----

1. User can create activity e.g Cycling, Eating, Running, Gym
2. User can friends activities
3. User can disable his activity
4. You can hide other user activity
5. User can see friends activities in the form of list and marker on the map.


### ---- **Contact Screen Functionality** -----

1. Sync the contacts to firebase database
2. If a user has phone number in the mobile and same phone is also registered in DropBy. They become friends to each other after contact sync.


### ---- **Messaging Functionality** -----

1. You can only chat with friends
2. You chat history is maintained unless he deletes it intentionlly
3. User can also pin the contacts for easy access


### ---- **Settings Functionality** -----

1. User can edit profile 
2. User can change phonee Number
3. User can change visibility radius
4. User can see privacy policy and Terms and conditions.
5. Nottification Settings


# Functionality Protocols implemented

`internal protocol  AuthenticationManager {`
        
    func add(profile: ProfileModel, onSuccess: @escaping () -> Void, onError: ErrorClosure?)
    func fetch(onSuccess: @escaping (Bool) -> Void, onError: ErrorClosure?)
    func fetchUser(withUserId userID: String, onSuccess: @escaping(ProfileModel?)->Void)
    func fetchAllNearBy(onSuccess: @escaping ([[String:Any]]) -> Void, onError: ErrorClosure?)
    func uploadPictureToStorage(url: URL, completion: @escaping((String?, Bool)->Void))
    
    
    func uploadScreenShotToStorage (url: URL, completion: @escaping((String?, Bool)->Void))
    func realtimeUpdates()
    func sendFriendRequest(with userID: String, and object: FriendRequestModel, onSuccess: @escaping(Bool)->Void)
    func fetchAllFriends(for userID: String, and isSelf: Bool, onSuccess: @escaping(Bool, [String]?)->Void, onError: ErrorClosure?)
    func fetchFriendRequestsSent(forUserID userId: String, onSuccess: @escaping(Bool, [String]?)->Void)
    
    
    func addFriend(user: ProfileModel, onSuccess: @escaping(Bool)->Void)
    func reportUser(model: ReportModel, onSuccess: @escaping(Bool)->Void)
    func contactUs(model: ContactUsModel, onSuccess: @escaping(Bool)->Void)
    func addToBlockList(model: ProfileModel, onSuccess: @escaping(Bool)->Void)
    func setDropByActivity(model: DropByActivityModel, onSuccess: @escaping(Bool)->Void)
    
    
    func deleteDropbyActivity(onSuccess: @escaping(Bool)->Void)
    func fetchAllActivities(onSuccess: @escaping([[String:Any]])->Void, onError: ErrorClosure?)
    func addToNearbyInvisibleList(userID: String, onSuccess: @escaping(Bool)->Void)
    func addToDropbyInvisibleList(userID: String, onSuccess: @escaping(Bool)->Void)
    func updateVisibility(onSuccess: @escaping(Bool)->Void)
    
    
    
    func loadAllTables()
    func unBlock(user blockedID: String, forUser userId: String, dict: [String: Any], onSuccess: @escaping(Bool)->Void)
    func unHideDropby(user hiddenID: String, forUser userId: String, dict: [String: Any], onSuccess: @escaping(Bool)->Void)
    func getUserSettings(onSuccess: @escaping([String: Any])->Void, onError: ErrorClosure?)
    func updateSettings(setting: SettingsModel)
    
    
    func updateLocation(latitude: Double, longitude: Double)
    func addToNotificationList(forUser userId: String, notificationModel model:NotificationsModel, onSuccess: @escaping(Bool)->Void)
    func removeConnection(user: ProfileModel, onSuccess: @escaping(Bool)->Void)
    func cancelConnectionRequest(user: ProfileModel, onSuccess: @escaping(Bool)->Void)
`}`
